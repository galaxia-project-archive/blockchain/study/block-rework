﻿# ============================================================================================== #
#                                                                                                #
#                                     Galaxia Blockchain                                         #
#                                                                                                #
# ---------------------------------------------------------------------------------------------- #
# This file is part of the Xi framework.                                                         #
# ---------------------------------------------------------------------------------------------- #
#                                                                                                #
# Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               #
#                                                                                                #
# This program is free software: you can redistribute it and/or modify it under the terms of the #
# GNU General Public License as published by the Free Software Foundation, either version 3 of   #
# the License, or (at your option) any later version.                                            #
#                                                                                                #
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      #
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      #
# See the GNU General Public License for more details.                                           #
#                                                                                                #
# You should have received a copy of the GNU General Public License along with this program.     #
# If not, see <https://www.gnu.org/licenses/>.                                                   #
#                                                                                                #
# ============================================================================================== #

# xi_check_submodule(
#   <dir>                                   << Git submodule directory
# )
#
# Checks if the submodule was cloned, if not tries to find git and inits the submodule himself.
if(XI_GIT_SUBMODULES)
  find_package(Git QUIET)
  if(GIT_FOUND)
    macro(xi_check_submodule dir)
      if(NOT EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/${dir}/.git")
        xi_status("Submodule not initialized, trying to fetch.")
        execute_process(
          COMMAND ${GIT_EXECUTABLE} submodule update --init --recursive
          WORKING_DIRECTORY ${dir}
          RESULT_VARIABLE GIT_SUBMOD_RESULT
        )
        if(NOT GIT_SUBMOD_RESULT EQUAL "0")
          xi_fatal("git submodule update --init failed with ${GIT_SUBMOD_RESULT}, please checkout submodules. (${dir})")
        endif()
      endif()
    endmacro(xi_check_submodule)
  else()
    xi_warning("XI_GIT_SUBMODULES is enabled but git has not been found.")
    macro(xi_check_submodule)
      if(NOT EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/${dir}/.git")
        xi_fatal("Required git submodule is missing (${dir}) please init submodules using git:\n\tgit submodule update --init --recursive")
      endif()
    endmacro(xi_check_submodule)
  endif()
else()
  macro(xi_check_submodule)
    if(NOT EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/${dir}/.git")
      xi_fatal("Required git submodule is missing (${dir}) please init submodules using git:\n\tgit submodule update --init --recursive")
    endif()
  endmacro(xi_check_submodule)
endif()

