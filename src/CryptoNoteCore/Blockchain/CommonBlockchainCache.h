﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <optional>

#include <Xi/Byte.hh>
#include <Xi/Blockchain/Currency/Reward.hpp>
#include <Logging/ILogger.h>
#include <Logging/LoggerRef.h>

#include "CryptoNoteCore/IBlockchainCache.h"
#include "CryptoNoteCore/CryptoNote.h"

namespace CryptoNote {
class Currency;

/*!
 * \brief The CommonBlockchainCache class partially implements a blockchain refactoring commonly shared
 * procedures of the in memory cache and database cache out.
 */
class CommonBlockchainCache : public IBlockchainCache {
 protected:
  CommonBlockchainCache(Logging::ILogger& logger, const Currency& currency);

 public:
  virtual ~CommonBlockchainCache() override = default;

  // ------------------------------------------ IBlockchainCache ------------------------------------------------------
  [[nodiscard]] bool isTransactionSpendTimeUnlocked(uint64_t unlockTime) const override;
  [[nodiscard]] bool isTransactionSpendTimeUnlocked(uint64_t unlockTime, uint32_t blockIndex) const override;
  [[nodiscard]] bool isTransactionSpendTimeUnlocked(uint64_t unlockTime, uint32_t blockIndex,
                                                    uint64_t timestamp) const override;
  [[nodiscard]] uint64_t blockTimestamp(uint32_t blockIndex) const override;

  [[nodiscard]] CachedBlockInfoVector lastUnits(uint32_t count, uint32_t index) const;

  void pushBlock(class BlockValidationCache&& block) override;
  // ------------------------------------------ IBlockchainCache ------------------------------------------------------

  // ----------------------------------------- Legacy Conversion ------------------------------------------------------
  Xi::Result<std::optional<Transaction> > constructStaticRewardTransaction(const BlockHash& previousBlockHash,
                                                                           const uint32_t index,
                                                                           const BlockVersion version) const;
  // ----------------------------------------- Legacy Conversion ------------------------------------------------------

  const Currency& currency() const;

 protected:
  uint32_t checkHeight(const BlockHeight& height, bool allowParent = false) const;
  void checkIndex(uint32_t index, bool allowParent = false) const;

 protected:
  virtual void doPushBlock(const CachedBlock& cachedBlock, const std::vector<CachedTransaction>& cachedTransactions,
                           const TransactionValidatorState& validatorState, size_t blockSize, uint64_t generatedCoins,
                           uint64_t blockDifficulty, RawBlock&& rawBlock) = 0;

 private:
  [[nodiscard]] bool isTransactionSpendTimeUnlockedByBlockIndex(uint64_t unlockTime, uint32_t blockIndex) const;
  [[nodiscard]] bool isTransactionSpendTimeUnlockedByTimestamp(uint64_t unlockTime, uint32_t blockIndex) const;

 private:
  Logging::LoggerRef m_logger;
  const Currency& m_currency;
};
}  // namespace CryptoNote
