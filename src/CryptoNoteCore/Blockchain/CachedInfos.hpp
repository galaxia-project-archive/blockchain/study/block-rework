﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <crypto/CryptoTypes.h>
#include <CryptoNoteCore/Transactions/Transaction.h>
#include <CryptoNoteCore/Transactions/TransactionValidatiorState.h>
#include <Xi/Blockchain/Block/Block.hpp>
#include <Xi/Blockchain/Block/Timestamp.hpp>
#include <Xi/Blockchain/Block/RawBlock.hpp>
#include <Xi/Blockchain/ProofOfWork/Difficulty.hpp>
#include <Xi/Blockchain/Cache/BlockInfo.hpp>

namespace CryptoNote {

union PackedOutIndex {
  struct {
    uint32_t blockIndex;
    uint16_t transactionIndex;
    uint16_t outputIndex;
  } data;

  uint64_t packedValue;
};

const uint32_t INVALID_BLOCK_INDEX = std::numeric_limits<uint32_t>::max();

struct PushedBlockInfo {
  RawBlock rawBlock;
  TransactionValidatorState validatorState;
  size_t blockSize;
  Amount emission;
  BlockDifficulty blockDifficulty;
  BlockTimestamp timestamp{0};
};

class ISerializer;

struct SpentKeyImage {
  uint32_t blockIndex;
  Crypto::KeyImage keyImage;

  [[nodiscard]] bool serialize(ISerializer& s);
};

struct CachedTransactionInfo {
  uint32_t blockIndex;
  uint32_t transactionIndex;
  Crypto::Hash transactionHash;
  TransactionUnlock unlockTime;
  std::vector<TransactionOutputTarget> outputs;
  // needed for getTransactionGlobalIndexes query
  std::vector<uint32_t> globalIndexes;
  bool isDeterministicallyGenerated;

  [[nodiscard]] bool serialize(ISerializer& s);
};
using CachedTransactionInfoVector = std::vector<CachedTransactionInfo>;

}  // namespace CryptoNote
