﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "CryptoNoteCore/Blockchain/CommonBlockchainCache.h"

#include <ctime>
#include <cassert>
#include <algorithm>
#include <numeric>

#include <Xi/Exceptions.hpp>
#include <Xi/Crypto/EllipticCurve/EllipticCurve.hpp>
#include <Xi/Blockchain/Currency/Reward.hpp>
#include <Xi/Blockchain/Validation/BlockValidationResult.hpp>
#include <Serialization/BinaryOutputStreamSerializer.h>

#include "CryptoNoteCore/Transactions/TransactionExtra.h"
#include "CryptoNoteCore/CryptoNoteTools.h"
#include "CryptoNoteCore/Currency.h"

using Logging::Level;

CryptoNote::CommonBlockchainCache::CommonBlockchainCache(Logging::ILogger &logger, const Currency &currency)
    : m_logger(logger, "CommonBlockchainCache"), m_currency(currency) {}

bool CryptoNote::CommonBlockchainCache::isTransactionSpendTimeUnlocked(uint64_t unlockTime) const {
  return isTransactionSpendTimeUnlocked(unlockTime, getTopBlockIndex());
}

bool CryptoNote::CommonBlockchainCache::isTransactionSpendTimeUnlocked(uint64_t unlockTime, uint32_t blockIndex) const {
  if (m_currency.isLockedBasedOnBlockIndex(unlockTime)) {
    return isTransactionSpendTimeUnlockedByBlockIndex(unlockTime, blockIndex);
  } else {
    return isTransactionSpendTimeUnlockedByTimestamp(unlockTime, blockIndex);
  }
}

bool CryptoNote::CommonBlockchainCache::isTransactionSpendTimeUnlocked(uint64_t unlockTime, uint32_t blockIndex,
                                                                       uint64_t timestamp) const {
  return m_currency.isUnlockSatisfied(unlockTime, blockIndex, timestamp);
}

uint64_t CryptoNote::CommonBlockchainCache::blockTimestamp(uint32_t blockIndex) const {
  using namespace Xi;

  if (blockIndex == BlockHeight::Genesis.toIndex()) {
    return m_currency.genesisTimestamp();
  } else {
    exceptional_if_not<OutOfRangeError>(blockIndex <= getTopBlockIndex());
    auto timestamps = getLastTimestamps(1, blockIndex, UseGenesis{true});
    exceptional_if<NotFoundError>(timestamps.empty(), "previous block timestamp not found");
    return timestamps.front();
  }
}

CryptoNote::CachedBlockInfoVector CryptoNote::CommonBlockchainCache::lastUnits(uint32_t count, uint32_t index) const {
  checkIndex(index, true);
  if (count > index + 1) {
    count = index + 1;
  };
  CachedBlockInfoVector infos{};
  getLastUnits(count, index, UseGenesis{true}, [&](const auto &info) {
    infos.push_back(info);
    return 0;
  });
  return infos;
}

void CryptoNote::CommonBlockchainCache::pushBlock(BlockValidationCache &&block) {
  try {
  } catch (std::exception &e) {
    m_logger(Logging::FATAL) << "Error pushing block: " << e.what();
    throw e;
  } catch (...) {
    m_logger(Logging::FATAL) << "Error pushing block: UNKNOWN";
    throw;
  }
}

Xi::Result<std::optional<CryptoNote::Transaction>> CryptoNote::CommonBlockchainCache::constructStaticRewardTransaction(
    const CryptoNote::BlockHash &previousBlockHash, const uint32_t index,
    const CryptoNote::BlockVersion version) const {
  using namespace Xi;

  XI_ERROR_TRY();
  const auto &cconsensus = currency().consensus(version).currency();
  exceptional_if_not<NotFoundError>(cconsensus.has_value(), "currency system disabled");
  if (cconsensus->staticReward()) {
    return emplaceSuccess<std::optional<Transaction>>(std::nullopt);
  }
  Xi::Blockchain::Currency::Reward reward{};
  reward.version(Xi::Blockchain::Currency::RewardVersion{1});
  AccountPublicAddress parsedRewardAddress = boost::value_initialized<AccountPublicAddress>();
  Xi::exceptional_if_not<AccountPublicAddressParseError>(parseAccountAddressString(rewardAddress, parsedRewardAddress));

  Transaction tx{};
  tx.nullify();

  auto saltedBuffer = m_deterministicKeySalt;
  saltedBuffer.reserve(saltedBuffer.size() + Crypto::Hash::bytes());
  std::copy(previousBlockHash.begin(), previousBlockHash.end(), std::back_inserter(saltedBuffer));

  const KeyPair txkey = generateDeterministicKeyPair(saltedBuffer);
  addTransactionPublicKeyToExtra(tx.extra, txkey.publicKey());

  // TODO
  BaseInput in;
  in.height = BlockHeight::fromIndex(0);

  std::vector<uint64_t> outAmounts;
  decompose_amount_into_digits(rewardAmount, [&outAmounts](uint64_t a_chunk) { outAmounts.push_back(a_chunk); });

  const size_t maxOuts = 10;
  while (maxOuts < outAmounts.size()) {
    outAmounts[outAmounts.size() - 2] += outAmounts.back();
    outAmounts.resize(outAmounts.size() - 1);
  }

  uint64_t summaryAmounts = 0;
  for (size_t no = 0; no < outAmounts.size(); no++) {
    Crypto::KeyDerivation derivation = boost::value_initialized<Crypto::KeyDerivation>();
    Crypto::PublicKey outEphemeralPubKey = boost::value_initialized<Crypto::PublicKey>();

    if (!Crypto::generate_key_derivation(parsedRewardAddress.viewKey(), txkey.secretKey(), derivation)) {
      Xi::exceptional<Crypto::KeyDerivationError>();
    }
    if (!Crypto::derive_public_key(derivation, no, parsedRewardAddress.spendKey(), outEphemeralPubKey)) {
      Xi::exceptional<Crypto::PublicKeyDerivationError>();
    }

    KeyOutput tk;
    tk.key = outEphemeralPubKey;

    TransactionOutput out;
    summaryAmounts += out.amount = outAmounts[no];
    out.target = tk;
    tx.exchange.outputs.push_back(out);
  }

  if (!(summaryAmounts == rewardAmount)) {
    Xi::exceptional<RewardMissmatchError>();
  }

  tx.prefix.version = Xi::Config::Transaction::version();
  // lock
  tx.prefix.unlock = 0;
  tx.exchange.inputs.push_back(in);

  return Xi::success<boost::optional<Transaction>>(std::move(tx));
  return success(std::make_optional<Transaction>(reval.takeOrThrow()));
  XI_ERROR_CATCH();
}

const CryptoNote::Currency &CryptoNote::CommonBlockchainCache::currency() const { return m_currency; }

uint32_t CryptoNote::CommonBlockchainCache::checkHeight(const CryptoNote::BlockHeight &height, bool allowParent) const {
  using namespace Xi;

  exceptional_if<InvalidArgumentError>(height.isNull());
  const auto index = height.toIndex();
  checkIndex(index, allowParent);
  return index;
}

void CryptoNote::CommonBlockchainCache::checkIndex(uint32_t index, bool allowParent) const {
  using namespace Xi;

  exceptional_if<NotFoundError>(index > getTopBlockIndex());
  exceptional_if<NotFoundError>(!allowParent && index < getStartBlockIndex());
}

bool CryptoNote::CommonBlockchainCache::isTransactionSpendTimeUnlockedByBlockIndex(uint64_t unlockTime,
                                                                                   uint32_t blockIndex) const {
  assert(unlockTime <= BlockHeight::max().toIndex());
  return m_currency.isUnlockSatisfied(unlockTime, blockIndex, 0);
}

bool CryptoNote::CommonBlockchainCache::isTransactionSpendTimeUnlockedByTimestamp(uint64_t unlockTime,
                                                                                  uint32_t blockIndex) const {
  assert(unlockTime > BlockHeight::max().toIndex());
  assert(blockIndex < getTopBlockIndex() + 1);

  if (blockIndex > getTopBlockIndex()) {
    m_logger(Level::FATAL)
        << "Cannot query children for transaction spend time unlock timestamp, this would be ambiguous.";
    return false;
  }

  if (blockIndex < getStartBlockIndex()) {
    auto parent = getParent();
    if (parent != nullptr) {
      return parent->isTransactionSpendTimeUnlocked(unlockTime, blockIndex);
    } else {
      m_logger(Level::FATAL) << "Unable to load block timestamp, block not contained by this and no parent given.";
      return false;
    }
  }

  uint64_t timestamp = 0;
  time_t localTime = time(nullptr);
  if (localTime < 0) {
    m_logger(Level::FATAL) << "Unable to retrieve unix timestamp. Expected > 0, actual: " << timestamp;
    return false;
  } else {
    timestamp = static_cast<uint64_t>(localTime);
  }

  return m_currency.isUnlockSatisfied(unlockTime, blockIndex, timestamp);
}
