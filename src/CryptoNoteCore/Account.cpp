﻿// Copyright (c) 2012-2017, The CryptoNote developers, The Bytecoin developers
// Copyright (c) 2014-2018, The Monero Project
// Copyright (c) 2018, The TurtleCoin Developers
//
// Please see the included LICENSE file for more information.

#include "Account.h"

#include "CryptoNoteSerialization.h"
#include <Xi/Crypto/EllipticCurve/Scalar.hpp>

namespace CryptoNote {
//-----------------------------------------------------------------
AccountBase::AccountBase() { nullify(); }
//-----------------------------------------------------------------
void AccountBase::nullify() { m_keys = AccountKeys(); }
//-----------------------------------------------------------------
void AccountBase::generate() {
  m_keys.spendSecretKey = Xi::Crypto::EllipticCurve::Scalar::generateRandom().takeOrThrow();
  m_keys.address.spendKey() = m_keys.spendSecretKey.derivePublicKey();
  generateViewFromSpend(m_keys.spendSecretKey, m_keys.viewSecretKey, m_keys.address.viewKey());
  m_creation_timestamp = time(NULL);
}
void AccountBase::generateViewFromSpend(const Crypto::SecretKey &spend, Crypto::SecretKey &viewSecret,
                                        Crypto::PublicKey &viewPublic) {
  generateViewFromSpend(spend, viewSecret);
  viewPublic = viewSecret.derivePublicKey();
}

void AccountBase::generateViewFromSpend(const Crypto::SecretKey &spend, Crypto::SecretKey &viewSecret) {
  viewSecret = Xi::Crypto::EllipticCurve::Scalar::generateRandom(spend.span()).takeOrThrow();
}
//-----------------------------------------------------------------
const AccountKeys &AccountBase::getAccountKeys() const { return m_keys; }

void AccountBase::setAccountKeys(const AccountKeys &keys) { m_keys = keys; }
//-----------------------------------------------------------------

bool AccountBase::serialize(ISerializer &s) {
  XI_RETURN_EC_IF_NOT(s(m_keys, "keys"), false);
  XI_RETURN_EC_IF_NOT(s(m_creation_timestamp, "creation_timestamp"), false);
  return true;
}
}  // namespace CryptoNote
