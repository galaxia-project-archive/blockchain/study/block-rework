﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <map>
#include <set>

#include <Xi/Result.h>

#include "CryptoNoteCore/Transactions/Transaction.h"
#include "CryptoNoteCore/Transactions/TransactionValidationErrors.h"
#include "CryptoNoteCore/Transactions/TransactionExtra.h"

namespace CryptoNote {

struct SemanticUnpackedExtra {
  ::Crypto::PublicKey publicKey;
  std::optional<PaymentId> paymentId;
  std::optional<BinaryArray> customData;
};

struct SemanticUnpackedTransaction {
  ::Crypto::PublicKey publicKey;
  std::map<Amount, GlobalOutputIndexVector> referencedOutputs;
};

Xi::Result<SemanticUnpackedTransaction> validateSemantic(const Transaction& tx);
Xi::Result<SemanticUnpackedExtra> validateExtra(const Transaction& tx);

bool validateExtraSize(const Transaction& tx);
bool validateExtraCumulativePadding(const std::vector<TransactionExtraField>& fields);

bool validateExtraPublicKeys(const std::vector<TransactionExtraField>& fields);
bool validateExtraPublicKeys(const TransactionExtraPublicKey& pk);

bool validateExtraNonce(const std::vector<TransactionExtraField>& fields);
bool validateExtraNonce(const TransactionExtraNonce& nonce);

bool validateCanonicalDecomposition(const Transaction& tx);

}  // namespace CryptoNote
