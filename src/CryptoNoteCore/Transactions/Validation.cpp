﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "CryptoNoteCore/Transactions/Validation.h"

#include <algorithm>
#include <iterator>

#include <Xi/Exceptional.hpp>
#include <Xi/Blockchain/Currency/Amount.hpp>

#include "CryptoNoteCore/CryptoNoteTools.h"
#include "CryptoNoteCore/CryptoNoteFormatUtils.h"

using Error = CryptoNote::error::TransactionValidationError;

Xi::Result<CryptoNote::SemanticUnpackedExtra> CryptoNote::validateExtra(const CryptoNote::Transaction &tx) {
  using Xi::makeError;

  XI_RETURN_EC_IF_NOT(validateExtraSize(tx), makeError(Error::EXTRA_TOO_LARGE));
  std::vector<TransactionExtraField> fields;
  XI_RETURN_EC_IF_NOT(parseTransactionExtra(tx.extra, fields), makeError(Error::EXTRA_ILL_FORMED));
  XI_RETURN_EC_IF_NOT(validateExtraPublicKeys(fields), makeError(Error::EXTRA_INVALID_PUBLIC_KEY));
  XI_RETURN_EC_IF_NOT(validateExtraNonce(fields), makeError(Error::EXTRA_INVALID_NONCE));

  SemanticUnpackedExtra result{};
  result.publicKey = std::get<TransactionExtraPublicKey>(fields.front()).publicKey;
  result.paymentId = std::nullopt;
  result.customData = std::nullopt;
  return Xi::success(result);
}

bool CryptoNote::validateExtraSize(const CryptoNote::Transaction &tx) { return tx.extra.size() <= TX_EXTRA_MAX_SIZE; }

bool CryptoNote::validateExtraPublicKeys(const std::vector<TransactionExtraField> &fields) {
  const auto isPublicKey = [](const auto &field) { return std::holds_alternative<TransactionExtraPublicKey>(field); };
  auto search = std::find_if(fields.begin(), fields.end(), isPublicKey);
  XI_RETURN_EC_IF(search != fields.begin(), false);
  const auto &pkField = std::get<TransactionExtraPublicKey>(*search);
  XI_RETURN_EC_IF_NOT(validateExtraPublicKeys(pkField), false);
  auto another = std::find_if(std::next(search), fields.end(), isPublicKey);
  return another == fields.end();
}

bool CryptoNote::validateExtraPublicKeys(const CryptoNote::TransactionExtraPublicKey &pk) {
  return pk.publicKey.isValid();
}

bool CryptoNote::validateExtraCumulativePadding(const std::vector<CryptoNote::TransactionExtraField> &fields) {
  size_t padCount = 0;
  bool previousWasPadding = true;
  for (const auto &field : fields) {
    if (auto padding = std::get_if<TransactionExtraPadding>(std::addressof(field))) {
      XI_UNUSED(padding);
      padCount += 1;
      XI_RETURN_EC_IF(padCount > TX_EXTRA_PADDING_MAX_COUNT, false);
      XI_RETURN_EC_IF(previousWasPadding, false);
      previousWasPadding = true;
    } else {
      previousWasPadding = false;
    }
  }
  return !previousWasPadding;
}

bool CryptoNote::validateExtraNonce(const std::vector<TransactionExtraField> &fields) {
  const auto isExtraNonce = [](const auto &field) { return std::holds_alternative<TransactionExtraNonce>(field); };
  auto search = std::find_if(fields.begin(), fields.end(), isExtraNonce);
  XI_RETURN_EC_IF(search == fields.end(), true);
  const auto &nonceField = std::get<TransactionExtraNonce>(*search);
  XI_RETURN_EC_IF_NOT(validateExtraNonce(nonceField), false);
  auto another = std::find_if(std::next(search), fields.end(), isExtraNonce);
  return another == fields.end();
}

bool CryptoNote::validateExtraNonce(const CryptoNote::TransactionExtraNonce &nonce) {
  XI_RETURN_EC_IF(nonce.nonce.empty(), false);
  XI_RETURN_EC_IF(nonce.nonce.size() > TX_EXTRA_NONCE_MAX_COUNT, false);

  if (nonce.nonce[0] == TX_EXTRA_NONCE_PAYMENT_ID) {
    XI_RETURN_EC_IF_NOT(nonce.nonce.size() == 1 + Crypto::PublicKey::bytes(), false);
    Crypto::PublicKey paymentId{};
    std::memcpy(paymentId.data(), &nonce.nonce[1], Crypto::PublicKey::bytes());
    XI_RETURN_EC_IF_NOT(paymentId.isValid(), false);
  } else {
    XI_RETURN_EC_IF_NOT(nonce.nonce[0] == TX_EXTRA_NONCE_CUSTOM_ID, false);
  }

  return true;
}

bool CryptoNote::validateCanonicalDecomposition(const CryptoNote::Transaction &tx) {
  return std::all_of(tx.exchange.outputs.begin(), tx.exchange.outputs.end(),
                     [](const auto &iOutput) { return Xi::Blockchain::Currency::isCanonicalAmount(iOutput.amount); });
}

Xi::Result<CryptoNote::SemanticUnpackedTransaction> CryptoNote::validateSemantic(const CryptoNote::Transaction &tx) {
  using Xi::makeError;
  if (tx.exchange.inputs.empty()) {
    return makeError(Error::EMPTY_INPUTS);
  }
  if (tx.exchange.outputs.empty()) {
    return makeError(Error::EMPTY_OUTPUTS);
  }

  //  if (auto ec = validateExtra(tx); ec != Error::VALIDATION_SUCCESS) {
  //    return makeError(ec);
  //  }

  Amount sumInputs = 0;
  std::map<Amount, std::set<GlobalOutputIndex>> usedGlobalIndices{};
  std::set<::Crypto::KeyImage> usedKeyImages{};

  if (const auto baseInput = std::get_if<BaseInput>(std::addressof(tx.exchange.inputs.front()))) {
    if (baseInput->height.isNull() || baseInput->height > BlockHeight::max()) {
      return makeError(Error::BASE_INPUT_INVALID_HEIGHT);
    }
    if (tx.exchange.inputs.size() > 1) {
      return makeError(Error::BASE_INPUT_WRONG_COUNT);
    }
    if (!tx.signatures.empty()) {
      return makeError(Error::INPUT_INVALID_SIGNATURES_COUNT);
    }
  } else {
    if (tx.signatures.size() != tx.exchange.inputs.size()) {
      return makeError(Error::INPUT_INVALID_SIGNATURES_COUNT);
    }

    size_t i = 0;
    for (const auto &input : tx.exchange.inputs) {
      const auto keyInput = std::get_if<KeyInput>(std::addressof(input));
      if (keyInput == nullptr) {
        return makeError(Error::INPUT_UNKNOWN_TYPE);
      }
      if (!isCanonicalAmount(keyInput->amount)) {
        return makeError(Error::INPUT_NOT_CANONICIAL);
      }
      if (!keyInput->keyImage.isValid()) {
        return makeError(Error::INPUT_INVALID_DOMAIN_KEYIMAGES);
      }
      if (!usedKeyImages.insert(keyInput->keyImage).second) {
        return makeError(Error::INPUT_IDENTICAL_KEYIMAGES);
      }

      GlobalOutputIndexVector globalIndices = relativeOutputOffsetsToAbsolute(keyInput->outputIndices);
      if (globalIndices.empty()) {
        return makeError(Error::INPUT_INVALID_GLOBAL_INDEX);
      }
      for (const auto globalIndex : globalIndices) {
        if (!usedGlobalIndices[keyInput->amount].insert(globalIndex).second) {
          return makeError(Error::INPUT_IDENTICAL_OUTPUT_INDEXES);
        }
      }

      if (tx.signatures[i++].size() != globalIndices.size()) {
        return makeError(Error::INPUT_INVALID_SIGNATURES_COUNT);
      }

      if (std::numeric_limits<Amount>::max() - keyInput->amount < sumInputs) {
        return makeError(Error::INPUTS_AMOUNT_OVERFLOW);
      }
      sumInputs += keyInput->amount;
    }
  }

  Amount sumOutputs = 0;
  std::set<::Crypto::PublicKey> outKeys{};
  for (const auto &output : tx.exchange.outputs) {
    if (!isCanonicalAmount(output.amount)) {
      return makeError(Error::OUTPUTS_NOT_CANONCIAL);
    }
    const auto keyOutput = std::get_if<KeyOutput>(std::addressof(output.target));
    if (keyOutput == nullptr) {
      return makeError(Error::OUTPUT_UNEXPECTED_TYPE);
    }
    if (!keyOutput->key.isValid()) {
      return makeError(Error::OUTPUT_INVALID_KEY);
    }
    if (!outKeys.insert(keyOutput->key).second) {
      return makeError(Error::OUTPUT_DUPLICATE_KEY);
    }

    if (std::numeric_limits<Amount>::max() - output.amount < sumOutputs) {
      return makeError(Error::OUTPUTS_AMOUNT_OVERFLOW);
    }
  }

  if (sumOutputs > sumInputs) {
    return makeError(Error::INPUT_AMOUNT_INSUFFICIENT);
  }

  return makeError(Error::VALIDATION_SUCCESS);
}
