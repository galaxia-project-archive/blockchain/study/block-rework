﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <vector>
#include <variant>

#include <Xi/ExternalIncludePush.h>
#include <boost/variant.hpp>
#include <Xi/ExternalIncludePop.h>

#include <Xi/Blockchain/Block/Height.hpp>
#include <Xi/Blockchain/Currency/Transfer.hpp>
#include <Serialization/ISerializer.h>
#include <Serialization/SerializationOverloads.h>
#include <crypto/CryptoTypes.h>

namespace CryptoNote {

using Amount = uint64_t;
using AmountVector = std::vector<uint64_t>;

using TransactionHash = Crypto::Hash;
using TransactionHashVector = std::vector<TransactionHash>;
XI_DECLARE_SPANS(TransactionHash)

using PaymentId = Xi::Blockchain::Currency::PaymentIdentifier;
using GlobalOutputIndex = uint32_t;
using GlobalOutputIndexVector = std::vector<GlobalOutputIndex>;

struct BaseInput {
  /// Index of the block generating coins.
  Xi::Blockchain::Block::Height height;

  KV_BEGIN_SERIALIZATION
  KV_MEMBER(height)
  KV_END_SERIALIZATION
};

struct KeyInput {
  /// Amount of the used input, all used outputs must correspond to this amount.
  uint64_t amount;

  /// Delta encoded global indices of outputs used (real one + mixins)
  std::vector<uint32_t> outputIndices;

  /// Double spending protection.
  Crypto::KeyImage keyImage;

  KV_BEGIN_SERIALIZATION
  KV_MEMBER(amount)
  KV_MEMBER_RENAME(outputIndices, output_indices)
  KV_MEMBER_RENAME(keyImage, key_image)
  KV_END_SERIALIZATION
};

struct KeyOutput {
  /// Diffie Hellmann key exchange, derived from the epheremal keys and destination public key.
  Crypto::PublicKey key;

  KV_BEGIN_SERIALIZATION
  KV_MEMBER(key)
  KV_END_SERIALIZATION
};

typedef std::variant<BaseInput, KeyInput> TransactionInput;
typedef std::variant<KeyOutput> TransactionOutputTarget;

struct TransactionOutput {
  using GlobalIndex = uint64_t;

  uint64_t amount;
  TransactionOutputTarget target;

  KV_BEGIN_SERIALIZATION
  KV_MEMBER(amount)
  KV_MEMBER(target)
  KV_END_SERIALIZATION
};

using TrasnferUnlock = std::optional<Xi::Blockchain::Currency::Unlock>;
using TransferPaymentId = std::optional<Xi::Blockchain::Currency::PaymentIdentifier>;

struct TransferExtra {
  Crypto::PublicKey publicKey;
  TransferPaymentId paymnetId;
  TrasnferUnlock unlock;

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(publicKey, public_key)
  KV_MEMBER_RENAME(paymnetId, payment_id)
  KV_MEMBER(unlock)
  KV_END_SERIALIZATION

  void nullify();
};

struct TransferPrefix {
  uint16_t version;
  TransferExtra extra;
  std::vector<TransactionInput> inputs;
  std::vector<TransactionOutput> outputs;

  KV_BEGIN_SERIALIZATION
  KV_MEMBER(version)
  XI_RETURN_EC_IF_NOT(version == 1, false);
  KV_MEMBER(extra)
  KV_MEMBER(inputs)
  KV_MEMBER(outputs)
  KV_END_SERIALIZATION

  void nullify();
};

struct TransactionPrefix {
  KV_BEGIN_SERIALIZATION

  KV_END_SERIALIZATION

  void nullify();
};

struct Transaction {
  TransactionPrefix prefix;
  TransactionExchange exchange;

  std::vector<std::vector<Crypto::Signature>> signatures;

  Transaction();
  XI_DEFAULT_COPY(Transaction);
  XI_DEFAULT_MOVE(Transaction);
  explicit Transaction(const Xi::Blockchain::Currency::Transfer& transfer);

  void nullify();

  Xi::Result<Xi::Blockchain::Currency::Transfer> toTransfer() const;

  KV_BEGIN_SERIALIZATION
  KV_MEMBER(prefix)
  KV_MEMBER(exchange)
  KV_MEMBER(signatures)
  KV_END_SERIALIZATION
};
}  // namespace CryptoNote

XI_SERIALIZATION_VARIANT_TAG(CryptoNote::TransactionInput, 0, 0x01, "base_input")
XI_SERIALIZATION_VARIANT_TAG(CryptoNote::TransactionInput, 1, 0x02, "key_input")

XI_SERIALIZATION_VARIANT_TAG(CryptoNote::TransactionOutputTarget, 0, 0x01, "key_output")
