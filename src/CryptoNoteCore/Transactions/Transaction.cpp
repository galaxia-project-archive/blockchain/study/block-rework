﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "CryptoNoteCore/Transactions/Transaction.h"

CryptoNote::Transaction::Transaction() { nullify(); }

CryptoNote::Transaction::Transaction(const Xi::Blockchain::Currency::Transfer& transfer) {
  nullify();

  prefix.version = transfer.prefix().version().native();
  if (prefix.version != 1) {
    throw std::runtime_error{"unuspported version"};
  }

  prefix.extra.unlock = transfer.prefix().unlock();
  prefix.extra.paymnetId = transfer.prefix().paymentIdentifier();
  prefix.extra.publicKey = transfer.prefix().keyExchange();

  exchange.inputs.reserve(transfer.inputs().size());
  for (const auto& input : transfer.inputs()) {
    KeyInput iInput{};
    iInput.amount = input.amount().native();
    iInput.keyImage = input.keyImage();
    iInput.outputIndices = input.outputs();
    exchange.inputs.emplace_back(std::move(iInput));
  }

  exchange.outputs.reserve(transfer.outputs().size());
  for (const auto& output : transfer.outputs()) {
    KeyOutput iKeyOutput{};
    iKeyOutput.key = output.keyExchange();
    TransactionOutput iOutput{};
    iOutput.amount = output.amount().native();
    iOutput.target = std::move(iKeyOutput);
    exchange.outputs.emplace_back(std::move(iOutput));
  }
}

void CryptoNote::Transaction::nullify() {
  prefix.nullify();
  exchange.nullify();
  signatures.clear();
}

Xi::Result<Xi::Blockchain::Currency::Transfer> CryptoNote::Transaction::toTransfer() const {
  using namespace Xi;
  using namespace Xi::Blockchain;
  using namespace Xi::Blockchain::Consensus;

  using Xi::Blockchain::Currency::isCanonicalAmount;
  using Xi::Blockchain::Currency::Transfer;
  using Xi::Blockchain::Currency::TransferFeatures;
  using Xi::Blockchain::Currency::TransferVersion;

  XI_ERROR_TRY();
  XI_RETURN_EC_IF_NOT(prefix.version == 1, makeError(TransferError::UnsupportedVersion));
  Transfer reval{};
  reval.prefix().version(TransferVersion{1});
  reval.prefix().unlock(prefix.extra.unlock);
  reval.prefix().paymentIdentifier(prefix.extra.paymnetId);
  reval.prefix().staticRingSize(std::nullopt);
  reval.prefix().keyExchange(prefix.extra.publicKey);

  uint8_t minRingSize = std::numeric_limits<uint8_t>::max();
  uint8_t maxRingSize = 0;

  XI_RETURN_EC_IF(exchange.inputs.empty(), makeError(TransferError::EmptyInputs));
  XI_RETURN_EC_IF_NOT(exchange.inputs.size() == signatures.size(), makeError(TransferError::SignaturesInconsistency));
  reval.inputs().reserve(exchange.inputs.size());
  for (size_t i = 0; i < exchange.inputs.size(); ++i) {
    const auto& iInput = exchange.inputs[i];
    exceptional_if_not<InvalidVariantTypeError>(std::holds_alternative<KeyInput>(iInput),
                                                "only key inputs can be used in a transfer");
    const KeyInput& iKeyInput = std::get<KeyInput>(iInput);
    XI_RETURN_EC_IF_NOT(isCanonicalAmount(iKeyInput.amount), makeError(TransferError::NoneCanonicalAmount));
    XI_RETURN_EC_IF(iKeyInput.outputIndices.size() > 0b01111111, makeError(TransferError::InvalidRingSize));

    const auto& iRingSignature = signatures[i];
    XI_RETURN_EC_IF_NOT(iKeyInput.outputIndices.size() == iRingSignature.size(),
                        makeError(TransferError::SignaturesInconsistency));
    minRingSize = std::min(static_cast<uint8_t>(iRingSignature.size()), minRingSize);
    maxRingSize = std::max(static_cast<uint8_t>(iRingSignature.size()), maxRingSize);

    Blockchain::Currency::KeyInput input{};
    input.amount(Blockchain::Currency::CanonicalAmount{iKeyInput.amount});
    input.keyImage(iKeyInput.keyImage);
    input.outputs(iKeyInput.outputIndices);
    reval.outputs().emplace_back(std::move(input));
  }

  if (maxRingSize > 1) {
    reval.prefix().features(addFlag(reval.prefix().features(), TransferFeatures::Mixins));
    if (minRingSize == maxRingSize) {
      reval.prefix().staticRingSize(minRingSize);
    }
  }

  XI_RETURN_EC_IF(exchange.outputs.empty(), makeError(TransferError::EmptyOutputs));
  reval.outputs().reserve(exchange.outputs.size());
  for (size_t i = 0; i < exchange.outputs.size(); ++i) {
    const TransactionOutput& iOutput = exchange.outputs[i];
    XI_RETURN_EC_IF_NOT(isCanonicalAmount(iOutput.amount), makeError(TransferError::NoneCanonicalAmount));
    exceptional_if_not<InvalidVariantTypeError>(std::holds_alternative<KeyOutput>(iOutput.target),
                                                "only key outputs supported for transfers");
    const KeyOutput& iKeyOutput = std::get<KeyOutput>(iOutput.target);

    Blockchain::Currency::KeyOutput output{};
    output.amount(Blockchain::Currency::CanonicalAmount{iOutput.amount});
    output.keyExchange(iKeyOutput.key);
    reval.outputs().emplace_back(std::move(output));
  }

  return emplaceSuccess<Transfer>(std::move(reval));
  XI_ERROR_CATCH();
}

void CryptoNote::TransactionExtra::nullify() {
  publicKey.nullify();
  paymnetId = std::nullopt;
  unlock = std::nullopt;
}

void CryptoNote::TransactionPrefix::nullify() {
  version = 0;
  extra.nullify();
}

void CryptoNote::TransactionExchange::nullify() {
  inputs.clear();
  outputs.clear();
}
