﻿// Copyright (c) 2012-2017, The CryptoNote developers, The Bytecoin developers
//
// This file is part of Bytecoin.
//
// Bytecoin is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bytecoin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Bytecoin.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <vector>
#include <string>
#include <variant>

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <CryptoNoteCore/CryptoNote.h>

#define TX_EXTRA_PADDING_MAX_SIZE 64
#define TX_EXTRA_PADDING_MAX_COUNT 4
#define TX_EXTRA_NONCE_MAX_COUNT 126
#define TX_EXTRA_MAX_SIZE 255

#define TX_EXTRA_TAG_PADDING 0xFF
#define TX_EXTRA_TAG_PUBKEY 0x01
#define TX_EXTRA_TAG_NONCE 0x02

#define TX_EXTRA_NONCE_PAYMENT_ID 0x01
#define TX_EXTRA_NONCE_CUSTOM_ID 0xFF

namespace CryptoNote {

struct TransactionExtraPadding {
  uint32_t size = 0;
};

struct TransactionExtraPublicKey {
  Crypto::PublicKey publicKey = Crypto::PublicKey::Null;
};

struct TransactionExtraNonce {
  std::vector<uint8_t> nonce{};
};

// tx_extra_field format, except tx_extra_padding and tx_extra_pub_key:
//   varint tag;
//   varint size;
//   varint data[];
typedef std::variant<TransactionExtraPadding, TransactionExtraPublicKey, TransactionExtraNonce> TransactionExtraField;

template <typename T>
bool findTransactionExtraFieldByType(const std::vector<TransactionExtraField>& tx_extra_fields, T& field) {
  auto it = std::find_if(tx_extra_fields.begin(), tx_extra_fields.end(),
                         [](const TransactionExtraField& f) { return std::holds_alternative<T>(f); });

  if (tx_extra_fields.end() == it) return false;

  field = std::get<T>(*it);
  return true;
}

[[nodiscard]] bool parseTransactionExtra(const std::vector<uint8_t>& tx_extra,
                                         std::vector<TransactionExtraField>& tx_extra_fields);
bool writeTransactionExtra(std::vector<uint8_t>& tx_extra, const std::vector<TransactionExtraField>& tx_extra_fields);

Crypto::PublicKey getTransactionPublicKeyFromExtra(const std::vector<uint8_t>& tx_extra);
bool addTransactionPublicKeyToExtra(std::vector<uint8_t>& tx_extra, const Crypto::PublicKey& tx_pub_key);
bool addExtraNonceToTransactionExtra(std::vector<uint8_t>& tx_extra, const BinaryArray& extra_nonce);
void setPaymentIdToTransactionExtraNonce(BinaryArray& extra_nonce, const PaymentId& payment_id);
bool getPaymentIdFromTransactionExtraNonce(const BinaryArray& extra_nonce, PaymentId& payment_id);

bool createTxExtraWithPaymentId(const std::string& paymentIdString, std::vector<uint8_t>& extra);
// returns false if payment id is not found or parse error
bool getPaymentIdFromTxExtra(const std::vector<uint8_t>& extra, PaymentId& paymentId);
bool parsePaymentId(const std::string& paymentIdString, PaymentId& paymentId);

}  // namespace CryptoNote
