﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <memory>
#include <cinttypes>

#include <Xi/Global.hh>
#include <Serialization/ISerializer.h>
#include <Serialization/VariantSerialization.hpp>

#include "Xi/Blockchain/Consensus/IDifficultyAlgorithm.hpp"
#include "Xi/Blockchain/Consensus/FixedDifficulty.hpp"
#include "Xi/Blockchain/Consensus/LWMA2.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

struct LWMAConfiguration {
 public:
  XI_PROPERTY(uint8_t, version)
  XI_PROPERTY(LWMA2Configuration, configuration, null)

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(m_version, version)
  return this->version() == 2;
  KV_END_SERIALIZATION
};

// clang-format off
XI_SERIALIZATION_VARIANT_INVARIANT(
    DifficultyConfiguration,
      FixedDifficultyConfiguration,
      LWMAConfiguration
)
// clang-format on

std::shared_ptr<IDifficultyAlgorithm> makeDifficultyAlgorithm(const DifficultyConfiguration& config);

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi

XI_SERIALIZATION_VARIANT_TAG(Xi::Blockchain::Consensus::DifficultyConfiguration, 0, 0x00001, "fixed")
XI_SERIALIZATION_VARIANT_TAG(Xi::Blockchain::Consensus::DifficultyConfiguration, 1, 0x01001, "lwma")
