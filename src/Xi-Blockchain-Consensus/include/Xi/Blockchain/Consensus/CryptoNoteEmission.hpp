﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Serialization/ISerializer.h>
#include <Serialization/OptionalSerialization.hpp>

#include "Xi/Blockchain/Consensus/IEmission.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

class CryptoNoteEmissionConfiguration {
 public:
  // Maximun coins to emit
  XI_PROPERTY(std::optional<Currency::Amount>, totalSupply, 0)
  // Coins emitted every block
  XI_PROPERTY(uint32_t, speed, 0)

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(totalSupply(), total_supply)
  XI_RETURN_EC_IF_NOT(totalSupply().value_or(Currency::Amount{1}) > Currency::Amount{0}, false);
  KV_MEMBER_RENAME(speed(), rate)
  XI_RETURN_EC_IF_NOT(speed() <= 8 * sizeof(uint64_t), false);
  KV_END_SERIALIZATION

  CryptoNoteEmissionConfiguration(Null = null) {}
};

class CryptoNoteEmission final : public IEmission {
 public:
  CryptoNoteEmission(const CryptoNoteEmissionConfiguration& config);
  ~CryptoNoteEmission() override = default;

  XI_PROPERTY(CryptoNoteEmissionConfiguration, configuration)

  [[nodiscard]] uint16_t windowSize() const override;
  [[nodiscard]] Currency::Amount totalSupply() const override;
  [[nodiscard]] Currency::Amount operator()(Cache::ConstBlockInfoSpan previousBlockInfos) const override;
};

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
