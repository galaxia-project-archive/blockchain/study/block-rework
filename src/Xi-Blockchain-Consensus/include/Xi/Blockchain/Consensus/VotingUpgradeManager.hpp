﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <Xi/Global.hh>
#include <Serialization/ISerializer.h>
#include <Serialization/OptionalSerialization.hpp>
#include <Xi/Blockchain/Block/Height.hpp>
#include <Xi/Blockchain/Cache/BlockInfo.hpp>

#include "Xi/Blockchain/Consensus/IUpgradeManager.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

class VotingUpgradeManagerConfiguration {
 public:
  XI_PROPERTY(Block::Version, version, Block::Version::Null)
  XI_PROPERTY(std::optional<Block::Height>, minimumHeight, std::nullopt)
  XI_PROPERTY(uint16_t, windowSize, 0)
  XI_PROPERTY(uint16_t, commitment, 0)

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(m_version, version)
  XI_RETURN_EC_IF(version().isNull(), false);
  KV_MEMBER_RENAME(m_minimumHeight, minimum_height)
  XI_RETURN_EC_IF(minimumHeight().value_or(Block::Height::Genesis).isNull(), false);
  KV_MEMBER_RENAME(m_windowSize, window_size)
  XI_RETURN_EC_IF_NOT(windowSize() > 0, false);
  KV_MEMBER_RENAME(m_commitment, commitment)
  XI_RETURN_EC_IF_NOT(commitment() > 0, false);
  XI_RETURN_EC_IF(commitment() > windowSize(), false);
  KV_END_SERIALIZATION

 public:
  VotingUpgradeManagerConfiguration(Null = null) {}
};

class VotingUpgradeManager final : public IUpgradeManager {
 public:
  XI_PROPERTY(VotingUpgradeManagerConfiguration, configuration, null)

 public:
  VotingUpgradeManager(const VotingUpgradeManagerConfiguration& config);
  ~VotingUpgradeManager() override = default;

  [[nodiscard]] uint16_t windowSize() const override;
  [[nodiscard]] Block::Version expectedVersion(Cache::ConstBlockInfoSpan previousBlockInfos,
                                               const Block::Height& currentHeight,
                                               const Block::Version& currentVersion) const override;
};

XI_DECLARE_SMART_POINTER(VotingUpgradeManager)

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
