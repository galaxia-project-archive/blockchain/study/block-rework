﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <Xi/Global.hh>

#include "Xi/Blockchain/Consensus/ITimeSynchronizer.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

class MedianTimeSynchronizerConfiguration {
 public:
  XI_PROPERTY(uint32_t, futureLimit, 0)
  XI_PROPERTY(uint16_t, windowSize, 0)

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(m_windowSize, windows_size)
  XI_RETURN_EC_IF_NOT(windowSize() > 0, false);
  KV_MEMBER_RENAME(m_futureLimit, future_limit)
  KV_END_SERIALIZATION

  MedianTimeSynchronizerConfiguration(Null = null) {}
};

class MedianTimeSynchronizer final : public ITimeSynchronizer {
 public:
  MedianTimeSynchronizer(const MedianTimeSynchronizerConfiguration& config);
  ~MedianTimeSynchronizer() override = default;

  XI_PROPERTY(MedianTimeSynchronizerConfiguration, configuration, null)

  [[nodiscard]] uint16_t windowSize() const override;
  [[nodiscard]] EligibleTimestampRange operator()(Cache::ConstBlockInfoSpan previousBlockInfos,
                                                  const Block::Timestamp& now) const override;
};

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
