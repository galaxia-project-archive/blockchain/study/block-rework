﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Serialization/VariantSerialization.hpp>

#include "Xi/Blockchain/Consensus/IUpgradeManager.hpp"
#include "Xi/Blockchain/Consensus/FixedHeightUpgradeManager.hpp"
#include "Xi/Blockchain/Consensus/VotingUpgradeManager.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

// clang-format off
XI_SERIALIZATION_VARIANT_INVARIANT(
    UpgradeManagerConfiguration,
      FixedHeightUpgradeManagerConfiguration,
      VotingUpgradeManagerConfiguration
)
// clang-format on

SharedIUpgradeManager makeUpgradeManager(const UpgradeManagerConfiguration& config);

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi

XI_SERIALIZATION_VARIANT_TAG(Xi::Blockchain::Consensus::UpgradeManagerConfiguration, 0, 0x0001, "fixed_height")
XI_SERIALIZATION_VARIANT_TAG(Xi::Blockchain::Consensus::UpgradeManagerConfiguration, 1, 0x0002, "voting")
