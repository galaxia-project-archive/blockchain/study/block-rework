﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Serialization/ISerializer.h>

#include "Xi/Blockchain/Consensus/IRingSizeStrategy.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

class ScalingRingSizeStrategyConfiguration {
 public:
  XI_PROPERTY(Currency::RingSize, minimum)   ///< Ring size is enforced to be one untile minimum is reached.
  XI_PROPERTY(Currency::RingSize, maximum)   ///< Scaling stops once maximum is reached.
  XI_PROPERTY(Currency::RingSize, stepSize)  ///< Each stepSize availabe outs required ring size is increased by 1

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(minimum(), minimum)
  KV_MEMBER_RENAME(maximum(), maximum)
  XI_RETURN_EC_IF_NOT(minimum() <= maximum(), false);
  KV_MEMBER_RENAME(stepSize(), step_size)
  XI_RETURN_EC_IF(stepSize() < Currency::RingSize{2}, false);
  KV_END_SERIALIZATION
};

class ScalingRingSizeStrategy final : public IRingSizeStrategy {
 public:
  explicit ScalingRingSizeStrategy(const ScalingRingSizeStrategyConfiguration& config);
  ~ScalingRingSizeStrategy() override = default;

  XI_PROPERTY(ScalingRingSizeStrategyConfiguration, configuration)

  [[nodiscard]] bool isFeasible(const Currency::Amount& amount, const Currency::RingSize& ringSize) const override;
  [[nodiscard]] Currency::RingSize operator()(const Currency::Amount& amount,
                                              const uint64_t availabeOuts) const override;
};

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
