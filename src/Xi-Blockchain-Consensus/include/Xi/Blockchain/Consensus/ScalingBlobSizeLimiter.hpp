﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Serialization/ISerializer.h>

#include "Xi/Blockchain/Consensus/IBlobSizeLimiter.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

class ScalingBlobSizeLimiterConfiguration {
 public:
  XI_PROPERTY(uint16_t, windowSize, 0)
  XI_PROPERTY(uint64_t, lowerBound, 0)
  XI_PROPERTY(uint64_t, upperBound, 0)
  XI_PROPERTY(uint64_t, scalingRate, 0)

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(m_windowSize, window_size)
  XI_RETURN_EC_IF_NOT(windowSize() > 0, false);
  KV_MEMBER_RENAME(m_lowerBound, lower_bound)
  XI_RETURN_EC_IF_NOT(lowerBound() > 0, false);
  KV_MEMBER_RENAME(m_upperBound, upper_bound)
  XI_RETURN_EC_IF_NOT(upperBound() > 0, false);
  XI_RETURN_EC_IF_NOT(lowerBound() < upperBound(), false);
  KV_MEMBER_RENAME(m_scalingRate, scaling_rate)
  XI_RETURN_EC_IF_NOT(scalingRate() > 0, false);
  KV_END_SERIALIZATION

  ScalingBlobSizeLimiterConfiguration(Null = null) {}
};

class ScalingBlobSizeLimiter final : public IBlobSizeLimiter {
 public:
  explicit ScalingBlobSizeLimiter(const ScalingBlobSizeLimiterConfiguration& config);
  ~ScalingBlobSizeLimiter() override = default;

  XI_PROPERTY(ScalingBlobSizeLimiterConfiguration, configuration, null)

  uint16_t windowSize() const override;
  uint64_t operator()(Cache::ConstBlockInfoSpan) override;
};

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
