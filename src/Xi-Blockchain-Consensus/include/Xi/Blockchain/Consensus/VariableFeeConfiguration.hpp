﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <Xi/Global.hh>
#include <Serialization/ISerializer.h>

#include "Xi/Blockchain/Currency/Amount.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

struct VariableFeeConfiguration {
  XI_PROPERTY(Currency::Amount, minimumFee)

  XI_PROPERTY(uint64_t, bucketFreeCount)
  XI_PROPERTY(Currency::Amount, bucketFeeRate)

  XI_PROPERTY(uint64_t, inputRateNumerator)
  XI_PROPERTY(uint64_t, inputRateDenominator)
  XI_PROPERTY(Currency::Amount, inputFeeRate)

  XI_PROPERTY(uint64_t, outputRateNumerator)
  XI_PROPERTY(uint64_t, outputRateDenominator)
  XI_PROPERTY(Currency::Amount, outputFeeRate)

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(m_minimumFee, "minimum_fee")

  KV_MEMBER_RENAME(m_bucketFreeCount, "buecket_free_count")
  KV_MEMBER_RENAME(m_bucketFeeRate, "bucket_fee_rate")

  KV_MEMBER_RENAME(m_inputRateNumerator, "input_rate_numerator");
  KV_MEMBER_RENAME(m_inputRateDenominator, "input_rate_denominator")
  KV_MEMBER_RENAME(m_inputFeeRate, "input_fee_rate")

  KV_MEMBER_RENAME(m_outputRateNumerator, "output_rate_numerator")
  KV_MEMBER_RENAME(m_outputRateDenominator, "output_rate_denominator")
  KV_MEMBER_RENAME(m_outputFeeRate, "output_fee_rate")
  KV_END_SERIALIZATION
};

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
