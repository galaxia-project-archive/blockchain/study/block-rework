﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>

#include "Xi/Blockchain/Block/Height.hpp"
#include "Xi/Blockchain/Consensus/IUpgradeManager.hpp"
#include "Xi/Blockchain/Cache/BlockInfo.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

class FixedHeightUpgradeManagerConfiguration {
 public:
  XI_PROPERTY(Block::Height, height, null)
  XI_PROPERTY(Block::Version, version, 0)

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(m_height, height)
  KV_MEMBER_RENAME(m_version, version)
  KV_END_SERIALIZATION

 public:
  FixedHeightUpgradeManagerConfiguration(Null = null) {}
};

class FixedHeightUpgradeManager final : public IUpgradeManager {
 public:
  XI_PROPERTY(FixedHeightUpgradeManagerConfiguration, configuration, null)

 public:
  FixedHeightUpgradeManager(const FixedHeightUpgradeManagerConfiguration& config);
  ~FixedHeightUpgradeManager() override = default;

  [[nodiscard]] uint16_t windowSize() const override;
  [[nodiscard]] Block::Version expectedVersion(Cache::ConstBlockInfoSpan previousBlockInfos,
                                               const Block::Height& currentHeight,
                                               const Block::Version& currentVersion) const override;
};

XI_DECLARE_SMART_POINTER(FixedHeightUpgradeManager)

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
