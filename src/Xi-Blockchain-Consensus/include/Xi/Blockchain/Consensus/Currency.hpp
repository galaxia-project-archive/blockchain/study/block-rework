﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <optional>

#include <Xi/Global.hh>
#include <Xi/ErrorCode.hpp>

#include <Serialization/ISerializer.h>

#include "Xi/Blockchain/Consensus/BlobSizeLimiter.hpp"
#include "Xi/Blockchain/Consensus/FeeStrategy.hpp"
#include "Xi/Blockchain/Consensus/Reward.hpp"
#include "Xi/Blockchain/Consensus/Transfer.hpp"
#include "Xi/Blockchain/Consensus/StaticReward.hpp"
#include "Xi/Blockchain/Consensus/Emission.hpp"
#include "Xi/Blockchain/Consensus/RingSizeStrategy.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

class CurrencyConfiguration {
 public:
  XI_PROPERTY(TransferConfiguration, transfer)
  XI_PROPERTY(RingSizeStrategyConfiguration, ringSize)
  XI_PROPERTY(std::optional<RewardConfiguration>, reward)
  XI_PROPERTY(std::optional<StaticReward>, staticReward)
  XI_PROPERTY(std::optional<EmissionConfiguration>, emission)

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(transfer(), transfer)
  KV_MEMBER_RENAME(ringSize(), ring_size)
  KV_MEMBER_RENAME(reward(), reward)
  KV_MEMBER_RENAME(staticReward(), static_reward)
  KV_MEMBER_RENAME(emission(), emission)
  KV_END_SERIALIZATION

 public:
  uint16_t blockInfoWindowSize() const;

  const IEmission* emitter() const;
  const IRingSizeStrategy& ringSizeStrategy() const;

 private:
  mutable SharedIEmission m_emitter{nullptr};
  mutable SharedIRingSizeStrategy m_ringSizeStrategy{nullptr};
};

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
