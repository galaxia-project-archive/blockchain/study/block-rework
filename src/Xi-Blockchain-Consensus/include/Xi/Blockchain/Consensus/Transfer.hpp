﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>

#include "Xi/Blockchain/Consensus/BlobSizeLimiter.hpp"
#include "Xi/Blockchain/Consensus/FeeStrategy.hpp"
#include "Xi/Blockchain/Consensus/Reward.hpp"
#include "Xi/Blockchain/Consensus/Transfer.hpp"
#include "Xi/Blockchain/Consensus/StaticReward.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

class TransferConfiguration {
 public:
  XI_PROPERTY(FeeConfiguration, fees)
  XI_PROPERTY(BlobSizeLimiterConfiguration, sizeLimit)

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(fees(), fees)
  KV_MEMBER_RENAME(sizeLimit(), size_limit)
  KV_END_SERIALIZATION

 public:
  const IFeeStrategy& feeStrategy() const;
  const IBlobSizeLimiter& sizeLimiter() const;

 private:
  mutable SharedIFeeStrategy m_feeStrategy{nullptr};
  mutable SharedIBlobSizeLimiter m_blobSizeLimiter{nullptr};
};

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
