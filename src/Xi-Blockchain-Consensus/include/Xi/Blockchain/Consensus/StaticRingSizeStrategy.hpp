﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Serialization/ISerializer.h>

#include "Xi/Blockchain/Consensus/IRingSizeStrategy.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

class StaticRingSizeStrategyConfiguration {
 public:
  XI_PROPERTY(Currency::RingSize, size)  ///< ring size enforced on all inputs

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(size(), size)
  KV_END_SERIALIZATION
};

class StaticRingSizeStrategy final : public IRingSizeStrategy {
 public:
  explicit StaticRingSizeStrategy(const StaticRingSizeStrategyConfiguration& config);
  ~StaticRingSizeStrategy() override = default;

  XI_PROPERTY(StaticRingSizeStrategyConfiguration, configuration)

  [[nodiscard]] bool isFeasible(const Currency::Amount& amount, const Currency::RingSize& ringSize) const override;
  [[nodiscard]] Currency::RingSize operator()(const Currency::Amount& amount,
                                              const uint64_t availabeOuts) const override;
};

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
