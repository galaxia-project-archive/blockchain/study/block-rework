﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>

#include "Xi/Blockchain/Currency/Transfer.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

class IRingSizeStrategy {
 public:
  virtual ~IRingSizeStrategy() = 0;

  [[nodiscard]] virtual bool isFeasible(const Currency::Amount& amount, const Currency::RingSize& ringSize) const = 0;
  [[nodiscard]] virtual Currency::RingSize operator()(const Currency::Amount& amount,
                                                      const uint64_t availabeOuts) const = 0;
};

XI_DECLARE_SMART_POINTER(IRingSizeStrategy)

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
