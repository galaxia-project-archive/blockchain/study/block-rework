﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <optional>

#include <Xi/ErrorCode.hpp>
#include <Xi/Crypto/Random/KeyGenerator.hpp>

#include "Xi/Blockchain/Block.hpp"
#include "Xi/Blockchain/Currency/Currency.hpp"
#include "Xi/Blockchain/ProofOfWork/ProofOfWork.hpp"
#include "Xi/Blockchain/Consensus/Version.hpp"
#include "Xi/Blockchain/Consensus/Hash.hpp"
#include "Xi/Blockchain/Consensus/ProofOfWorkError.hpp"
#include "Xi/Blockchain/Consensus/MergeMining.hpp"
#include "Xi/Blockchain/Consensus/MergeMiningError.hpp"
#include "Xi/Blockchain/Consensus/Reward.hpp"
#include "Xi/Blockchain/Consensus/RewardError.hpp"
#include "Xi/Blockchain/Consensus/StaticReward.hpp"
#include "Xi/Blockchain/Consensus/StaticRewardError.hpp"
#include "Xi/Blockchain/Consensus/Transfer.hpp"
#include "Xi/Blockchain/Consensus/TransferError.hpp"
#include "Xi/Blockchain/Consensus/UpgradeError.hpp"
#include "Xi/Blockchain/Consensus/TimeSynchronizer.hpp"
#include "Xi/Blockchain/Consensus/TimeError.hpp"
#include "Xi/Blockchain/Consensus/BlobSizeError.hpp"
#include "Xi/Blockchain/Consensus/EmissionError.hpp"
#include "Xi/Blockchain/Consensus/UpgradeManager.hpp"
#include "Xi/Blockchain/Consensus/DifficultyAlgorithm.hpp"
#include "Xi/Blockchain/Consensus/Currency.hpp"
#include "Xi/Blockchain/Consensus/CurrencyError.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

XI_ERROR_CODE_BEGIN(Consensus)

/// Consensus for given version does not exist.
XI_ERROR_CODE_VALUE(NotFound, 0x0001)

/// Unable to read/write from/to binary format
XI_ERROR_CODE_VALUE(SerializationFailure, 0x0002)

/// Previous block is not known.
XI_ERROR_CODE_VALUE(Orphan, 0x0003)

/// Should not happen but better than throwing an exception.
XI_ERROR_CODE_VALUE(Internal, 0x00FF)

XI_ERROR_CODE_END(Consensus, NoneSpecializedConsensusError)

class Consensus {
 public:
  XI_PROPERTY(Version, version, 0)
  XI_PROPERTY(std::optional<MergeMining>, mergeMining, std::nullopt)

  // maximum previous block infos required for all configrations.
  uint16_t blockInfoWindowSize() const;

  // use null to disable crypto currency^^
  XI_PROPERTY(std::optional<CurrencyConfiguration>, currency)

  // Proof of Work and Block Time
  XI_PROPERTY(DifficultyConfiguration, difficulty)
  const IDifficultyAlgorithm& difficultyAlgorithm() const;
  const ProofOfWork::IProofOfWork& proofOfWorkHash() const;

  // Upgrade
  XI_PROPERTY(std::optional<UpgradeManagerConfiguration>, upgrade, std::nullopt)
  const IUpgradeManager* upgradeManager() const;

  // Time
  XI_PROPERTY(TimeSynchronizerConfiguration, time, null)
  const ITimeSynchronizer& timeSynchronizer() const;

 private:
  mutable std::shared_ptr<Crypto::Random::IKeyGenerator> m_keyGenerator;
  mutable std::shared_ptr<IDifficultyAlgorithm> m_difficultyAlgorithm;
  mutable std::shared_ptr<ProofOfWork::IProofOfWork> m_proofOfWorkHash;
  mutable std::shared_ptr<IUpgradeManager> m_upgradeManager;
  mutable std::shared_ptr<ITimeSynchronizer> m_timeSynchronizer;
};

XI_DECLARE_SMART_POINTER(Consensus)

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Blockchain::Consensus, Consensus)
