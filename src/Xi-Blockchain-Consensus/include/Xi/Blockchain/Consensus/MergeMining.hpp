﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <Xi/Global.hh>
#include <Xi/ErrorCode.hpp>
#include <Serialization/ISerializer.h>

namespace Xi {
namespace Blockchain {
namespace Consensus {

class MergeMining {
 public:
  XI_PROPERTY(uint64_t, chainCountMinimum, 0)
  XI_PROPERTY(uint64_t, chainCountMaximum, 0)

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(m_chainCountMinimum, "chain_count_minimum")
  XI_RETURN_EC_IF_NOT(chainCountMinimum() > 1 && chainCountMinimum() <= 32, false);
  KV_MEMBER_RENAME(m_chainCountMaximum, "chain_count_maximum")
  XI_RETURN_EC_IF_NOT(chainCountMaximum() > 1 && chainCountMaximum() <= 32, false);
  XI_RETURN_EC_IF(chainCountMaximum() > chainCountMinimum(), false);
  KV_END_SERIALIZATION
};

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
