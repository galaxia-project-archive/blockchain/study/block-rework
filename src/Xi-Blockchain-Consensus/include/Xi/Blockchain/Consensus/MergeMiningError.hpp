﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/ErrorCode.hpp>

namespace Xi {
namespace Blockchain {
namespace Consensus {

XI_ERROR_CODE_BEGIN(MergeMining)

XI_ERROR_CODE_VALUE(Empty, 0x0001)
XI_ERROR_CODE_VALUE(TooMany, 0x0002)
XI_ERROR_CODE_VALUE(TooFew, 0x0003)
XI_ERROR_CODE_VALUE(Missing, 0x0004)
XI_ERROR_CODE_VALUE(Disabled, 0x0005)
XI_ERROR_CODE_VALUE(Inconsistent, 0x0006)
XI_ERROR_CODE_VALUE(NullHash, 0x0007)

XI_ERROR_CODE_END(MergeMining, MergeMiningError)

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Blockchain::Consensus, MergeMining)
