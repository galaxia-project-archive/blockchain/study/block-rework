﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <optional>

#include <Xi/Global.hh>
#include <Xi/ErrorCode.hpp>
#include <Serialization/ISerializer.h>
#include <Serialization/OptionalSerialization.hpp>

#include "Xi/Blockchain/Currency/Reward.hpp"
#include "Xi/Blockchain/Consensus/BlobSizeLimiter.hpp"
#include "Xi/Blockchain/Consensus/Emission.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

class RewardConfiguration {
 public:
  // If provided only the higher digits of the reward can be generated, others are cutted off.
  XI_PROPERTY(std::optional<uint16_t>, maximumDigits, std::nullopt)

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(maximumDigits(), maximum_digits)
  // Cutting all digits would always yield an empty reward.
  XI_RETURN_EC_IF_NOT(maximumDigits().value_or(64) > 0, false);
  KV_END_SERIALIZATION

  RewardConfiguration(Null = null) {}
};

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi

namespace CryptoNote {
using BlockReward = Xi::Blockchain::Currency::Reward;
}
