﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <Xi/Global.hh>
#include <Serialization/ISerializer.h>

#include "Xi/Blockchain/Consensus/IDifficultyAlgorithm.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

class LWMA2Configuration {
 public:
  XI_PROPERTY(uint16_t, windowSize, 0)
  XI_PROPERTY(uint32_t, blockTime, 0)
  XI_PROPERTY(ProofOfWork::Difficulty, initialDifficulty, 0)

  bool isValid() const;

  [[nodiscard]] bool serialize(CryptoNote::ISerializer& serializer);

  LWMA2Configuration(Null = null) {}
};

class LWMA2 final : public IDifficultyAlgorithm {
 public:
  explicit LWMA2(const LWMA2Configuration& config);
  ~LWMA2() override = default;

 public:
  XI_PROPERTY(LWMA2Configuration, configuration)

 public:
  [[nodiscard]] virtual uint16_t windowSize() const override;
  [[nodiscard]] virtual ProofOfWork::Difficulty nextDifficulty(Cache::ConstBlockInfoSpan infos) const override;
};

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
