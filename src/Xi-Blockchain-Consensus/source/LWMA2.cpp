﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Consensus/LWMA2.hpp"

#include <cassert>
#include <limits>

namespace Xi {
namespace Blockchain {
namespace Consensus {

bool LWMA2Configuration::isValid() const {
  XI_RETURN_EC_IF_NOT(windowSize() > 0, false);
  XI_RETURN_EC_IF_NOT(blockTime() > 0, false);
  XI_RETURN_EC_IF_NOT(initialDifficulty().isValid(), false);
  XI_RETURN_SC(true);
}

bool LWMA2Configuration::serialize(CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer(windowSize(), "window_size"), false);
  XI_RETURN_EC_IF_NOT(serializer(blockTime(), "window_size"), false);
  XI_RETURN_EC_IF_NOT(serializer(initialDifficulty(), "initial_difficulty"), false);
  if (!isValid()) {
    XI_RETURN_EC(false);
  } else {
    XI_RETURN_SC(true);
  }
}

LWMA2::LWMA2(const LWMA2Configuration &config) : m_configuration{config} {}

uint16_t LWMA2::windowSize() const { return configuration().windowSize() + 1; }

// LWMA-2 difficulty algorithm
// Copyright (c) 2017-2018 Zawy, MIT License
// https://github.com/zawy12/difficulty-algorithms/issues/3
ProofOfWork::Difficulty LWMA2::nextDifficulty(Cache::ConstBlockInfoSpan infos) const {
  assert(configuration().isValid());
  const auto T = static_cast<int64_t>(configuration().blockTime());
  const auto N = static_cast<int64_t>(configuration().windowSize());

  XI_RETURN_SC_IF(infos.size() < windowSize() + 1U, configuration().initialDifficulty());

  std::vector<uint64_t> timestamps{};
  timestamps.resize(windowSize());
  std::vector<uint64_t> cumulativeDifficulties{};
  cumulativeDifficulties.resize(windowSize());

  for (size_t i = 0; i < windowSize(); ++i) {
    const auto &iInfo = infos[i];
    timestamps[i] = iInfo.timestamp.native();
    cumulativeDifficulties[i] = iInfo.cumulativeDifficulty.native();
  }

  int64_t L(0), ST, sum_3_ST(0), next_D, prev_D;

  for (uint32_t i = 1; i <= N; i++) {
    ST = static_cast<int64_t>(timestamps[i]) - static_cast<int64_t>(timestamps[i - 1]);

    ST = std::max(-4 * static_cast<int64_t>(T), std::min(ST, 6 * static_cast<int64_t>(T)));

    L += ST * i;

    if (i > N - 3) {
      sum_3_ST += ST;
    }
  }

  next_D = (static_cast<int64_t>(cumulativeDifficulties[static_cast<uint32_t>(N)] - cumulativeDifficulties[0]) * T *
            (N + 1) * 99) /
           (100 * 2 * L);

  prev_D = static_cast<int64_t>(cumulativeDifficulties[static_cast<uint32_t>(N)]) -
           static_cast<int64_t>(cumulativeDifficulties[static_cast<uint32_t>(N) - 1]);

  next_D = std::max((prev_D * 67) / 100, std::min(next_D, (prev_D * 150) / 100));

  if (sum_3_ST < (8 * T) / 10) {
    next_D = std::max(next_D, (prev_D * 108) / 100);
  }

  ProofOfWork::Difficulty reval{static_cast<uint64_t>(next_D)};
  assert(reval.isValid());
  return reval;
}

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
