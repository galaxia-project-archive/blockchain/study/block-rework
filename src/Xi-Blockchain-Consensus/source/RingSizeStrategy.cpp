﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Consensus/RingSizeStrategy.hpp"

#include <Xi/Exceptions.hpp>

namespace Xi {
namespace Blockchain {

Consensus::SharedIRingSizeStrategy Consensus::makeRingSizeStrategy(const RingSizeStrategyConfiguration &config) {
  if (const auto stat = std::get_if<StaticRingSizeStrategyConfiguration>(std::addressof(config))) {
    return std::make_shared<StaticRingSizeStrategy>(*stat);
  } else if (const auto scale = std::get_if<ScalingRingSizeStrategyConfiguration>(std::addressof(config))) {
    return std::make_shared<ScalingRingSizeStrategy>(*scale);
  } else {
    exceptional<InvalidVariantTypeError>("unknown ring size strategy configuration");
  }
}

}  // namespace Blockchain
}  // namespace Xi
