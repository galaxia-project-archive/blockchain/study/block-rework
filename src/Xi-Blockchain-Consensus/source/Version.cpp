﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Consensus/Version.hpp"

#include <Xi/Exceptions.hpp>

const Xi::Blockchain::Consensus::Version Xi::Blockchain::Consensus::Version::Null{0};

bool Xi::Blockchain::Consensus::serialize(Xi::Blockchain::Consensus::Version &version, Common::StringView name,
                                          CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer(version.value, name), false);
  XI_RETURN_EC_IF(version.isNull() || version.native() > 0b01111111, false);
  XI_RETURN_SC(true);
}

std::string Xi::Blockchain::Consensus::toString(const Xi::Blockchain::Consensus::Version version) {
  return std::string{"v"} + std::to_string(version.value);
}

bool Xi::Blockchain::Consensus::Version::isNull() const { return *this == Null; }
