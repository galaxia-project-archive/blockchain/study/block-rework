﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Consensus/VariableFeeStrategy.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

Currency::Amount VariableFeeStrategy::operator()(Currency::ConstAmountSpan inputs,
                                                 Currency::ConstAmountSpan outputs) const {
  const auto config = configuration();

  auto numBuckets = Currency::countCanonicalAmountBuckets(outputs);
  if (numBuckets < config.bucketFreeCount()) {
    numBuckets = 0;
  } else {
    numBuckets = numBuckets - config.bucketFreeCount();
  }
  const auto bucketsFeePortion = numBuckets * config.bucketFeeRate().native();

  const auto numInputs = inputs.size();
  const auto inputsFeePortion =
      ((numInputs * config.inputRateNumerator()) / config.inputRateDenominator()) * config.inputFeeRate().native();

  const auto numOutputs = outputs.size();
  const auto outputsFeePortion =
      ((numOutputs * config.outputRateNumerator()) / config.outputRateDenominator()) * config.outputFeeRate().native();

  const auto fee = config.minimumFee().native() + inputsFeePortion + outputsFeePortion + bucketsFeePortion;
  return Currency::Amount{fee};
}

bool VariableFeeStrategy::isFeasible(const Currency::Amount fee) const { return fee >= configuration().minimumFee(); }

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
