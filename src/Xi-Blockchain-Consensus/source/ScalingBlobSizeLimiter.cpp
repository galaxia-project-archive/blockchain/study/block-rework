﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Consensus/ScalingBlobSizeLimiter.hpp"

#include <numeric>
#include <algorithm>
#include <iterator>

namespace Xi {
namespace Blockchain {
namespace Consensus {

ScalingBlobSizeLimiter::ScalingBlobSizeLimiter(const ScalingBlobSizeLimiterConfiguration& config)
    : m_configuration{config} {}

uint16_t ScalingBlobSizeLimiter::windowSize() const { return configuration().windowSize() + 1; }

uint64_t ScalingBlobSizeLimiter::operator()(Cache::ConstBlockInfoSpan previousBlockInfos) {
  const auto min = std::min({
      configuration().lowerBound() + configuration().scalingRate(),
      configuration().upperBound(),
  });
  XI_RETURN_SC_IF(previousBlockInfos.size() < 2, min);
  if (previousBlockInfos.size() > windowSize()) {
    previousBlockInfos = previousBlockInfos.range(previousBlockInfos.size() - windowSize());
  }

  auto average =
      std::accumulate(std::next(previousBlockInfos.begin()), previousBlockInfos.end(), 0ULL,
                      [](const auto acc, const auto& iBlockInfo) { return acc + iBlockInfo.cumulativeSize; });
  XI_RETURN_EC_IF_NOT(average > 0, min);
  XI_RETURN_EC_IF(average < (previousBlockInfos.size() - 1) * previousBlockInfos.begin()->cumulativeSize, min);
  average = (average - (previousBlockInfos.size() - 1) * previousBlockInfos.begin()->cumulativeSize) /
            previousBlockInfos.size();
  return std::min({std::max<uint64_t>({
                       min,
                       average + configuration().scalingRate(),
                   }),
                   configuration().upperBound()});
}

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
