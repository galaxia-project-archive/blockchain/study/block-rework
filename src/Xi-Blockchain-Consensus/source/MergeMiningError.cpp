﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Consensus/MergeMiningError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Blockchain::Consensus, MergeMining)
XI_ERROR_CODE_DESC(Empty, "merge mining tag is empty")
XI_ERROR_CODE_DESC(TooMany, "too many chains mined using merge mining")
XI_ERROR_CODE_DESC(TooFew, "not enough chains mined using merge mining")
XI_ERROR_CODE_DESC(Missing, "merge mining tag required but missing")
XI_ERROR_CODE_DESC(Disabled, "merge mining tag avaialable but disable by consensus")
XI_ERROR_CODE_DESC(Inconsistent, "header setup is inconsistent (ie. feature flag set but value not provided")
XI_ERROR_CODE_DESC(NullHash, "one of the merge mining hashes is null")
XI_ERROR_CODE_CATEGORY_END()
