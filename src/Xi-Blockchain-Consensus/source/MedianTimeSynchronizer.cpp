﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Consensus/MedianTimeSynchronizer.hpp"

#include <cassert>
#include <set>
#include <algorithm>

#include <Xi/Algorithm/Math.h>

namespace Xi {
namespace Blockchain {
namespace Consensus {

MedianTimeSynchronizer::MedianTimeSynchronizer(const MedianTimeSynchronizerConfiguration &config)
    : m_configuration{config} {}

uint16_t MedianTimeSynchronizer::windowSize() const { return configuration().windowSize(); }

EligibleTimestampRange MedianTimeSynchronizer::operator()(Cache::ConstBlockInfoSpan previousBlockInfos,
                                                          const Block::Timestamp &now) const {
  assert(now.isValid());
  EligibleTimestampRange reval{null};
  if (previousBlockInfos.empty()) {
    reval.minimum(std::nullopt);
  } else {
    std::set<uint64_t> timestamps{};
    std::transform(previousBlockInfos.begin(), previousBlockInfos.end(), std::inserter(timestamps, timestamps.begin()),
                   [](const auto &iInfo) { return iInfo.timestamp.native(); });
    reval.minimum(Block::Timestamp{median(timestamps)});
  }
  reval.maximum(Block::Timestamp{now.native() + configuration().futureLimit()});
  return reval;
}

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
