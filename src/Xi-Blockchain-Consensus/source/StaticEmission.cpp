﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Consensus/StaticEmission.hpp"

#include <cassert>
#include <limits>

namespace Xi {
namespace Blockchain {
namespace Consensus {

StaticEmission::StaticEmission(const StaticEmissionConfiguration &config) : m_configuration{config} {}

uint16_t StaticEmission::windowSize() const { return 1; }

Currency::Amount StaticEmission::totalSupply() const {
  return configuration().totalSupply().value_or(Currency::Amount{Currency::Amount::max});
}

Currency::Amount StaticEmission::operator()(Cache::ConstBlockInfoSpan previousBlockInfos) const {
  XI_RETURN_EC_IF(previousBlockInfos.empty(), Currency::Amount{0});
  const auto circulatingSupply = previousBlockInfos.back().cumulativeEmission;
  const auto maxSupply = totalSupply();
  XI_RETURN_EC_IF_NOT(circulatingSupply <= maxSupply, Currency::Amount{0});
  const auto leftToEmit = maxSupply - circulatingSupply;
  XI_RETURN_SC_IF(leftToEmit == Currency::Amount{0}, Currency::Amount{0});
  XI_RETURN_SC_IF(leftToEmit < configuration().rate(), leftToEmit);
  XI_RETURN_SC(configuration().rate());
}

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
