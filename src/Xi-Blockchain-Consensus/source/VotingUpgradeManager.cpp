﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Consensus/VotingUpgradeManager.hpp"

#include <cassert>

namespace Xi {
namespace Blockchain {
namespace Consensus {

VotingUpgradeManager::VotingUpgradeManager(const VotingUpgradeManagerConfiguration &config) : m_configuration(config) {}

uint16_t VotingUpgradeManager::windowSize() const { return configuration().windowSize(); }

Block::Version VotingUpgradeManager::expectedVersion(Cache::ConstBlockInfoSpan previousBlockInfos,
                                                     const Block::Height &currentHeight,
                                                     const Block::Version &currentVersion) const {
  assert(currentHeight.native() > previousBlockInfos.size());
  XI_RETURN_EC_IF(currentHeight.isNull(), currentVersion);
  XI_RETURN_SC_IF(currentHeight < configuration().minimumHeight(), currentVersion);
  const auto minimumHeight = configuration().minimumHeight().value_or(Block::Height::Genesis);
  const auto minimumVotingHeight =
      minimumHeight.native() <= windowSize() ? Block::Height::Genesis : minimumHeight.shift(-windowSize());
  uint64_t commitment = 0;
  for (uint16_t i = 0; i < previousBlockInfos.size() && i < windowSize(); ++i) {
    const auto &iBlockInfo = previousBlockInfos[previousBlockInfos.size() - i - 1];
    const auto iBlockHeight = currentHeight.shift(-1 - i);
    if (iBlockHeight.isNull() || iBlockHeight < minimumVotingHeight) {
      break;
    }
    if (hasFlag(iBlockInfo.features, Block::Features::UpgradeVote)) {
      commitment += 1;
    }
  }
  XI_RETURN_SC_IF(commitment >= configuration().commitment(), configuration().version());
  XI_RETURN_SC(currentVersion);
}

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
