﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Consensus/ScalingRingSizeStrategy.hpp"

namespace Xi {
namespace Blockchain {
namespace Consensus {

ScalingRingSizeStrategy::ScalingRingSizeStrategy(const ScalingRingSizeStrategyConfiguration &config)
    : m_configuration{config} {}

bool ScalingRingSizeStrategy::isFeasible(const Currency::Amount &, const Currency::RingSize &ringSize) const {
  XI_RETURN_SC_IF(ringSize == Currency::RingSize{1}, true);
  XI_RETURN_EC_IF(ringSize < configuration().minimum(), false);
  XI_RETURN_EC_IF(ringSize > configuration().maximum(), false);
  XI_RETURN_SC(true);
}

Currency::RingSize ScalingRingSizeStrategy::operator()(const Currency::Amount &, const uint64_t availabeOuts) const {
  const auto steps = availabeOuts / configuration().stepSize().native();
  XI_RETURN_SC_IF(steps < configuration().minimum().native(), Currency::RingSize{1});
  XI_RETURN_SC_IF(steps > configuration().maximum().native(), configuration().maximum());
  XI_RETURN_SC(Currency::RingSize{static_cast<Currency::RingSize::value_type>(steps)});
}

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
