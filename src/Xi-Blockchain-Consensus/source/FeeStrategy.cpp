﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Consensus/FeeStrategy.hpp"

#include <Xi/Exceptions.hpp>

#include "Xi/Blockchain/Consensus/StaticFeeStrategy.hpp"
#include "Xi/Blockchain/Consensus/VariableFeeStrategy.hpp"

namespace Xi {
namespace Blockchain {

Consensus::SharedIFeeStrategy Consensus::makeFeeStrategy(const FeeConfiguration &config) {
  if (const auto staticConfig = std::get_if<StaticFeeConfiguration>(std::addressof(config))) {
    auto reval = std::make_shared<StaticFeeStrategy>();
    reval->configuration(*staticConfig);
    return reval;
  } else if (const auto variableConfig = std::get_if<VariableFeeConfiguration>(std::addressof(config))) {
    auto reval = std::make_shared<VariableFeeStrategy>();
    reval->configuration(*variableConfig);
    return reval;
  } else {
    exceptional<InvalidVariantTypeError>("unknown variant type for fee startegy");
  }
}

}  // namespace Blockchain
}  // namespace Xi
