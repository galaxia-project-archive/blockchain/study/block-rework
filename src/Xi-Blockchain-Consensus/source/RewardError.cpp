﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Consensus/RewardError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Blockchain::Consensus, Reward)
XI_ERROR_CODE_DESC(Missmatch, "emitted coins in reward transaction does not match the expected reward")
XI_ERROR_CODE_DESC(HeaderInconsistency, "reward feature flag and reward embedded do not match")
XI_ERROR_CODE_DESC(Missing, "expected a reward but none given")
XI_ERROR_CODE_DESC(NotExpected, "reward system is disable but block embeds a reward")
XI_ERROR_CODE_DESC(NullReward, "the reward is empty or could not have generated any coins at all")
XI_ERROR_CODE_CATEGORY_END()
