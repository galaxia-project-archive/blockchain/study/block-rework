﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Consensus/Currency.hpp"

#include <algorithm>

namespace Xi {
namespace Blockchain {
namespace Consensus {

uint16_t CurrencyConfiguration::blockInfoWindowSize() const {
  return std::max({
      emitter() != nullptr ? emitter()->windowSize() : (uint16_t)0,
      transfer().sizeLimiter().windowSize(),
  });
}

const IEmission *CurrencyConfiguration::emitter() const {
  if (emission() && !m_emitter) {
    m_emitter = makeEmission(*emission());
  }
  return m_emitter.get();
}

const IRingSizeStrategy &CurrencyConfiguration::ringSizeStrategy() const {
  if (!m_ringSizeStrategy) {
    m_ringSizeStrategy = makeRingSizeStrategy(ringSize());
  }
  return *m_ringSizeStrategy;
}

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
