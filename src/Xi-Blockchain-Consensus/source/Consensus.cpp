﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Consensus/Consensus.hpp"

#include <algorithm>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Blockchain::Consensus, Consensus)
XI_ERROR_CODE_DESC(NotFound, "consensus version was not found")
XI_ERROR_CODE_DESC(SerializationFailure, "failed to serialize blob")
XI_ERROR_CODE_DESC(Orphan, "previous block hash is unknown")
XI_ERROR_CODE_DESC(Internal, "internal evaluation error")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Blockchain {
namespace Consensus {

uint16_t Consensus::blockInfoWindowSize() const {
  // clang-format off
  return std::max<uint16_t>({
      difficultyAlgorithm().windowSize(),
      timeSynchronizer().windowSize(),
      (upgrade().has_value() ? upgradeManager()->windowSize() : (uint16_t)0),
      (currency() ? currency()->blockInfoWindowSize() : (uint16_t)0)
  });
  // clang-format on
}

const IDifficultyAlgorithm& Consensus::difficultyAlgorithm() const {
  if (!m_difficultyAlgorithm) {
    m_difficultyAlgorithm = makeDifficultyAlgorithm(difficulty());
  }
  return *m_difficultyAlgorithm;
}

const ProofOfWork::IProofOfWork& Consensus::proofOfWorkHash() const {
  if (!m_proofOfWorkHash) {
    m_proofOfWorkHash = ProofOfWork::makeProofOfWork();
  }
  return *m_proofOfWorkHash;
}

const IUpgradeManager* Consensus::upgradeManager() const {
  if (upgrade().has_value()) {
    if (!m_upgradeManager) {
      m_upgradeManager = makeUpgradeManager(*upgrade());
    }
    return m_upgradeManager.get();
  } else {
    return nullptr;
  }
}

const ITimeSynchronizer& Consensus::timeSynchronizer() const {
  if (!m_timeSynchronizer) {
    m_timeSynchronizer = makeTimeSynchronizer(time());
  }
  return *m_timeSynchronizer;
}

}  // namespace Consensus
}  // namespace Blockchain
}  // namespace Xi
