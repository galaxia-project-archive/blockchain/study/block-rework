﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Consensus/Emission.hpp"

#include <Xi/Exceptions.hpp>

namespace Xi {
namespace Blockchain {

Consensus::SharedIEmission Consensus::makeEmission(const Consensus::EmissionConfiguration &config) {
  if (const auto stat = std::get_if<StaticEmissionConfiguration>(std::addressof(config))) {
    return std::make_shared<StaticEmission>(*stat);
  } else if (const auto cn = std::get_if<CryptoNoteEmissionConfiguration>(std::addressof(config))) {
    return std::make_shared<CryptoNoteEmission>(*cn);
  } else {
    exceptional<InvalidVariantTypeError>();
  }
}

}  // namespace Blockchain
}  // namespace Xi
