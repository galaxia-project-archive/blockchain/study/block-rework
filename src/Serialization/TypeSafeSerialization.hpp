﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <type_traits>
#include <Xi/TypeSafe/TypeSafe.hpp>

#include "Serialization/ISerializer.h"

#define XI_SERIALIAZTION_TYPESAFE(TYPE)                                                                             \
  [[nodiscard]] inline bool serialize(TYPE& type, Common::StringView name, ::CryptoNote::ISerializer& serializer) { \
    auto native = type.native();                                                                                    \
    XI_RETURN_EC_IF_NOT(serializer(native, name), false);                                                           \
    type = TYPE{native};                                                                                            \
    XI_RETURN_SC(true);                                                                                             \
  }

#define XI_SERIALIAZTION_TYPESAFE_RANGE(TYPE, MIN, MAX)                                                             \
  [[nodiscard]] inline bool serialize(TYPE& type, Common::StringView name, ::CryptoNote::ISerializer& serializer) { \
    auto native = type.native();                                                                                    \
    XI_RETURN_EC_IF_NOT(serializer(native, name), false);                                                           \
    XI_RETURN_EC_IF_NOT(native >= MIN, false);                                                                      \
    XI_RETURN_EC_IF_NOT(native <= MAX, false);                                                                      \
    type = TYPE{native};                                                                                            \
    XI_RETURN_SC(true);                                                                                             \
  }

#define XI_SERIALIAZTION_TYPESAFE_MAX(TYPE, MAX)                                                                    \
  [[nodiscard]] inline bool serialize(TYPE& type, Common::StringView name, ::CryptoNote::ISerializer& serializer) { \
    auto native = type.native();                                                                                    \
    XI_RETURN_EC_IF_NOT(serializer(native, name), false);                                                           \
    XI_RETURN_EC_IF_NOT(native <= MAX, false);                                                                      \
    type = TYPE{native};                                                                                            \
    XI_RETURN_SC(true);                                                                                             \
  }

#define XI_SERIALIAZTION_TYPESAFE_MIN(TYPE, MIN)                                                                    \
  [[nodiscard]] inline bool serialize(TYPE& type, Common::StringView name, ::CryptoNote::ISerializer& serializer) { \
    auto native = type.native();                                                                                    \
    XI_RETURN_EC_IF_NOT(serializer(native, name), false);                                                           \
    XI_RETURN_EC_IF_NOT(native >= MIN, false);                                                                      \
    type = TYPE{native};                                                                                            \
    XI_RETURN_SC(true);                                                                                             \
  }
