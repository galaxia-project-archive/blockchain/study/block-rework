﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <optional>

#include <Xi/Byte.hh>
#include <Xi/Exceptions.hpp>
#include <Common/VectorOutputStream.h>

#include "Serialization/ISerializer.h"
#include "Serialization/BinaryOutputStreamSerializer.h"
#include "Serialization/BinaryInputStreamSerializer.h"

namespace CryptoNote {

template <typename _ValueT>
class BlobWrapper {
 private:
  mutable std::optional<Xi::ByteVector> m_blob;
  mutable std::optional<_ValueT> m_value;

  void lazyInitializeBlob() const {
    if (!this->m_blob.has_value()) {
      Xi::exceptional_if_not<Xi::NotInitializedError>(this->m_value.has_value());
      m_blob = toBinaryArray(*this->m_value);
    }
  }

  void lazyInitializeValue() const {
    if (!this->m_value.has_value()) {
      Xi::exceptional_if_not<Xi::NotInitializedError>(this->m_blob.has_value());
      this->m_value = fromBinaryArray<_ValueT>(*this->m_blob);
    }
  }

 public:
  XI_DEFAULT_COPY(BlobWrapper);
  XI_DEFAULT_MOVE(BlobWrapper);

  const _ValueT& value() const {
    this->lazyInitializeValue();
    return *this->m_value;
  }

  _ValueT&& takeValue() {
    this->lazyInitializeValue();
    auto reval = std::move(*this->m_value);
    this->m_value = std::nullopt;
    return reval;
  }

  const Xi::ByteVector& blob() const {
    this->lazyInitializeBlob();
    return *this->m_blob;
  }

  Xi::ByteVector&& takeBlob() {
    this->lazyInitializeBlob();
    auto reval = std::move(*this->m_blob);
    this->m_blob = std::nullopt;
    return std::move(reval);
  }

  void set(const _ValueT& value) {
    this->m_value = value;
    this->m_blob = std::nullopt;
  }

  void set(_ValueT&& value) {
    this->m_value = std::move(value);
    this->m_blob = std::nullopt;
  }

  void set(const Xi::ByteVector& blob) {
    this->m_value = std::nullopt;
    this->m_blob = blob;
  }

  void set(Xi::ByteVector&& blob) {
    this->m_value = std::nullopt;
    this->m_blob = std::move(blob);
  }

  [[nodiscard]] bool fill() {
    try {
      fill();
      XI_RETURN_SC(true);
    } catch (...) {
      XI_RETURN_EC(false);
    }
  }

  BlobWrapper() : m_blob{std::nullopt}, m_value{std::nullopt} {}
  explicit BlobWrapper(const _ValueT& value) : m_blob{std::nullopt}, m_value{value} {}
  explicit BlobWrapper(_ValueT&& value) : m_blob{std::nullopt}, m_value{std::move(value)} {}
  explicit BlobWrapper(const Xi::ByteVector& blob) : m_blob{blob}, m_value{std::nullopt} {}
  explicit BlobWrapper(Xi::ByteVector&& blob) : m_blob{std::move(blob)}, m_value{std::nullopt} {}
};

template <typename _ValueT>
[[nodiscard]] inline bool serialize(BlobWrapper<_ValueT>& blob, Common::StringView name, ISerializer& serializer) {
  try {
    if (serializer.isOutput()) {
      return serializer.binary(const_cast<Xi::ByteVector&>(blob.blob()), name);
    } else if (serializer.isInput()) {
      Xi::ByteVector raw{};
      XI_RETURN_EC_IF_NOT(serializer.binary(raw, name), false);
      blob.set(std::move(raw));
      return true;
    } else {
      return false;
    }
  } catch (...) {
    return false;
  }
}

}  // namespace CryptoNote
