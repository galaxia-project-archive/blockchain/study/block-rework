﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <numeric>
#include <vector>

#include <Xi/Global.hh>
#include <Xi/Exceptions.hpp>
#include <Serialization/ISerializer.h>

namespace Xi {
namespace Blockchain {
namespace Cache {

using GlobalOutputIndex = uint64_t;
using GlobalOutputIndexVector = std::vector<GlobalOutputIndex>;
XI_DECLARE_SPANS(GlobalOutputIndex)

template <typename _IntegerT>
std::vector<_IntegerT> relativeOutputOffsetsToAbsolute(const std::vector<_IntegerT>& off) {
  static_assert(std::is_integral_v<_IntegerT>, "must be an integer");
  std::vector<_IntegerT> res = off;
  for (size_t i = 1; i < res.size(); i++) {
    exceptional_if<OverflowError>(res[i] + res[i - 1] < res[i], "relative offsets overlfow");
    res[i] += res[i - 1];
  }
  return res;
}

template <typename _IntegerT>
std::vector<_IntegerT> absoluteOutputOffsetsToRelative(const std::vector<_IntegerT>& off) {
  static_assert(std::is_integral_v<_IntegerT>, "must be an integer");
  if (off.empty()) return {};
  auto copy = off;
  for (size_t i = 1; i < copy.size(); ++i) {
    exceptional_if<UnderflowError>(off[i] < off[i - 1]);
    copy[i] = off[i] - off[i - 1];
  }
  return copy;
}

}  // namespace Cache
}  // namespace Blockchain
}  // namespace Xi
