﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Serialization/ISerializer.h>

#include "Xi/Blockchain/Block/Version.hpp"
#include "Xi/Blockchain/Block/Features.hpp"
#include "Xi/Blockchain/Block/Hash.hpp"
#include "Xi/Blockchain/Block/Timestamp.hpp"
#include "Xi/Blockchain/ProofOfWork/Difficulty.hpp"
#include "Xi/Blockchain/Currency/Amount.hpp"

namespace Xi {
namespace Blockchain {
namespace Cache {

class BlockInfo {
 public:
  Block::Hash blockHash = Block::Hash::Null;
  Block::Version version = Block::Version::Null;
  Block::Features features = Block::Features::None;
  Block::Timestamp timestamp = Block::Timestamp::Null;
  ProofOfWork::Difficulty cumulativeDifficulty = ProofOfWork::Difficulty{0};
  Currency::Amount cumulativeEmission{0};
  uint64_t cumulativeTransactionsCount = 0;
  uint64_t cumulativeSize = 0;

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(blockHash, block_hash)
  KV_MEMBER_RENAME(version, version)
  KV_MEMBER_RENAME(features, features)
  KV_MEMBER_RENAME(timestamp, timestamp)
  KV_MEMBER_RENAME(cumulativeEmission, cumulative_emission)
  KV_MEMBER_RENAME(cumulativeTransactionsCount, cumulative_transactions_count)
  KV_MEMBER_RENAME(cumulativeDifficulty, cumulative_difficulty)
  KV_MEMBER_RENAME(cumulativeSize, cumulative_size)
  KV_END_SERIALIZATION
};

using BlockInfoVector = std::vector<BlockInfo>;
XI_DECLARE_SPANS(BlockInfo)

}  // namespace Cache
}  // namespace Blockchain
}  // namespace Xi

namespace CryptoNote {
using CachedBlockInfo = Xi::Blockchain::Cache::BlockInfo;
using CachedBlockInfoVector = std::vector<CachedBlockInfo>;
XI_DECLARE_SPANS(CachedBlockInfo)
}  // namespace CryptoNote
