﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <vector>

#include <Xi/Global.hh>
#include <Xi/Byte.hh>
#include <Xi/Span.hpp>
#include <Serialization/ISerializer.h>
#include <Xi/TypeSafe/Arithmetic.hpp>

namespace Xi {
namespace Blockchain {
namespace ProofOfWork {

struct Difficulty : TypeSafe::EnableArithmeticFromThis<uint64_t, Difficulty> {
  Difficulty(Null = null);
  using EnableArithmeticFromThis::EnableArithmeticFromThis;

  [[nodiscard]] bool isValid() const;
  [[nodiscard]] bool check(const Byte hash[256]);

 private:
  friend bool serialize(Difficulty&, Common::StringView, CryptoNote::ISerializer&);
};

using DifficultyVector = std::vector<Difficulty>;
XI_DECLARE_SPANS(Difficulty)

[[nodiscard]] bool serialize(Difficulty& diff, Common::StringView name, CryptoNote::ISerializer& serializer);

}  // namespace ProofOfWork
}  // namespace Blockchain
}  // namespace Xi

namespace CryptoNote {
using BlockDifficulty = Xi::Blockchain::ProofOfWork::Difficulty;
}
