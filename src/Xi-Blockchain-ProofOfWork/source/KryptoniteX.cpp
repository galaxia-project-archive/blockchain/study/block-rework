﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/ProofOfWork/KryptoniteX.hpp"

#include <cstring>

#include <Xi/Crypto/Hash/Sha2.hh>

namespace Xi {
namespace Blockchain {
namespace ProofOfWork {

Hash KryptoniteX::operator()(ConstByteSpan blob) const {
  using inner_hash_type = Crypto::Hash::Sha2::Hash512;
  static_assert(Hash::bytes() == inner_hash_type::bytes(), "assumes 512 bit pow hash");
  inner_hash_type hash{};
  compute(blob, hash);
  Hash reval{};
  std::memcpy(reval.data(), hash.data(), Hash::bytes());
  return reval;
}

}  // namespace ProofOfWork
}  // namespace Blockchain
}  // namespace Xi
