﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/ProofOfWork/Difficulty.hpp"

#include <limits>
#include <algorithm>

#include <Common/int-util.h>

namespace {

#if defined(__SIZEOF_INT128__)

static inline void mul(uint64_t a, uint64_t b, uint64_t &low, uint64_t &high) {
  typedef unsigned __int128 uint128_t;
  uint128_t res = (uint128_t)a * (uint128_t)b;
  low = (uint64_t)res;
  high = (uint64_t)(res >> 64);
}

#else

static inline void mul(uint64_t a, uint64_t b, uint64_t &low, uint64_t &high) { low = mul128(a, b, &high); }

#endif

static inline bool cadd(uint64_t a, uint64_t b) { return a + b < a; }

static inline bool cadc(uint64_t a, uint64_t b, bool c) { return a + b < a || (c && a + b == (uint64_t)-1); }

}  // namespace

namespace Xi {
namespace Blockchain {
namespace ProofOfWork {

bool Difficulty::isValid() const { return native() > 0 && native() < std::numeric_limits<int64_t>::max(); }

bool Difficulty::check(const Byte hash[]) {
  if (std::all_of(hash, hash + 64, [](const auto &byte) { return byte == 0; })) {
    XI_RETURN_EC(false);
  }
  XI_RETURN_EC_IF_NOT(isValid(), false);

  uint64_t low, high, top, cur;
  // First check the highest word, this will most likely fail for a random hash.
  mul(swap64le(((const uint64_t *)&hash)[3]), native(), top, high);
  if (high != 0) {
    return false;
  }
  mul(swap64le(((const uint64_t *)&hash)[0]), native(), low, cur);
  mul(swap64le(((const uint64_t *)&hash)[1]), native(), low, high);
  bool carry = cadd(cur, low);
  cur = high;
  mul(swap64le(((const uint64_t *)&hash)[2]), native(), low, high);
  carry = cadc(cur, low, carry);
  carry = cadc(high, top, carry);
  return !carry;
}

bool serialize(Difficulty &diff, Common::StringView name, CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer(diff.value, name), false);
  return diff.isValid();
}

}  // namespace ProofOfWork
}  // namespace Blockchain
}  // namespace Xi
