﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <type_traits>
#include <iterator>
#include <vector>
#include <algorithm>
#include <set>

#include <Xi/Span.hpp>

namespace Xi {
/*!
 * \brief pow computes the to the power of function (base^exponent)
 * \param base the base of the formula
 * \param exponent the exponent of the formula
 * \return base^exponent, 1 if exponent is 0
 */
static inline constexpr uint32_t pow(uint32_t base, uint32_t exponent) {
  return exponent == 0 ? 1 : base * pow(base, exponent - 1);
}

/*!
 * \brief pow computes the to the power of function (base^exponent)
 * \param base the base of the formula
 * \param exponent the exponent of the formula
 * \return base^exponent, 1 if exponent is 0
 */
static inline constexpr uint64_t pow64(uint64_t base, uint64_t exponent) {
  return exponent == 0 ? 1 : base * pow64(base, exponent - 1);
}

template <typename _IntegerT>
static inline constexpr std::make_unsigned_t<_IntegerT> abs(const _IntegerT value) {
  if constexpr (std::is_signed_v<_IntegerT>) {
    return value;
  } else {
    if (value < 0) {
      return static_cast<std::make_signed_t<_IntegerT>>(-1 * value);
    } else {
      return static_cast<std::make_signed_t<_IntegerT>>(value);
    }
  }
}

template <typename _IntegerT>
static inline _IntegerT median(const std::set<_IntegerT>& values) {
  if (values.empty()) {
    return 0;
  } else if (values.size() == 1) {
    return *values.begin();
  } else {
    const auto n = values.size() / 2;
    if (values.size() & 0b1) {
      return ((*std::next(values.begin(), n - 1)) + (*std::next(values.begin(), n))) / 2;
    } else {
      return *std::next(values.begin(), n);
    }
  }
}

template <typename _IntegerT>
static inline _IntegerT median(ConstSpan<_IntegerT> values) {
  if (values.empty()) {
    return 0;
  } else if (values.size() == 1) {
    return values[0];
  } else {
    std::set<_IntegerT> sort{};
    std::copy(values.begin(), values.end(), std::inserter(sort, sort.begin()));
    return median<_IntegerT>(sort);
  }
}
}  // namespace Xi

static inline constexpr uint64_t operator"" _k(unsigned long long kilo) { return kilo * Xi::pow(10, 3); }
static inline constexpr uint64_t operator"" _M(unsigned long long mega) { return mega * Xi::pow(10, 6); }
static inline constexpr uint64_t operator"" _G(unsigned long long giga) { return giga * Xi::pow(10, 9); }
static inline constexpr uint64_t operator"" _T(unsigned long long tera) { return tera * Xi::pow(10, 12); }
