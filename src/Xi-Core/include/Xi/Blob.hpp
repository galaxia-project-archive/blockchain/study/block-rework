﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cstring>

#include "Xi/Exceptions.hpp"
#include "Xi/Span.hpp"
#include "Xi/Byte.hh"

namespace Xi {

template <typename _T, size_t _Bytes>
struct enable_blob_from_this : protected ByteArray<_Bytes> {
  using value_type = std::remove_cv_t<_T>;
  using array_type = ByteArray<_Bytes>;

  static constexpr bool is_const_v = std::is_const_v<_T>;
  static constexpr bool is_mutable_v = !is_const_v;

  static inline constexpr size_t bytes() { return _Bytes; }

  enable_blob_from_this() { this->fill(0); }
  explicit enable_blob_from_this(array_type raw) : array_type(std::move(raw)) {}

  const Byte *data() const { return this->ByteArray<_Bytes>::data(); }
  std::conditional_t<is_const_v, const Byte *, Byte *> data() { return this->ByteArray<_Bytes>::data(); }

  using array_type::fill;
  using array_type::size;
  using array_type::operator[];

  std::conditional_t<is_const_v, ConstByteSpan, ByteSpan> span() {
    return std::conditional_t<is_const_v, ConstByteSpan, ByteSpan>{this->data(), this->size()};
  }
  ConstByteSpan span() const { return ConstByteSpan{this->data(), this->size()}; }

  inline bool operator==(const value_type &rhs) { return ::std::memcmp(this->data(), rhs.data(), bytes()) == 0; }
  inline bool operator!=(const value_type &rhs) { return ::std::memcmp(this->data(), rhs.data(), bytes()) != 0; }

 protected:
  Byte *mutableData() { return this->ByteArray<_Bytes>::data(); }
  ByteSpan mutableSpan() { return ByteSpan{this->mutableData(), this->size()}; }
};

}  // namespace Xi
