﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <type_traits>
#include <ostream>
#include <string>

namespace Xi {
namespace TypeSafe {

template <typename _ValueT, typename _CompType>
struct EnableIntegralFromThis {
 public:
  static_assert(std::is_integral_v<_ValueT>, "value type must be an integral for EnableIntegralFromThis");
  using value_type = _ValueT;
  using compatible_t = _CompType;

 protected:
  value_type value;  ///< stores the actual value representation

 protected:
  constexpr compatible_t &this_compatible() { return *static_cast<compatible_t *>(this); }
  constexpr const compatible_t &this_compatible() const { return *static_cast<const compatible_t *>(this); }

 public:
  explicit constexpr EnableIntegralFromThis() : value{} {}
  explicit constexpr EnableIntegralFromThis(value_type _value) : value{_value} {}
  explicit constexpr EnableIntegralFromThis(const EnableIntegralFromThis &_value) : value{_value.value} {}
  compatible_t &operator=(compatible_t _value) {
    this->value = _value;
    return this_compatible();
  }

  constexpr value_type native() const { return this->value; }

  constexpr bool operator==(const compatible_t &rhs) const { return this->value == rhs.value; }
  constexpr bool operator!=(const compatible_t &rhs) const { return this->value != rhs.value; }

  constexpr bool operator<(const compatible_t &rhs) const { return this->value < rhs.value; }
  constexpr bool operator<=(const compatible_t &rhs) const { return this->value <= rhs.value; }
  constexpr bool operator>(const compatible_t &rhs) const { return this->value > rhs.value; }
  constexpr bool operator>=(const compatible_t &rhs) const { return this->value >= rhs.value; }
};

template <typename _ValueT, typename _CompType>
inline std::ostream &operator<<(std::ostream &stream, const EnableIntegralFromThis<_ValueT, _CompType> &value) {
  return stream << value.native();
}

template <typename _ValueT, typename _CompType>
inline std::string toString(const EnableIntegralFromThis<_ValueT, _CompType> &value) {
  return std::to_string(value.native());
}

}  // namespace TypeSafe
}  // namespace Xi

#if !defined(XI_TYPESAFE_HASH_OVERLOAD)
#define XI_TYPESAFE_HASH_OVERLOAD(NS, TYPE)                                                                 \
  namespace std {                                                                                           \
  template <>                                                                                               \
  struct hash<NS::TYPE> {                                                                                   \
    size_t operator()(const NS::TYPE &value) const { return hash<NS::TYPE::value_type>{}(value.native()); } \
  };                                                                                                        \
  }  // namespace std
#endif
