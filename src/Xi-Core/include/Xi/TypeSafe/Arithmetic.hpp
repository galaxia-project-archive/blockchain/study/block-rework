﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <type_traits>

#include "Xi/Exceptions.hpp"
#include "Xi/TypeSafe/Integral.hpp"

namespace Xi {
namespace TypeSafe {

template <typename _ValueT, typename _CompType, bool _Checked = true>
struct EnableArithmeticFromThis
    : EnableIntegralFromThis<_ValueT, EnableArithmeticFromThis<_ValueT, _CompType, _Checked>> {
 public:
  static_assert(!_Checked || std::is_unsigned_v<_ValueT>, "check not supported for signed types");
  static_assert(std::is_arithmetic_v<_ValueT>, "Arithmetic can only wrap arithmetic types");

  using value_type = _ValueT;
  using compatible_t = _CompType;

 public:
  using EnableIntegralFromThis<_ValueT, EnableArithmeticFromThis<_ValueT, _CompType, _Checked>>::EnableIntegralFromThis;

  compatible_t operator+(const compatible_t &rhs) const {
    if constexpr (_Checked) {
      exceptional_if<OverflowError>(this->value + rhs.value < this->value);
    }
    return compatible_t{this->value + rhs.value};
  }
  compatible_t operator+=(const compatible_t &rhs) const {
    if constexpr (_Checked) {
      exceptional_if<OverflowError>(this->value + rhs.value < this->value);
    }
    this->value += rhs.value;
    return *this;
  }
  compatible_t operator-(const compatible_t &rhs) const {
    if constexpr (_Checked) {
      exceptional_if<UnderflowError>(this->value - rhs.value > this->value);
    }
    return compatible_t{this->value - rhs.value};
  }
  compatible_t operator-=(const compatible_t &rhs) const {
    if constexpr (_Checked) {
      exceptional_if<UnderflowError>(this->value - rhs.value > this->value);
    }
    this->value -= rhs.value;
    return *this;
  }
};

}  // namespace TypeSafe
}  // namespace Xi
