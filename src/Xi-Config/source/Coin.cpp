﻿#include "Xi/Config/Coin.h"

std::string Xi::Config::Coin::genesisTransactionBlob(Network::Type network) {
  switch (network) {
      // clang-format off
    case Network::Type::LocalTestNet:
    case Network::Type::TestNet:
    case Network::Type::StageNet:
    case Network::Type::MainNet:
      return "010a010101018090cad2c60e014d70099c42c5e47bd9c620168a63340aa114674b9fbbed4c79db69c5fc7374da2101705a666301dda5783d23b643f1e90e918561b2a4dd3a6bac297b2b4c6bf170e9";
      // clang-format on
  }
  throw std::runtime_error{"unexpected network type, cannot provide genesis transaction hash."};
}

uint64_t Xi::Config::Coin::genesisTimestamp(Xi::Config::Network::Type network) {
  switch (network) {
    case Network::Type::StageNet:
      return 1549720565;
    case Network::Type::TestNet:
      return 1558439939;
    case Network::Type::LocalTestNet:
      return 1544396293;
    default:
      return 0;
  }
}
