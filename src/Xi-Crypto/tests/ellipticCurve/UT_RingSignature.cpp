﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <random>
#include <utility>

#include <Xi/Crypto/Random/Engine.hpp>
#include <Xi/Crypto/EllipticCurve/Scalar.hpp>
#include <Xi/Crypto/EllipticCurve/RingSignature.hpp>

#define XI_TESTSUITE Xi_Crypto_EllipticCurve_RingSignature

class XI_TESTSUITE : public ::testing::Test {
 public:
  Xi::Crypto::EllipticCurve::Scalar privateKey;
  Xi::Crypto::EllipticCurve::Scalar invalidPrivateKey;
  Xi::Crypto::EllipticCurve::Point image;
  Xi::Crypto::EllipticCurve::Point publicKey;
  Xi::Crypto::EllipticCurve::Point invalidPublicKey;
  Xi::Crypto::EllipticCurve::PointVector additionalPublicKeys;
  std::vector<Xi::ByteVector> messages;

  void SetUp() {
    ASSERT_TRUE(random(privateKey));
    publicKey = privateKey.toPoint();
    ASSERT_TRUE(random(invalidPrivateKey));
    invalidPublicKey = invalidPrivateKey.toPoint();

    auto imageComputation = Xi::Crypto::EllipticCurve::Point::generateKeyImage(publicKey, privateKey);
    ASSERT_FALSE(imageComputation.isError());
    image = imageComputation.take();

    messages.push_back(Xi::ByteVector{});

    std::default_random_engine eng{};
    std::uniform_int_distribution<size_t> dist{1, 64};
    for (size_t i = 0; i < 10; ++i) {
      Xi::ByteVector message{};
      message.resize(dist(eng));
      ASSERT_EQ(Xi::Crypto::Random::generate(message), Xi::Crypto::Random::RandomError::Success);
      messages.emplace_back(move(message));
    }

    for (size_t i = 0; i < 3; ++i) {
      Xi::Crypto::EllipticCurve::Scalar is;
      ASSERT_TRUE(random(is));
      additionalPublicKeys.emplace_back(is.toPoint());
    }
  }
};

TEST_F(XI_TESTSUITE, BasicGenerationValidation) {
  using namespace Xi;
  using namespace Xi::Crypto::Random;
  using namespace Xi::Crypto::EllipticCurve;
  for (const auto& message : messages) {
    for (size_t i = 1; i < additionalPublicKeys.size(); ++i) {
      for (size_t j = 0; j < i; ++j) {
        auto alteredPublicKeys = additionalPublicKeys;
        alteredPublicKeys.resize(i);
        alteredPublicKeys[j] = publicKey;
        auto signatureResult = RingSignature::sign(message, image, alteredPublicKeys, privateKey, j);
        ASSERT_FALSE(signatureResult.isError());
        EXPECT_TRUE(signatureResult->validate(message, alteredPublicKeys));
      }
    }
  }
}

TEST_F(XI_TESTSUITE, InvalidPublicKeyValidationFails) {
  using namespace ::testing;
  using namespace Xi;
  using namespace Xi::Crypto::Random;
  using namespace Xi::Crypto::EllipticCurve;

  for (const auto& message : messages) {
    for (size_t i = 1; i < additionalPublicKeys.size(); ++i) {
      for (size_t j = 0; j < i; ++j) {
        auto alteredPublicKeys = additionalPublicKeys;
        alteredPublicKeys.resize(i);
        alteredPublicKeys[j] = publicKey;
        auto signatureResult = RingSignature::sign(message, image, alteredPublicKeys, privateKey, j);
        ASSERT_FALSE(signatureResult.isError());
        alteredPublicKeys[j] = invalidPublicKey;
        EXPECT_FALSE(signatureResult->validate(message, alteredPublicKeys));
      }
    }
  }
}

TEST_F(XI_TESTSUITE, AlteredMessageInvalidates) {
  using namespace Xi;
  using namespace Xi::Crypto::Random;
  using namespace Xi::Crypto::EllipticCurve;
  for (const auto& message : messages) {
    for (size_t i = 1; i < additionalPublicKeys.size(); ++i) {
      for (size_t j = 0; j < i; ++j) {
        auto alteredPublicKeys = additionalPublicKeys;
        alteredPublicKeys.resize(i);
        alteredPublicKeys[j] = publicKey;
        auto signatureResult = RingSignature::sign(message, image, alteredPublicKeys, privateKey, j);
        ASSERT_FALSE(signatureResult.isError());

        for (size_t k = 0; k < message.size() && k < 4; ++k) {
          auto alteredMessage = message;
          alteredMessage[k]++;
          EXPECT_FALSE(signatureResult->validate(alteredMessage, alteredPublicKeys));
        }
      }
    }
  }
}
