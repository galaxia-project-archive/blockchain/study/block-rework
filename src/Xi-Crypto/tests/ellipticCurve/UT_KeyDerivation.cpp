﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <unordered_set>
#include <random>
#include <limits>

#include <Xi/Crypto/EllipticCurve/EllipticCurve.hpp>

#define XI_TESTSUITE Xi_Crypto_EllipticCurve_KeyDerivation

using namespace Xi;
using namespace Xi::Crypto;
using namespace Xi::Crypto::EllipticCurve;

class XI_TESTSUITE : public ::testing::Test {
 public:
  std::unordered_set<uint64_t> indices;

  Point viewPublicKey;
  Scalar viewSecretKey;

  Point spendPublicKey;
  Scalar spendSecretKey;

  Point ephPublicKey;
  Scalar ephSecretKey;

  void SetUp() override {
    indices.insert(std::numeric_limits<uint64_t>::min());
    indices.insert(std::numeric_limits<uint64_t>::max());
    std::default_random_engine eng{};
    std::uniform_int_distribution<uint64_t> dist{};
    for (size_t i = 0; i < 10; ++i) {
      indices.insert(dist(eng));
    }

    ASSERT_TRUE(random(viewSecretKey));
    viewPublicKey = viewSecretKey.toPoint();
    ASSERT_TRUE(random(spendSecretKey));
    spendPublicKey = spendSecretKey.toPoint();
    ASSERT_TRUE(random(ephSecretKey));
    ephPublicKey = ephSecretKey.toPoint();
  }
};

TEST_F(XI_TESTSUITE, PublicKeyOutputDerivation) {
  auto derivation = Point::generateKeyDerivation(viewPublicKey, ephSecretKey);
  ASSERT_FALSE(derivation.isError());
  ASSERT_TRUE(derivation->isValid());
  for (const auto index : indices) {
    auto outKey = Point::fromKeyDerivation(*derivation, index, spendPublicKey);
    ASSERT_FALSE(outKey.isError());
    EXPECT_TRUE(outKey->isValid());
  }
}

TEST_F(XI_TESTSUITE, EphPublicKeyRecovery) {
  auto derivation = Point::generateKeyDerivation(viewPublicKey, ephSecretKey);
  ASSERT_FALSE(derivation.isError());
  ASSERT_TRUE(derivation->isValid());
  for (const auto index : indices) {
    auto outKey = Point::fromKeyDerivation(*derivation, index, spendPublicKey);
    ASSERT_FALSE(outKey.isError());
    EXPECT_TRUE(outKey->isValid());

    auto derivePulblicKey = Point::fromKeyDerivation(*derivation, index, spendPublicKey);
    ASSERT_FALSE(derivePulblicKey.isError());
    EXPECT_TRUE(*derivePulblicKey == *outKey);
  }
}

TEST_F(XI_TESTSUITE, EphSecretKeyRecovery) {
  auto derivation = Point::generateKeyDerivation(viewPublicKey, ephSecretKey);
  ASSERT_FALSE(derivation.isError());
  ASSERT_TRUE(derivation->isValid());
  for (const auto index : indices) {
    auto outKey = Point::fromKeyDerivation(*derivation, index, spendPublicKey);
    ASSERT_FALSE(outKey.isError());
    EXPECT_TRUE(outKey->isValid());

    auto ephSecret = Scalar::fromKeyDerivation(*derivation, index, spendSecretKey);
    ASSERT_FALSE(ephSecret.isError());
    EXPECT_TRUE(ephSecret->toPoint() == *outKey);
  }
}
