﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <random>
#include <utility>

#include <Xi/Crypto/Random/Engine.hpp>
#include <Xi/Crypto/EllipticCurve/Scalar.hpp>
#include <Xi/Crypto/EllipticCurve/Signature.hpp>

#define XI_TESTSUITE Xi_Crypto_EllipticCurve_Signature

class XI_TESTSUITE : public ::testing::Test {
 public:
  Xi::Crypto::EllipticCurve::Scalar privateKey;
  Xi::Crypto::EllipticCurve::Scalar invalidPrivateKey;
  Xi::Crypto::EllipticCurve::Point publicKey;
  Xi::Crypto::EllipticCurve::Point invalidPublicKey;
  Xi::Crypto::EllipticCurve::Signature null{Xi::Crypto::EllipticCurve::Signature::Null};
  std::vector<Xi::ByteVector> messages;

  void SetUp() {
    ASSERT_TRUE(random(privateKey));
    publicKey = privateKey.toPoint();
    ASSERT_TRUE(random(invalidPrivateKey));
    invalidPublicKey = invalidPrivateKey.toPoint();

    messages.push_back(Xi::ByteVector{});

    std::default_random_engine eng{};
    std::uniform_int_distribution<size_t> dist{1, 64};
    for (size_t i = 0; i < 5; ++i) {
      Xi::ByteVector message{};
      message.resize(dist(eng));
      ASSERT_EQ(Xi::Crypto::Random::generate(message), Xi::Crypto::Random::RandomError::Success);
      messages.emplace_back(move(message));
    }
  }
};

TEST_F(XI_TESTSUITE, BasicGenerationValidation) {
  for (const auto& message : messages) {
    auto signatureResult = Xi::Crypto::EllipticCurve::Signature::sign(message, publicKey, privateKey);
    ASSERT_FALSE(signatureResult.isError());
    auto signature = signatureResult.take();
    EXPECT_TRUE(signature.validate(message, publicKey));
  }
}

TEST_F(XI_TESTSUITE, InvalidPublicKeyValidationFails) {
  using namespace ::testing;
  using namespace Xi;
  using namespace Xi::Crypto::Random;
  using namespace Xi::Crypto::EllipticCurve;

  for (const auto& message : messages) {
    auto signatureResult = Signature::sign(message, publicKey, privateKey);
    ASSERT_FALSE(signatureResult.isError());
    auto signature = signatureResult.take();
    EXPECT_FALSE(null.validate(message, publicKey));
    EXPECT_FALSE(signature.validate(message, invalidPublicKey));
  }
}

TEST_F(XI_TESTSUITE, AlteredSignatureIsInvalid) {
  using namespace ::testing;
  using namespace Xi;
  using namespace Xi::Crypto::Random;
  using namespace Xi::Crypto::EllipticCurve;

  for (const auto& message : messages) {
    auto signatureResult = Xi::Crypto::EllipticCurve::Signature::sign(message, publicKey, privateKey);
    ASSERT_FALSE(signatureResult.isError());
    auto signature = signatureResult.take();
    ASSERT_TRUE(signature.validate(message, publicKey));

    for (size_t i = 0; i < signature.size(); ++i) {
      auto alteredSignature = signature;
      const_cast<Xi::Byte*>(alteredSignature.data())[i]++;
      EXPECT_FALSE(alteredSignature.validate(message, publicKey));
    }
  }
}

TEST_F(XI_TESTSUITE, AlteredMessageInvalidates) {
  using namespace ::testing;
  using namespace Xi;
  using namespace Xi::Crypto::Random;
  using namespace Xi::Crypto::EllipticCurve;

  for (const auto& message : messages) {
    if (message.empty()) {
      continue;
    }
    auto signatureResult = Xi::Crypto::EllipticCurve::Signature::sign(message, publicKey, privateKey);
    ASSERT_FALSE(signatureResult.isError());
    auto signature = signatureResult.take();
    ASSERT_TRUE(signature.validate(message, publicKey));

    for (size_t i = 0; i < message.size(); ++i) {
      auto alteredMessage = message;
      alteredMessage[i]++;
      EXPECT_FALSE(signature.validate(alteredMessage, publicKey));
    }
  }
}
