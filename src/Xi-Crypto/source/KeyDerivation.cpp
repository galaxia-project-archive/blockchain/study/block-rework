﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Crypto/KeyDerivation.hpp"

#include <stdexcept>
#include <utility>

#include <Common/StringTools.h>

#include "ellipticCurve/bernstein/Bernstein.hh"

const Xi::Crypto::KeyDerivation Xi::Crypto::KeyDerivation::Null{};

Xi::Result<Xi::Crypto::KeyDerivation> Xi::Crypto::KeyDerivation::fromString(const std::string &hex) {
  XI_ERROR_TRY();
  KeyDerivation reval;
  if (KeyDerivation::bytes() * 2 != hex.size()) {
    throw std::runtime_error{"wrong hex size"};
  }
  if (!Common::fromHex(hex, reval.mutableData(), reval.size() * sizeof(value_type))) {
    throw std::runtime_error{"invalid hex string"};
  }
  if (!reval.isValid()) {
    throw std::runtime_error{"key invalid"};
  }

  return success(std::move(reval));
  XI_ERROR_CATCH();
}

Xi::Crypto::KeyDerivation::KeyDerivation() { nullify(); }
Xi::Crypto::KeyDerivation::KeyDerivation(Xi::Crypto::KeyDerivation::array_type raw) : Point(std::move(raw)) {}

Xi::Crypto::KeyDerivation::~KeyDerivation() {}

Xi::Crypto::KeyDerivation::KeyDerivation(const Xi::Crypto::EllipticCurve::Point &point) : Point(point) {}

Xi::Crypto::KeyDerivation::KeyDerivation(Xi::Crypto::EllipticCurve::Point &&point) : Point(std::move(point)) {}

Xi::Crypto::KeyDerivation &Crypto::KeyDerivation::operator=(const Xi::Crypto::EllipticCurve::Point &point) {
  asPoint() = point;
  return *this;
}

Xi::Crypto::KeyDerivation &Crypto::KeyDerivation::operator=(Xi::Crypto::EllipticCurve::Point &&point) {
  asPoint() = std::move(point);
  return *this;
}

std::string Xi::Crypto::KeyDerivation::toString() const { return Common::toHex(data(), size() * sizeof(value_type)); }

bool Xi::Crypto::KeyDerivation::isNull() const { return (*this) == Null; }

bool Xi::Crypto::KeyDerivation::isValid() const { return asPoint().isValid(); }

Xi::ConstByteSpan Xi::Crypto::KeyDerivation::span() const { return Xi::ConstByteSpan{data(), bytes()}; }

Xi::Crypto::EllipticCurve::Point &Crypto::KeyDerivation::asPoint() { return *this; }

const Xi::Crypto::EllipticCurve::Point &Crypto::KeyDerivation::asPoint() const { return *this; }

void Xi::Crypto::KeyDerivation::nullify() { fill(0); }

bool Xi::Crypto::serialize(Xi::Crypto::KeyDerivation &keyDerivation, Common::StringView name,
                           CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer.binary(keyDerivation.mutableData(), KeyDerivation::bytes(), name), false);
  XI_RETURN_EC_IF_NOT(keyDerivation.isValid(), false);
  return true;
}
