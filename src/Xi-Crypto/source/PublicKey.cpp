﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Crypto/PublicKey.hpp"

#include <stdexcept>
#include <utility>

#include <Common/StringTools.h>

#include "ellipticCurve/bernstein/Bernstein.hh"

const Xi::Crypto::PublicKey Xi::Crypto::PublicKey::Null{};

Xi::Result<Xi::Crypto::PublicKey> Xi::Crypto::PublicKey::fromString(const std::string &hex) {
  XI_ERROR_TRY();
  PublicKey reval;
  if (PublicKey::bytes() * 2 != hex.size()) {
    throw std::runtime_error{"wrong hex size"};
  }
  if (!Common::fromHex(hex, reval.mutableData(), reval.size() * sizeof(value_type))) {
    throw std::runtime_error{"invalid hex string"};
  }
  if (!reval.isValid()) {
    throw std::runtime_error{"public key is invalid"};
  }
  return success(std::move(reval));
  XI_ERROR_CATCH();
}

Xi::Crypto::PublicKey::PublicKey() { nullify(); }

Xi::Crypto::PublicKey::PublicKey(Xi::Null) { nullify(); }

Xi::Crypto::PublicKey::PublicKey(Xi::Crypto::PublicKey::array_type raw) : Point(std::move(raw)) {}

Xi::Crypto::PublicKey::~PublicKey() {}

Crypto::PublicKey::PublicKey(const Xi::Crypto::EllipticCurve::Point &point) : Point(point) {}

Crypto::PublicKey::PublicKey(Xi::Crypto::EllipticCurve::Point &&point) : Point(std::move(point)) {}

Xi::Crypto::PublicKey &Crypto::PublicKey::operator=(const Xi::Crypto::EllipticCurve::Point &point) {
  asPoint() = point;
  return *this;
}

Xi::Crypto::PublicKey &Crypto::PublicKey::operator=(Xi::Crypto::EllipticCurve::Point &&point) {
  asPoint() = std::move(point);
  return *this;
}

std::string Xi::Crypto::PublicKey::toString() const { return Common::toHex(data(), size() * sizeof(value_type)); }

bool Xi::Crypto::PublicKey::isNull() const { return *this == PublicKey::Null; }

Xi::ConstByteSpan Xi::Crypto::PublicKey::span() const { return Xi::ConstByteSpan{data(), bytes()}; }

Xi::Crypto::EllipticCurve::Point &Xi::Crypto::PublicKey::asPoint() { return *this; }

const Xi::Crypto::EllipticCurve::Point &Xi::Crypto::PublicKey::asPoint() const { return *this; }

bool Xi::Crypto::PublicKey::isValid() const { return asPoint().isValid(); }

void Xi::Crypto::PublicKey::nullify() { fill(0); }

bool Xi::Crypto::serialize(Xi::Crypto::PublicKey &publicKey, Common::StringView name,
                           CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer.binary(publicKey.mutableData(), PublicKey::bytes(), name), false);
  XI_RETURN_EC_IF_NOT(publicKey.isValid(), false);
  XI_RETURN_SC(true);
}

std::ostream &Xi::Crypto::operator<<(std::ostream &stream, const Xi::Crypto::PublicKey &key) {
  return stream << key.toString();
}
