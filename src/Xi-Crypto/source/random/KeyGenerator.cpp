﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Crypto/Random/KeyGenerator.hpp"

#include <Xi/Exceptions.hpp>

#include "Xi/Crypto/EllipticCurve/Scalar.hpp"

namespace Xi {
namespace Crypto {
namespace Random {

Result<KeyPair> KeyGenerator::operator()() const {
  XI_ERROR_TRY();
  KeyPair reval{};
  reval.secretKey(EllipticCurve::Scalar::generateRandom().takeOrThrow());
  reval.publicKey(reval.secretKey().derivePublicKey());
  return success(std::move(reval));
  XI_ERROR_CATCH();
}

Result<KeyPair> KeyGenerator::operator()(ConstByteSpan seed) const {
  XI_ERROR_TRY();
  KeyPair reval{};
  reval.secretKey(EllipticCurve::Scalar::generateRandom(seed).takeOrThrow());
  reval.publicKey(reval.secretKey().derivePublicKey());
  return success(std::move(reval));
  XI_ERROR_CATCH();
}

std::shared_ptr<IKeyGenerator> makeKeyGenerator(const KeyGeneratorConfiguration &config) {
  if (!config.has_value()) {
    return std::make_shared<KeyGenerator>();
  } else {
    if (const auto salted = std::get_if<SaltedKeyGeneratorConfig>(std::addressof(*config))) {
      return std::make_shared<SaltedKeyGenerator>(asConstByteSpan(salted->salt()));
    } else {
      exceptional<InvalidVariantTypeError>("uknown key configuration type");
    }
  }
}

}  // namespace Random
}  // namespace Crypto
}  // namespace Xi
