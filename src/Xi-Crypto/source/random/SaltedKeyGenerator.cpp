﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Crypto/Random/SaltedKeyGenerator.hpp"

#include <cstring>

namespace Xi {
namespace Crypto {
namespace Random {

bool SaltedKeyGeneratorConfig::serialize(CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer.binary(m_salt, "salt"), false);
  return true;
}

SaltedKeyGenerator::SaltedKeyGenerator(ConstByteSpan salt) {
  m_salt.resize(salt.size_bytes());
  std::memcpy(m_salt.data(), salt.data(), salt.size_bytes());
}

Result<KeyPair> SaltedKeyGenerator::operator()() const {
  XI_ERROR_TRY();
  KeyPair reval{};
  reval.secretKey(EllipticCurve::Scalar::generateRandom().takeOrThrow());
  reval.publicKey(reval.secretKey().derivePublicKey());
  return success(std::move(reval));
  XI_ERROR_CATCH();
}

Result<KeyPair> SaltedKeyGenerator::operator()(ConstByteSpan seed) const {
  XI_ERROR_TRY();
  KeyPair reval{};
  if (seed.empty()) {
    reval.secretKey(EllipticCurve::Scalar::generateRandom().takeOrThrow());
    reval.publicKey(reval.secretKey().derivePublicKey());
  } else {
    auto saltedSeed = m_salt;
    saltedSeed.resize(saltedSeed.size() + seed.size_bytes());
    std::memcpy(saltedSeed.data() + m_salt.size(), seed.data(), seed.size_bytes());
    reval.secretKey(EllipticCurve::Scalar::generateRandom(saltedSeed).takeOrThrow());
    reval.publicKey(reval.secretKey().derivePublicKey());
  }
  return success(std::move(reval));
  XI_ERROR_CATCH();
}

}  // namespace Random
}  // namespace Crypto
}  // namespace Xi
