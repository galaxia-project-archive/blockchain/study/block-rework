﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <stdexcept>
#include <utility>

#include "Xi/Crypto/SecretKey.hpp"

#include <Xi/Memory/Clear.hh>
#include <Common/StringTools.h>

const Xi::Crypto::SecretKey Xi::Crypto::SecretKey::Null{};

Xi::Result<Xi::Crypto::SecretKey> Xi::Crypto::SecretKey::fromString(const std::string &hex) {
  XI_ERROR_TRY();
  SecretKey reval;
  if (SecretKey::bytes() * 2 != hex.size()) {
    throw std::runtime_error{"wrong hex size"};
  }
  if (!Common::fromHex(hex, reval.data(), reval.size() * sizeof(value_type))) {
    throw std::runtime_error{"invalid hex string"};
  }
  if (!reval.isValid()) {
    throw std::runtime_error{"secret key is invalid"};
  }
  return success(std::move(reval));
  XI_ERROR_CATCH();
}

Xi::Crypto::SecretKey::SecretKey() { nullify(); }

Crypto::SecretKey::SecretKey(const Xi::Crypto::EllipticCurve::Scalar &scalar) : Scalar(scalar) {}

Crypto::SecretKey::SecretKey(Xi::Crypto::EllipticCurve::Scalar &&scalar) : Scalar(std::move(scalar)) {}

Xi::Crypto::SecretKey::SecretKey(Xi::Crypto::SecretKey::array_type raw) : Scalar(std::move(raw)) {}

Xi::Crypto::SecretKey &Crypto::SecretKey::operator=(const Xi::Crypto::EllipticCurve::Scalar &scalar) {
  asScalar() = scalar;
  return *this;
}

Xi::Crypto::SecretKey &Crypto::SecretKey::operator=(Xi::Crypto::EllipticCurve::Scalar &&scalar) {
  asScalar() = std::move(scalar);
  return *this;
}

Xi::Crypto::PublicKey Crypto::SecretKey::derivePublicKey() const { return asScalar().toPoint(); }

Xi::Crypto::SecretKey::~SecretKey() { Xi::Memory::secureClear(span()); }

std::string Xi::Crypto::SecretKey::toString() const { return Common::toHex(data(), size() * sizeof(value_type)); }

bool Xi::Crypto::SecretKey::isNull() const { return *this == SecretKey::Null; }

bool Xi::Crypto::SecretKey::isValid() const { return asScalar().isValid(); }

Xi::ConstByteSpan Xi::Crypto::SecretKey::span() const { return Xi::ConstByteSpan{data(), bytes()}; }

Xi::ByteSpan Xi::Crypto::SecretKey::span() { return Xi::ByteSpan{data(), bytes()}; }

void Xi::Crypto::SecretKey::nullify() { fill(0); }

Xi::Crypto::EllipticCurve::Scalar &Crypto::SecretKey::asScalar() { return *this; }

const Xi::Crypto::EllipticCurve::Scalar &Crypto::SecretKey::asScalar() const { return *this; }

bool Xi::Crypto::serialize(Xi::Crypto::SecretKey &secretKey, Common::StringView name,
                           CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer.binary(secretKey.data(), SecretKey::bytes(), name), false);
  XI_RETURN_EC_IF_NOT(secretKey.isValid(), false);
  return true;
}
