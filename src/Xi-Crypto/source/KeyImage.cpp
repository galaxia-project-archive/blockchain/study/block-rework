﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Crypto/KeyImage.hpp"

#include <stdexcept>
#include <utility>

#include <Common/StringTools.h>

#include "ellipticCurve/bernstein/Bernstein.hh"

const Xi::Crypto::KeyImage Xi::Crypto::KeyImage::Null{};

Xi::Result<Xi::Crypto::KeyImage> Xi::Crypto::KeyImage::fromString(const std::string &hex) {
  XI_ERROR_TRY();
  KeyImage reval;
  if (KeyImage::bytes() * 2 != hex.size()) {
    throw std::runtime_error{"wrong hex size"};
  }
  if (!Common::fromHex(hex, reval.mutableData(), reval.size() * sizeof(value_type))) {
    throw std::runtime_error{"invalid hex string"};
  }
  if (!reval.isValid()) {
    throw std::runtime_error{"key image is invalid"};
  }
  return success(std::move(reval));
  XI_ERROR_CATCH();
}

Xi::Crypto::KeyImage::KeyImage() { nullify(); }

Xi::Crypto::KeyImage::KeyImage(Xi::Crypto::KeyImage::array_type raw) : Point(std::move(raw)) {}

Xi::Crypto::KeyImage::~KeyImage() {}

Xi::Crypto::KeyImage::KeyImage(const Xi::Crypto::EllipticCurve::Point &point) : Point(point) {}

Xi::Crypto::KeyImage::KeyImage(Xi::Crypto::EllipticCurve::Point &&point) : Point(std::move(point)) {}

Xi::Crypto::KeyImage &Xi::Crypto::KeyImage::operator=(const Xi::Crypto::EllipticCurve::Point &point) {
  asPoint() = point;
  return *this;
}

Xi::Crypto::KeyImage &Xi::Crypto::KeyImage::operator=(Xi::Crypto::EllipticCurve::Point &&point) {
  asPoint() = std::move(point);
  return *this;
}

std::string Xi::Crypto::KeyImage::toString() const { return Common::toHex(data(), size() * sizeof(value_type)); }

Xi::ConstByteSpan Xi::Crypto::KeyImage::span() const { return Xi::ConstByteSpan{data(), bytes()}; }

bool Xi::Crypto::KeyImage::isValid() const {
  static const KeyImage I{{0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                           0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                           0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};
  static const KeyImage L{{0xed, 0xd3, 0xf5, 0x5c, 0x1a, 0x63, 0x12, 0x58, 0xd6, 0x9c, 0xf7,
                           0xa2, 0xde, 0xf9, 0xde, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                           0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10}};
  ge_p3 A;
  ge_p2 R;
  XI_RETURN_EC_IF(ge_frombytes_vartime(&A, data()) != 0, false);
  ge_scalarmult(&R, I.data(), &A);
  KeyImage aP;
  ge_tobytes(aP.mutableData(), &R);
  return std::memcmp(aP.data(), I.data(), bytes()) == 0;
}

bool Xi::Crypto::KeyImage::isNull() const { return (*this) == Null; }

void Xi::Crypto::KeyImage::nullify() { fill(0); }

Xi::Crypto::EllipticCurve::Point &Xi::Crypto::KeyImage::asPoint() { return *this; }

const Xi::Crypto::EllipticCurve::Point &Xi::Crypto::KeyImage::asPoint() const { return *this; }

bool Xi::Crypto::serialize(Xi::Crypto::KeyImage &keyImage, Common::StringView name,
                           CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer.binary(keyImage.mutableData(), KeyImage::bytes(), name), false);
  XI_RETURN_EC_IF_NOT(keyImage.isValid(), false);
  return true;
}
