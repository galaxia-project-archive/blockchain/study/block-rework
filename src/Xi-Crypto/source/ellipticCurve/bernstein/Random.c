/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "ellipticCurve/bernstein/Random.hh"

#include <Xi/Global.hh>

#include "Xi/Crypto/EllipticCurve/Constants.hh"
#include "ellipticCurve/bernstein/Operations.hh"

int sc_random(xi_byte_t *out) {
  int ec = XI_RETURN_CODE_SUCCESS;
  do {
    ec = xi_crypto_random_bytes(out, XI_CRYPTO_ELLIPTIC_CURVE_SCALAR_SIZE);
    XI_RETURN_EC_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, ec);
  } while (!sc_isnonzero(out) && !sc_less_32(out, xi_crypto_elliptic_curve_constants_curveOrder));
  sc_reduce32(out);
  return XI_RETURN_CODE_SUCCESS;
}

int sc_random_deterministic(xi_byte_t *out, const xi_byte_t *seed, size_t length) {
  xi_crypto_random_state *rng = xi_crypto_random_state_create();
  XI_RETURN_EC_IF(rng == NULL, XI_RETURN_CODE_NO_SUCCESS);
  int ec = xi_crypto_random_state_init_deterministic(rng, seed, length);
  if (ec != XI_RETURN_CODE_SUCCESS) {
    xi_crypto_random_state_destroy(rng);
    return XI_RETURN_CODE_NO_SUCCESS;
  }

  do {
    ec = xi_crypto_random_bytes_from_state_deterministic(out, XI_CRYPTO_ELLIPTIC_CURVE_SCALAR_SIZE,
                                                         rng);
    if (ec != XI_RETURN_CODE_SUCCESS) {
      xi_crypto_random_state_destroy(rng);
      return XI_RETURN_CODE_NO_SUCCESS;
    }
  } while (!sc_isnonzero(out) && !sc_less_32(out, xi_crypto_elliptic_curve_constants_curveOrder));
  sc_reduce32(out);
  xi_crypto_random_state_destroy(rng);
  return XI_RETURN_CODE_SUCCESS;
}
