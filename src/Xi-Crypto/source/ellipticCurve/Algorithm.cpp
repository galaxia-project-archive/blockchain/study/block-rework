﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Crypto/EllipticCurve/Algorithm.hpp"

#include <Xi/Exceptions.hpp>

#include "ellipticCurve/bernstein/Bernstein.hh"

Xi::Crypto::EllipticCurve::ScalarVector Xi::Crypto::EllipticCurve::powers(const Xi::Crypto::EllipticCurve::Scalar &base,
                                                                          size_t maxExponent) {
  if (maxExponent == 0) {
    return {};
  } else if (maxExponent == 1) {
    return {identity()};
  }
  ScalarVector res{};
  res.resize(maxExponent);

  res[0] = identity();
  res[1] = base;

  for (size_t i = 2; i < maxExponent; ++i) {
    res[i] = res[i - 1] * base;
  }
  return res;
}

Xi::Crypto::EllipticCurve::Scalar Xi::Crypto::EllipticCurve::powersSum(Xi::Crypto::EllipticCurve::Scalar base,
                                                                       size_t maxExponent) {
  if (maxExponent == 0) {
    return zero();
  } else if (maxExponent == 1) {
    return identity();
  } else {
    Scalar reval = identity();
    const bool isPowerOf2 = (maxExponent & (maxExponent - 1)) == 0;

    if (isPowerOf2) {
      sc_add(reval.data(), reval.data(), base.data());
      while (maxExponent > 2) {
        sc_mul(base.data(), base.data(), base.data());
        sc_muladd(reval.data(), base.data(), reval.data(), reval.data());
        maxExponent /= 2;
      }
    } else {
      auto prev = base;
      for (size_t i = 1; i < maxExponent; ++i) {
        if (i > 1) sc_mul(prev.data(), prev.data(), base.data());
        sc_add(reval.data(), reval.data(), prev.data());
      }
    }

    return reval;
  }
}

Xi::Crypto::EllipticCurve::Scalar Xi::Crypto::EllipticCurve::innerProduct(
    Xi::Crypto::EllipticCurve::ConstScalarSpan lhs, Xi::Crypto::EllipticCurve::ConstScalarSpan rhs) {
  exceptional_if_not<Xi::InvalidSizeError>(lhs.size() == rhs.size(), "inner product required vectors of same size");

  Scalar res = zero();
  for (size_t i = 0; i < lhs.size(); ++i) {
    sc_muladd(res.data(), lhs[i].data(), rhs[i].data(), res.data());
  }
  return res;
}

Xi::Crypto::EllipticCurve::ScalarVector Xi::Crypto::EllipticCurve::hadamardProduct(
    Xi::Crypto::EllipticCurve::ConstScalarSpan lhs, Xi::Crypto::EllipticCurve::ConstScalarSpan rhs) {
  return lhs * rhs;
}
