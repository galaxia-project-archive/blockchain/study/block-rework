﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Crypto/Signature.hpp"

#include <stdexcept>
#include <utility>

#include <Common/StringTools.h>

#include "ellipticCurve/bernstein/Bernstein.hh"

const Xi::Crypto::Signature Xi::Crypto::Signature::Null{};

Xi::Result<Xi::Crypto::Signature> Xi::Crypto::Signature::fromString(const std::string &hex) {
  XI_ERROR_TRY();
  Signature reval;
  if (Signature::bytes() * 2 != hex.size()) {
    throw std::runtime_error{"wrong hex size"};
  }
  if (!Common::fromHex(hex, reval.data(), reval.size() * sizeof(value_type))) {
    throw std::runtime_error{"invalid hex string"};
  }
  if (!reval.isValid()) {
    throw std::runtime_error{"signature is invalid"};
  }
  return success(std::move(reval));
  XI_ERROR_CATCH();
}

Xi::Crypto::Signature::Signature() { nullify(); }

Xi::Crypto::Signature::Signature(Xi::Crypto::Signature::array_type raw) : array_type(std::move(raw)) {}

Xi::Crypto::Signature::~Signature() {}

std::string Xi::Crypto::Signature::toString() const { return Common::toHex(data(), size() * sizeof(value_type)); }

bool Xi::Crypto::Signature::isValid() const { return sc_check(data()) == 0 && sc_check(data() + 32) == 0; }

bool Xi::Crypto::Signature::isNull() const { return (*this) == Null; }

Xi::ConstByteSpan Xi::Crypto::Signature::span() const { return Xi::ConstByteSpan{data(), bytes()}; }

Xi::ByteSpan Xi::Crypto::Signature::span() { return Xi::ByteSpan{data(), bytes()}; }

void Xi::Crypto::Signature::nullify() { fill(0); }

bool Xi::Crypto::serialize(Xi::Crypto::Signature &signature, Common::StringView name,
                           CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer.binary(signature.data(), Signature::bytes(), name), false);
  XI_RETURN_EC_IF_NOT(signature.isValid(), false);
  return true;
}
