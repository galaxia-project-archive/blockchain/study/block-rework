﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Crypto/Hash.hpp"

#include <stdexcept>
#include <utility>

#include <Xi/Exceptions.hpp>
#include <Xi/Crypto/Hash/FastHash.hh>
#include <Xi/Crypto/Hash/TreeHash.hh>
#include <Common/StringTools.h>

const Xi::Crypto::FastHash Xi::Crypto::FastHash::Null{};

Xi::Result<Xi::Crypto::FastHash> Xi::Crypto::FastHash::fromString(const std::string &hex) {
  XI_ERROR_TRY();
  FastHash reval;
  if (FastHash::bytes() * 2 != hex.size()) {
    throw std::runtime_error{"wrong hex size"};
  }
  if (Common::fromHex(hex, reval.data(), reval.size() * sizeof(value_type)) != reval.size() * sizeof(value_type)) {
    throw std::runtime_error{"invalid hex string"};
  }
  return success(std::move(reval));
  XI_ERROR_CATCH();
}

Xi::Result<Xi::Crypto::FastHash> Xi::Crypto::FastHash::compute(Xi::ConstByteSpan data) {
  XI_ERROR_TRY();
  FastHash reval{};
  compute(data, reval).throwOnError();
  return success(std::move(reval));
  XI_ERROR_CATCH();
}

Xi::Result<void> Xi::Crypto::FastHash::compute(Xi::ConstByteSpan data, Xi::Crypto::FastHash &out) {
  XI_ERROR_TRY();
  Hash::fastHash(data, out.span());
  return Xi::success();
  XI_ERROR_CATCH();
}

Xi::Result<void> Xi::Crypto::FastHash::compute(Xi::ConstByteSpan data, Xi::ByteSpan out) {
  XI_ERROR_TRY();
  Hash::fastHash(data, out);
  return Xi::success();
  XI_ERROR_CATCH();
}

Xi::Result<Xi::Crypto::FastHash> Xi::Crypto::FastHash::computeMerkleTree(Xi::ConstByteSpan data, size_t count) {
  XI_ERROR_TRY();
  FastHash reval{};
  computeMerkleTree(data, count, reval).throwOnError();
  return success(std::move(reval));
  XI_ERROR_CATCH();
}

Xi::Result<void> Xi::Crypto::FastHash::computeMerkleTree(Xi::ConstByteSpan data, size_t count,
                                                         Xi::Crypto::FastHash &out) {
  XI_ERROR_TRY();
  Xi::Crypto::Hash::treeHash(data, count, out.span());
  return Xi::success();
  XI_ERROR_CATCH();
}

Xi::Result<Xi::Crypto::FastHash> Xi::Crypto::FastHash::computeMerkleTree(ConstFastHashSpan data) {
  XI_ERROR_TRY();
  FastHash reval{};
  computeMerkleTree(data, reval).throwOnError();
  return success(std::move(reval));
  XI_ERROR_CATCH();
}

Xi::Result<void> Xi::Crypto::FastHash::computeMerkleTree(ConstFastHashSpan data, Xi::Crypto::FastHash &out) {
  XI_ERROR_TRY();
  Hash::treeHash(
      Xi::ConstByteSpan{reinterpret_cast<const Xi::Byte *>(data.data()), data.size() * Xi::Crypto::FastHash::bytes()},
      data.size(), out.span());
  return Xi::success();
  XI_ERROR_CATCH();
}

Xi::Crypto::FastHash::FastHash(Xi::Null) { nullify(); }

Xi::Crypto::FastHash::FastHash() { nullify(); }

Xi::Crypto::FastHash::FastHash(Xi::Crypto::FastHash::array_type raw) : array_type(std::move(raw)) {}

Xi::Crypto::FastHash::~FastHash() {}

bool Xi::Crypto::FastHash::isNull() const { return *this == FastHash::Null; }

std::string Xi::Crypto::FastHash::toString() const { return Common::toHex(data(), size()); }

std::string Xi::Crypto::FastHash::toShortString() const {
  std::string res = toString();
  res.erase(8, 48);
  res.insert(8, "....");
  return res;
}

Xi::ConstByteSpan Xi::Crypto::FastHash::span() const { return Xi::ConstByteSpan{data(), bytes()}; }

Xi::ByteSpan Xi::Crypto::FastHash::span() { return Xi::ByteSpan{data(), bytes()}; }

void Xi::Crypto::FastHash::assign(Xi::ConstByteSpan src) {
  Xi::exceptional_if<Xi::OutOfRangeError>(src.size_bytes() > this->size());
  std::memcpy(data(), src.data(), src.size_bytes());
}

void Xi::Crypto::FastHash::nullify() { fill(0); }

bool Xi::Crypto::serialize(Xi::Crypto::FastHash &hash, Common::StringView name, CryptoNote::ISerializer &serializer) {
  return serializer.binary(hash.data(), FastHash::bytes(), name);
}

std::ostream &Xi::Crypto::operator<<(std::ostream &stream, const Xi::Crypto::FastHash &hash) {
  return stream << hash.toString();
}
