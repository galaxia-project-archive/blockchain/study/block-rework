﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/Result.h>

#include "Xi/Crypto/EllipticCurve/Hash.hh"
#include "Xi/Crypto/EllipticCurve/Point.hpp"
#include "Xi/Crypto/EllipticCurve/Signature.hpp"

namespace Xi {
namespace Crypto {
namespace EllipticCurve {

struct RingSignature {
 public:
  static Result<RingSignature> sign(const Hash& messageHash, const Point& image, ConstPointSpan publicKeys,
                                    const Scalar& secretKey, const size_t secretKeyIndex);
  static Result<RingSignature> sign(ConstByteSpan message, const Point& image, ConstPointSpan publicKeys,
                                    const Scalar& secretKey, const size_t secretKeyIndex);

 public:
  [[nodiscard]] bool validate(const Hash& messageHash, ConstPointSpan publicKeys) const;
  [[nodiscard]] bool validate(ConstByteSpan message, ConstPointSpan publicKeys) const;

 private:
  Point m_image;
  SignatureVector m_signatures;
};

}  // namespace EllipticCurve
}  // namespace Crypto
}  // namespace Xi
