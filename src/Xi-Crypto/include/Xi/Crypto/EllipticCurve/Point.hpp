﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <memory>

#include <Xi/Result.h>
#include <Xi/Span.hpp>
#include <Xi/Memory/SecureBlob.hpp>

#include "Xi/Crypto/EllipticCurve/Constants.hh"
#include "Xi/Crypto/EllipticCurve/Hash.hh"

namespace Xi {
namespace Crypto {
namespace EllipticCurve {

struct Point : Memory::EnableSecureBlobFromThis<const Point, pointSize()> {
 public:
  static Result<Point> generateKeyImage(const Point& publicKey, const struct Scalar& secretKey);
  static Result<Point> generateKeyDerivation(const Point& publicKey, const struct Scalar& secretKey);
  static Result<Point> fromKeyDerivation(const Point& derivation, uint64_t output_index, const Point& base);

 public:
  static const Point Null;

  using Memory::EnableSecureBlobFromThis<const Point, pointSize()>::EnableSecureBlobFromThis;

  XI_DEFAULT_MOVE(Point);
  XI_DEFAULT_COPY(Point);

  bool isValid() const;

  operator bool() const;
  bool operator!() const;

  Point operator+(const Point& rhs) const;
  Point& operator+=(const Point& rhs);

 private:
  friend struct Scalar;
  friend Point operator*(eight_t, const Point&);
  friend Point& operator*=(Point&, eight_t);
};

using PointVector = std::vector<Point>;
XI_DECLARE_SPANS(Point)

}  // namespace EllipticCurve
}  // namespace Crypto
}  // namespace Xi
