﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/ErrorCode.hpp>

namespace Xi {
namespace Crypto {
namespace EllipticCurve {

XI_ERROR_CODE_BEGIN(KeyDerivation)

/// Hash computation failed.
XI_ERROR_CODE_VALUE(HashComputationFailed, 0x0001)

/// Reduction succeeded but the result is invalid.
XI_ERROR_CODE_VALUE(InvalidDerivation, 0x0002)

/// The random generator used for the derivation failed.
XI_ERROR_CODE_VALUE(RandomGenerationFailed, 0x0003)

XI_ERROR_CODE_END(KeyDerivation, "key derivation operation failed")

}  // namespace EllipticCurve
}  // namespace Crypto
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Crypto::EllipticCurve, KeyDerivation)
