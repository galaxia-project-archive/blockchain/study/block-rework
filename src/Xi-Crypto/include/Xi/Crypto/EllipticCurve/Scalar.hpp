﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Result.h>
#include <Xi/Span.hpp>
#include <Xi/Memory/SecureBlob.hpp>

#include "Xi/Crypto/EllipticCurve/Constants.hh"
#include "Xi/Crypto/EllipticCurve/Hash.hh"
#include "Xi/Crypto/EllipticCurve/Point.hpp"

namespace Xi {
namespace Crypto {
namespace EllipticCurve {

struct Scalar : Memory::EnableSecureBlobFromThis<Scalar, scalarSize()> {
 public:
  /*!
   *
   * \brief fromMessageHash Hashes the message and reduce the hash to a scalar
   * \param message The message to hash
   * \return An error if the hash computaton failed, otherwise the recuded hash value.
   *
   */
  static Result<Scalar> fromMessageHash(ConstByteSpan message);
  static Result<Scalar> generateRandom();
  static Result<Scalar> generateRandom(ConstByteSpan seed);
  static Result<Scalar> fromKeyDerivation(const Point& derivation, const uint64_t outputIndex);
  static Result<Scalar> fromKeyDerivation(const Point& derivation, const uint64_t outputIndex, const Scalar& base);

 public:
  using EnableSecureBlobFromThis::EnableSecureBlobFromThis;
  Scalar(zero_t);
  Scalar(identity_t);
  XI_DEFAULT_COPY(Scalar);
  XI_DEFAULT_MOVE(Scalar);

  static const Scalar Null;

  bool isValid() const;
  operator bool() const;
  bool operator!() const;

  Point toPoint() const;

  void invert();
  Scalar inverted() const;
  Scalar operator~() const;

  Scalar& operator+=(const Scalar& rhs);
  Scalar operator+(const Scalar& rhs) const;

  Scalar& operator-=(const Scalar& rhs);
  Scalar operator-(const Scalar& rhs) const;

  Scalar& operator*=(const Scalar& rhs);
  Scalar operator*(const Scalar& rhs) const;

  Point operator*(basePoint_t) const;
  Point operator*(commitment_t) const;
  Point operator*(const Point& rhs) const;

 private:
  friend struct Signature;
  friend struct RingSignature;

  friend bool random(Scalar&);
  friend bool random(ConstByteSpan, Scalar&);
  friend Scalar powersSum(Scalar, size_t);
  friend bool toScalar(const Hash&, Scalar&);
};

using ScalarVector = std::vector<Scalar>;

XI_DECLARE_SPANS(Scalar)

[[nodiscard]] bool random(Scalar& out);
[[nodiscard]] bool random(ConstByteSpan seed, Scalar& out);

Point operator*(eight_t, const Point& rhs);
PointVector operator*(eight_t, const ConstPointSpan& rhs);
Point& operator*=(Point&, eight_t);
PointSpan operator*=(PointSpan&, eight_t);

PointVector operator*(ConstScalarSpan lhs, basePoint_t);
PointVector operator*(ConstScalarSpan lhs, commitment_t);

#define XI_CRYPTO_ECLLIPTIC_CURVE_DECLARE_SCALAR_VECTOR_OPERATIONS(OP) \
  ScalarVector operator OP(ConstScalarSpan lhs, ConstScalarSpan rhs);  \
  ScalarSpan operator OP##=(ScalarSpan lhs, ConstScalarSpan rhs);      \
  ScalarVector operator OP(ConstScalarSpan lhs, const Scalar& rhs);    \
  ScalarSpan operator OP##=(ScalarSpan lhs, const Scalar& rhs);

XI_CRYPTO_ECLLIPTIC_CURVE_DECLARE_SCALAR_VECTOR_OPERATIONS(+)
XI_CRYPTO_ECLLIPTIC_CURVE_DECLARE_SCALAR_VECTOR_OPERATIONS(-)
XI_CRYPTO_ECLLIPTIC_CURVE_DECLARE_SCALAR_VECTOR_OPERATIONS(*)

#undef XI_CRYPTO_ECLLIPTIC_CURVE_DECLARE_SCALAR_VECTOR_OPERATIONS

}  // namespace EllipticCurve
}  // namespace Crypto
}  // namespace Xi
