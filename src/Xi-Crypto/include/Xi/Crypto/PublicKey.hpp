﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <array>
#include <string>
#include <set>
#include <map>
#include <vector>
#include <ostream>

#include <Xi/Global.hh>
#include <Xi/Result.h>
#include <Xi/Byte.hh>
#include <Xi/Span.hpp>
#include <Serialization/ISerializer.h>
#include <Xi/Algorithm/GenericHash.h>
#include <Xi/Algorithm/GenericComparison.h>

#include "Xi/Crypto/EllipticCurve/Point.hpp"

namespace Xi {
namespace Crypto {
struct PublicKey : private EllipticCurve::Point {
  static const PublicKey Null;
  static inline constexpr size_t bytes() { return Point::bytes(); }
  static Xi::Result<PublicKey> fromString(const std::string& hex);

  PublicKey();
  explicit PublicKey(Xi::Null);
  explicit PublicKey(array_type raw);
  XI_DEFAULT_COPY(PublicKey);
  XI_DEFAULT_MOVE(PublicKey);
  ~PublicKey();

  using Point::Point;
  PublicKey(const Point& point);
  PublicKey(Point&& point);
  PublicKey& operator=(const Point& point);
  PublicKey& operator=(Point&& point);

  std::string toString() const;
  bool isNull() const;

  Xi::ConstByteSpan span() const;

  EllipticCurve::Point& asPoint();
  const EllipticCurve::Point& asPoint() const;

  /*
   * \brief checks that this is indeed a valid ecc point and has the right order.
   */
  bool isValid() const;

  void nullify();

 private:
  friend bool serialize(PublicKey&, Common::StringView name, CryptoNote::ISerializer&);
};

XI_MAKE_GENERIC_HASH_FUNC(PublicKey)
XI_MAKE_GENERIC_COMPARISON(PublicKey)

using PublicKeyVector = std::vector<PublicKey>;
XI_DECLARE_SPANS(PublicKey) using PublicKeySet = std::set<PublicKey>;
template <typename _ValueT>
using PublicKeyMap = std::map<PublicKey, _ValueT>;

[[nodiscard]] bool serialize(PublicKey& publicKey, Common::StringView name, CryptoNote::ISerializer& serializer);

std::ostream& operator<<(std::ostream& stream, const PublicKey& key);

}  // namespace Crypto
}  // namespace Xi

XI_MAKE_GENERIC_HASH_OVERLOAD(Xi::Crypto, PublicKey)

namespace Crypto {
using PublicKey = Xi::Crypto::PublicKey;
using PublicKeyVector = std::vector<PublicKey>;
XI_DECLARE_SPANS(PublicKey)
}  // namespace Crypto
