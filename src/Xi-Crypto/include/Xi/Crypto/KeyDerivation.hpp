﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <array>
#include <string>
#include <vector>

#include <Xi/Global.hh>
#include <Xi/Result.h>
#include <Xi/Byte.hh>
#include <Serialization/ISerializer.h>
#include <Xi/Algorithm/GenericHash.h>
#include <Xi/Algorithm/GenericComparison.h>

#include "Xi/Crypto/EllipticCurve/Point.hpp"

namespace Xi {
namespace Crypto {
struct KeyDerivation : private EllipticCurve::Point {
  static const KeyDerivation Null;
  static inline constexpr size_t bytes() { return Point::bytes(); }
  static Xi::Result<KeyDerivation> fromString(const std::string& hex);

  KeyDerivation();
  explicit KeyDerivation(array_type raw);
  XI_DEFAULT_COPY(KeyDerivation);
  XI_DEFAULT_MOVE(KeyDerivation);
  ~KeyDerivation();

  using Point::Point;
  KeyDerivation(const Point& point);
  KeyDerivation(Point&& point);
  KeyDerivation& operator=(const Point& point);
  KeyDerivation& operator=(Point&& point);

  std::string toString() const;

  bool isNull() const;
  bool isValid() const;

  Xi::ConstByteSpan span() const;

  EllipticCurve::Point& asPoint();
  const EllipticCurve::Point& asPoint() const;

  void nullify();

 private:
  friend bool serialize(KeyDerivation&, Common::StringView, CryptoNote::ISerializer&);
};

XI_MAKE_GENERIC_HASH_FUNC(KeyDerivation)
XI_MAKE_GENERIC_COMPARISON(KeyDerivation)

using KeyDerivationVector = std::vector<KeyDerivation>;
XI_DECLARE_SPANS(KeyDerivation)

[[nodiscard]] bool serialize(KeyDerivation& keyDerivation, Common::StringView name,
                             CryptoNote::ISerializer& serializer);

}  // namespace Crypto
}  // namespace Xi

XI_MAKE_GENERIC_HASH_OVERLOAD(Xi::Crypto, KeyDerivation)

namespace Crypto {
using KeyDerivation = Xi::Crypto::KeyDerivation;
using KeyDerivationVector = std::vector<KeyDerivation>;
XI_DECLARE_SPANS(KeyDerivation)
}  // namespace Crypto
