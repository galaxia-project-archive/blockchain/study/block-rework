﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <array>
#include <string>

#include <Xi/Global.hh>
#include <Xi/Result.h>
#include <Xi/Byte.hh>
#include <Serialization/ISerializer.h>
#include <Xi/Algorithm/GenericHash.h>
#include <Xi/Algorithm/GenericComparison.h>

#include "Xi/Crypto/EllipticCurve/Scalar.hpp"
#include "Xi/Crypto/PublicKey.hpp"

namespace Xi {
namespace Crypto {
struct SecretKey : private EllipticCurve::Scalar {
  static const SecretKey Null;
  static inline constexpr size_t bytes() { return Scalar::bytes(); }
  static Xi::Result<SecretKey> fromString(const std::string& hex);

  SecretKey();
  explicit SecretKey(array_type raw);
  XI_DEFAULT_COPY(SecretKey);
  XI_DEFAULT_MOVE(SecretKey);
  ~SecretKey();

  using Scalar::Scalar;
  SecretKey(const Scalar& scalar);
  SecretKey(Scalar&& scalar);
  SecretKey& operator=(const Scalar& scalar);
  SecretKey& operator=(Scalar&& scalar);

  PublicKey derivePublicKey() const;

  std::string toString() const;
  bool isNull() const;

  bool isValid() const;

  Xi::ConstByteSpan span() const;
  Xi::ByteSpan span();

  void nullify();

  EllipticCurve::Scalar& asScalar();
  const EllipticCurve::Scalar& asScalar() const;

 private:
  friend bool serialize(SecretKey& secretKey, Common::StringView, CryptoNote::ISerializer&);
};

XI_MAKE_GENERIC_HASH_FUNC(SecretKey)
XI_MAKE_GENERIC_COMPARISON(SecretKey)

using SecretKeyVector = std::vector<SecretKey>;
XI_DECLARE_SPANS(SecretKey);

[[nodiscard]] bool serialize(SecretKey& secretKey, Common::StringView name, CryptoNote::ISerializer& serializer);

}  // namespace Crypto
}  // namespace Xi

XI_MAKE_GENERIC_HASH_OVERLOAD(Xi::Crypto, SecretKey)

namespace Crypto {
using SecretKey = Xi::Crypto::SecretKey;
using SecretKeyVector = std::vector<SecretKey>;
XI_DECLARE_SPANS(SecretKey)
}  // namespace Crypto
