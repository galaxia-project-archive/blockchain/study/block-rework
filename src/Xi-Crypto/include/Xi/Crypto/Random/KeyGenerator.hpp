﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <variant>

#include <Serialization/ISerializer.h>
#include <Serialization/OptionalSerialization.hpp>
#include <Serialization/VariantSerialization.hpp>

#include "Xi/Crypto/Random/IKeyGenerator.hpp"
#include "Xi/Crypto/Random/SaltedKeyGenerator.hpp"

namespace Xi {
namespace Crypto {
namespace Random {

class KeyGenerator final : public IKeyGenerator {
 public:
  ~KeyGenerator() override = default;

  [[nodiscard]] Result<KeyPair> operator()() const override;
  [[nodiscard]] Result<KeyPair> operator()(ConstByteSpan seed) const override;
};

// clang-format off
XI_SERIALIZATION_VARIANT_INVARIANT(
    SpecificKeyGeneratorConfiguration,
      SaltedKeyGeneratorConfig
)
// clang-format on

using KeyGeneratorConfiguration = std::optional<SpecificKeyGeneratorConfiguration>;

std::shared_ptr<IKeyGenerator> makeKeyGenerator(const KeyGeneratorConfiguration& config);

}  // namespace Random
}  // namespace Crypto
}  // namespace Xi

XI_SERIALIZATION_VARIANT_TAG(Xi::Crypto::Random::SpecificKeyGeneratorConfiguration, 0, 0x0001, "salted")
