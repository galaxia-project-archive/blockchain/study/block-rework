﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Serialization/ISerializer.h>

#include "Xi/Crypto/PublicKey.hpp"
#include "Xi/Crypto/SecretKey.hpp"

namespace Xi {
namespace Crypto {
class KeyPair {
 public:
  XI_PROPERTY(PublicKey, publicKey)
  XI_PROPERTY(SecretKey, secretKey)

  bool isValid() const;

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(m_publicKey, "public_key")
  KV_MEMBER_RENAME(m_secretKey, "secret_key")
  KV_END_SERIALIZATION
};

}  // namespace Crypto
}  // namespace Xi

namespace CryptoNote {
using KeyPair = Xi::Crypto::KeyPair;
}
