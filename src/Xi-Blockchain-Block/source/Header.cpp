﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Block/Header.hpp"

namespace Xi {
namespace Blockchain {
namespace Block {

bool Header::serialize(CryptoNote::ISerializer &serializer) { return serialize(serializer, false); }

bool Header::serializePrefix(CryptoNote::ISerializer &serializer) { return serialize(serializer, true); }

bool Header::serialize(CryptoNote::ISerializer &serializer, const bool prefix) {
  XI_RETURN_EC_IF_NOT(serializer(m_version, "version"), false);
  if (!prefix) {
    XI_RETURN_EC_IF_NOT(serializer(m_nonce, "nonce"), false);
  }
  XI_RETURN_EC_IF_NOT(serializer(m_features, "features"), false);
  XI_RETURN_EC_IF_NOT(serializer(m_timestampShift, "timestamp_shift"), false);
  XI_RETURN_EC_IF_NOT(serializer(m_previousHash, "previous_hash"), false);

  if (!prefix) {
    if (hasFlag(features(), Features::MergeMining)) {
      if (serializer.isOutput()) {
        XI_RETURN_EC_IF_NOT(mergeMiningTag(), false);
      } else {
        mergeMiningTag(MergeMiningTag{null});
      }
      XI_RETURN_EC_IF_NOT(serializer(*m_mergeMiningTag, "merge_mining_tag"), false);
    } else {
      if (serializer.isOutput()) {
        XI_RETURN_EC_IF(mergeMiningTag().has_value(), false);
      } else {
        mergeMiningTag(std::nullopt);
      }
    }
  }

  return true;
}

Features Header::features() const { return m_features; }

const std::optional<MergeMiningTag> &Header::mergeMiningTag() const { return m_mergeMiningTag; }

Header &Header::mergeMiningTag(const std::optional<MergeMiningTag> &tag) {
  m_mergeMiningTag = tag;
  if (m_mergeMiningTag) {
    m_features = addFlag(m_features, Features::MergeMining);
  } else {
    m_features = discardFlag(m_features, Features::MergeMining);
  }
  return *this;
}

}  // namespace Block
}  // namespace Blockchain
}  // namespace Xi
