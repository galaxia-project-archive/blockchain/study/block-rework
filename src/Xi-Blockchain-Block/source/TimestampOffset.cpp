﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Block/TimestampOffset.hpp"

#include <limits>

#include <Xi/Exceptions.hpp>
#include <Xi/Algorithm/Math.h>

namespace {
const int32_t oneYearInSeconds = 365 * 24 * 60 * 60;
}

namespace Xi {
namespace Blockchain {
namespace Block {

bool serialize(TimestampOffset &timestamp, Common::StringView name, CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer(timestamp.value, name), false);
  return timestamp.isValid();
}

Result<TimestampOffset> TimestampOffset::fromTimestamps(const uint64_t origin, const uint64_t target) {
  XI_ERROR_TRY();
  const auto signedOrigin = static_cast<int64_t>(origin);
  const auto signedTarget = static_cast<int64_t>(target);
  exceptional_if<OutOfRangeError>(origin < target && signedTarget - signedOrigin > oneYearInSeconds);
  exceptional_if<OutOfRangeError>(target > origin && signedOrigin - signedTarget > oneYearInSeconds);
  return success(TimestampOffset{static_cast<value_type>(static_cast<int64_t>(target) - static_cast<int64_t>(origin))});
  XI_ERROR_CATCH();
}

bool TimestampOffset::isValid() const { return value >= -oneYearInSeconds && value <= oneYearInSeconds; }

uint64_t TimestampOffset::shift(const uint64_t timestamp) const {
  exceptional_if<OutOfRangeError>(timestamp > std::numeric_limits<int64_t>::max());
  const auto signedTimestamp = static_cast<int64_t>(timestamp);
  exceptional_if<OutOfRangeError>(signedTimestamp + value < 0);
  return static_cast<uint64_t>(signedTimestamp + value);
}

}  // namespace Block
}  // namespace Blockchain
}  // namespace Xi
