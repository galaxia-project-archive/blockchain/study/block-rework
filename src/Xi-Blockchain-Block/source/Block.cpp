﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Block/Block.hpp"

#include <Xi/Exceptions.hpp>

namespace Xi {
namespace Blockchain {
namespace Block {

bool Block::serialize(CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer(m_header, "header"), false);

  if (serializer.isInput()) {
    if (hasFlag(header().features(), Features::Currency)) {
      currency(Currency::BlockEntity{null});
      XI_RETURN_EC_IF_NOT(serializer(*m_currency, "currency"), false);
    } else {
      currency(std::nullopt);
    }
  } else if (serializer.isOutput()) {
    if (hasFlag(header().features(), Features::Currency)) {
      XI_RETURN_EC_IF_NOT(currency(), false);
      XI_RETURN_EC_IF_NOT(serializer(*m_currency, "currency"), false);
    } else {
      XI_RETURN_EC_IF(currency(), false);
    }
  } else {
    XI_RETURN_EC(false);
  }

  return true;
}

std::optional<Currency::BlockEntity> Block::currency() const { return m_currency; }

Block &Block::currency(const std::optional<Currency::BlockEntity> &_currency) {
  m_currency = _currency;
  if (m_currency.has_value()) {
    header().features(addFlag(header().features(), Features::Currency));
  } else {
    header().features(discardFlag(header().features(), Features::Currency));
  }
  return *this;
}

Shell Block::shell() const {
  Shell reval{null};
  reval.header(header());
  if (currency()) {
    reval.entities().emplace_back(currency()->hash());
  }
  return reval;
}

}  // namespace Block
}  // namespace Blockchain
}  // namespace Xi
