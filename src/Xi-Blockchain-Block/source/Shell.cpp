﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Block/Shell.hpp"

#include <algorithm>
#include <iterator>

#include <Xi/Exceptions.hpp>
#include <Common/VectorOutputStream.h>
#include <Serialization/BinaryOutputStreamSerializer.h>

namespace Xi {
namespace Blockchain {
namespace Block {

Hash Shell::proofOfWorkHash() const {
  HashVector hashes{};
  hashes.reserve(1 + entities().size());

  {
    ByteVector blob{};
    Common::VectorOutputStream stream{blob};
    CryptoNote::BinaryOutputStreamSerializer serializer{stream};
    if (!const_cast<Header&>(header()).serializePrefix(serializer)) {
      exceptional<RuntimeError>("block prefix serialization failed");
    }
    hashes.emplace_back(Hash::compute(blob).takeOrThrow());
  }

  std::copy(begin(entities()), end(entities()), back_inserter(hashes));
  return Hash::computeMerkleTree(hashes).takeOrThrow();
}

ProofOfWorkPrefix Shell::proofOfWorkPrefix() const {
  ProofOfWorkPrefix reval{null};
  reval.nonce() = header().nonce();
  if (header().mergeMiningTag()) {
    const auto& mergeTag = *header().mergeMiningTag();
    HashVector hashes{};
    hashes.reserve(mergeTag.prefix().size() + mergeTag.suffix().size() + 1);
    std::copy(begin(mergeTag.prefix()), end(mergeTag.prefix()), back_inserter(hashes));
    hashes.emplace_back(proofOfWorkHash());
    std::copy(begin(mergeTag.suffix()), end(mergeTag.suffix()), back_inserter(hashes));
    reval.mergedHash(Hash::computeMerkleTree(hashes).takeOrThrow());
  } else {
    reval.mergedHash(proofOfWorkHash());
  }
  return reval;
}

}  // namespace Block
}  // namespace Blockchain
}  // namespace Xi
