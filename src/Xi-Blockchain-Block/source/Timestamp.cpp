﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Block/Timestamp.hpp"

#include <limits>

namespace Xi {
namespace Blockchain {
namespace Block {

const Timestamp Timestamp::Null{0};

bool serialize(Timestamp &timestamp, Common::StringView name, CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer(timestamp.value, name), false);
  XI_RETURN_EC_IF_NOT(timestamp.isValid(), false);
  XI_RETURN_SC(true);
}

Timestamp::Timestamp(Xi::Null) : EnableIntegralFromThis(Null) {}

bool Timestamp::isNull() const { return (*this) == Null; }

bool Timestamp::isValid() const {
  const uint64_t secondsInTenThousandYears = 10ULL * 1000ULL * 365ULL * 60ULL * 60ULL;
  XI_RETURN_EC_IF(native() > secondsInTenThousandYears, false);
  return true;
}

int64_t Timestamp::signedNative() const { return static_cast<int64_t>(native()); }

}  // namespace Block
}  // namespace Blockchain
}  // namespace Xi
