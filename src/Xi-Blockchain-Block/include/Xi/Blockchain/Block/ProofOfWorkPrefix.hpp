﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>
#include <Xi/ErrorCode.hpp>
#include <Serialization/ISerializer.h>

#include "Xi/Blockchain/Block/Nonce.hpp"
#include "Xi/Blockchain/Block/Hash.hpp"

namespace Xi {
namespace Blockchain {
namespace Block {

class ProofOfWorkPrefix {
 public:
  using blob_type = ByteArray<Nonce::bytes() + Hash::bytes()>;

 private:
  blob_type m_buffer{};

 public:
  Nonce nonce() const;
  ProofOfWorkPrefix& nonce(const Nonce& nonce);

  Hash mergedHash() const;
  ProofOfWorkPrefix& mergedHash(const Hash& mergedHash);

  const blob_type& blob() const;

 public:
  ProofOfWorkPrefix(Null = null) { m_buffer.fill(0); }
};

}  // namespace Block
}  // namespace Blockchain
}  // namespace Xi
