﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <vector>

#include <Xi/Global.hh>
#include <Serialization/ISerializer.h>

#include "Xi/Blockchain/Block/Header.hpp"
#include "Xi/Blockchain/Block/Shell.hpp"
#include "Xi/Blockchain/Currency/BlockEntity.hpp"

namespace Xi {
namespace Blockchain {
namespace Block {

class Block {
 public:
  XI_PROPERTY(Header, header, null)

 private:
  std::optional<Currency::BlockEntity> m_currency{std::nullopt};

 public:
  std::optional<Currency::BlockEntity> currency() const;
  Block& currency(const std::optional<Currency::BlockEntity>& _reward);

 public:
  Block(Null = null) {}

  Shell shell() const;

  [[nodiscard]] bool serialize(CryptoNote::ISerializer& serializer);
};

}  // namespace Block
}  // namespace Blockchain
}  // namespace Xi
