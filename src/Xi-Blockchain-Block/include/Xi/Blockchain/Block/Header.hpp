﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <optional>

#include <Xi/Global.hh>
#include <Xi/Crypto/Hash/Crc.hpp>
#include <Serialization/ISerializer.h>

#include "Xi/Blockchain/Block/Version.hpp"
#include "Xi/Blockchain/Block/Nonce.hpp"
#include "Xi/Blockchain/Block/TimestampOffset.hpp"
#include "Xi/Blockchain/Block/Hash.hpp"
#include "Xi/Blockchain/Block/Features.hpp"
#include "Xi/Blockchain/Block/MergeMiningTag.hpp"

namespace Xi {
namespace Blockchain {
namespace Block {

struct Header {
  XI_PROPERTY(Version, version, Version::Null)
  XI_PROPERTY(Nonce, nonce, null)
  XI_PROPERTY(TimestampOffset, timestampShift, TimestampOffset{0})
  XI_PROPERTY(Hash, previousHash, null)

 private:
  Features m_features{Features::None};

 public:
  Features features() const;
  Header& features(Features _features);

 private:
  std::optional<MergeMiningTag> m_mergeMiningTag{std::nullopt};

 public:
  const std::optional<MergeMiningTag>& mergeMiningTag() const;
  Header& mergeMiningTag(const std::optional<MergeMiningTag>& tag);

 public:
  [[nodiscard]] bool serialize(CryptoNote::ISerializer& serializer);
  [[nodiscard]] bool serializePrefix(CryptoNote::ISerializer& serializer);

 private:
  [[nodiscard]] bool serialize(CryptoNote::ISerializer& serializer, const bool prefix);

 public:
  Header(Null = null) {}
};

}  // namespace Block
}  // namespace Blockchain
}  // namespace Xi

namespace CryptoNote {
using BlockHeader = Xi::Blockchain::Block::Header;
}
