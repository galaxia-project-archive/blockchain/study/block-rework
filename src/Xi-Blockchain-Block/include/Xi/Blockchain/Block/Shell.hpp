﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Serialization/ISerializer.h>
#include <Serialization/SerializationOverloads.h>

#include "Xi/Blockchain/Block/Header.hpp"
#include "Xi/Blockchain/Block/ProofOfWorkPrefix.hpp"
#include "Xi/Blockchain/Currency/Reward.hpp"

namespace Xi {
namespace Blockchain {
namespace Block {

class Shell {
 public:
  XI_PROPERTY(Header, header, null)
  XI_PROPERTY(Crypto::FastHashVector, entities, Crypto::FastHash::Null)

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(m_header, header)
  KV_MEMBER_RENAME(m_entities, entities)
  KV_END_SERIALIZATION

  Shell(Null = null) {}

  Hash proofOfWorkHash() const;
  ProofOfWorkPrefix proofOfWorkPrefix() const;
};

}  // namespace Block
}  // namespace Blockchain
}  // namespace Xi

namespace CryptoNote {
using BlockTemplate = Xi::Blockchain::Block::Shell;
}
