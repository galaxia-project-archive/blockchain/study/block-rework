﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <Xi/Global.hh>
#include <Xi/TypeSafe/Flag.hpp>
#include <Serialization/FlagSerialization.hpp>

namespace Xi {
namespace Blockchain {
namespace Block {

/// Alternative to optionals, based on emplaced features block serialization is altered.
enum struct Features {
  None = 0,
  /// Header contains an additional version to vote for an upgrade.
  UpgradeVote = 1 << 0,
  /// Merge mining, embedds prefix hashes of merge mined blocks.
  MergeMining = 1 << 1,
  /// Header contains a currency entity
  Currency = 1 << 2,
};

XI_TYPESAFE_FLAG_MAKE_OPERATIONS(Features)
XI_SERIALIZATION_FLAG(Features)

}  // namespace Block
}  // namespace Blockchain
}  // namespace Xi

XI_SERIALIZATION_FLAG_RANGE(Xi::Blockchain::Block::Features, UpgradeVote, Currency)
XI_SERIALIZATION_FLAG_TAG(Xi::Blockchain::Block::Features, UpgradeVote, "upgrade_vote")
XI_SERIALIZATION_FLAG_TAG(Xi::Blockchain::Block::Features, MergeMining, "merge_mining")
XI_SERIALIZATION_FLAG_TAG(Xi::Blockchain::Block::Features, Currency, "currency")

namespace CryptoNote {
using BlockFeatures = Xi::Blockchain::Block::Features;
}
