﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <vector>

#include <Xi/Global.hh>
#include <Serialization/ISerializer.h>
#include <Serialization/SerializationOverloads.h>
#include <Serialization/OptionalSerialization.hpp>
#include <Serialization/BlobWrapper.hpp>

#include "Xi/Blockchain/Block/Header.hpp"
#include "Xi/Blockchain/Block/Shell.hpp"
#include "Xi/Blockchain/Currency/Reward.hpp"
#include "Xi/Blockchain/Currency/Transfer.hpp"

namespace Xi {
namespace Blockchain {
namespace Block {

struct RawBlock {
  CryptoNote::BlobWrapper<Header> header;
  std::optional<CryptoNote::BlobWrapper<Currency::Reward>> reward;
  std::vector<CryptoNote::BlobWrapper<Currency::Transfer>> transfers;

  KV_BEGIN_SERIALIZATION
  KV_MEMBER(header)
  KV_MEMBER(reward)
  KV_MEMBER(transfers)
  KV_END_SERIALIZATION
};

}  // namespace Block
}  // namespace Blockchain
}  // namespace Xi

namespace CryptoNote {
using RawBlock = Xi::Blockchain::Block::RawBlock;
using RawBlockVector = std::vector<RawBlock>;
XI_DECLARE_SPANS(RawBlock)
}  // namespace CryptoNote
