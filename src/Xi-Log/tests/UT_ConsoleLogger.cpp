﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <Xi/ExternalIncludePush.h>
#include <gmock/gmock.h>
#include <Xi/ExternalIncludePop.h>

#include <iostream>

#include <Xi/Log/Sink.hpp>
#include <Xi/Log/Handle.hpp>
#include <Xi/Log/Registry.hpp>

TEST(Xi_Log_ConsoleLogger, StdOutSink) {
  using namespace ::testing;
  using namespace Xi::Log;

  Registry::put(std::make_shared<Sink>(std::cout));
  auto handle = Registry::get("");
  handle.trace("The answer is {}. Just took {:.2f} seconds to find out.", 42, 1.23);
}
