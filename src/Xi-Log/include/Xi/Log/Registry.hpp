﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <map>

#include <Xi/Concurrent/RecursiveLock.h>

#include "Xi/Log/Category.hpp"
#include "Xi/Log/Handle.hpp"

namespace Xi {
namespace Log {

class Registry final {
 private:
  Registry();
  static Registry &singleton();

 public:
  ~Registry();

  static Handle get(std::string_view category);

  /*!
   * \brief put adds a new logger to the system.
   * \param logger the logger used by every handle.
   */
  static void put(SharedILogger logger);

 private:
  Handle doGet(std::string_view category);
  void doPut(SharedILogger logger);

 private:
  SharedCategory getCategory(const std::string &category, const std::string &path);

 private:
  class LoggerWrapper;

  std::map<std::string, SharedCategory> m_categories;
  std::shared_ptr<LoggerWrapper> m_logger;
  Concurrent::RecursiveLock m_guard;
};

}  // namespace Log
}  // namespace Xi
