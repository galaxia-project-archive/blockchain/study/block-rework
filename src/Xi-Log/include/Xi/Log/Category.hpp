﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <vector>
#include <string>
#include <string_view>

#include <Xi/Global.hh>

namespace Xi {
namespace Log {

XI_DECLARE_SMART_POINTER_CLASS(Category)

class Category {
 private:
  Category(std::string_view name, std::string_view fullName);
  friend class Registry;

 public:
  ~Category() = default;

  const std::string& name() const;
  std::string fullName() const;

  WeakCategory parent() const;
  const std::vector<WeakCategory>& children() const;

 private:
  std::string m_name;
  std::string m_fullName;
  WeakCategory m_parent;
  std::vector<WeakCategory> m_children;
};

}  // namespace Log
}  // namespace Xi
