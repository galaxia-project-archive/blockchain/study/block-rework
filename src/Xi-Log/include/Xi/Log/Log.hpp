﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#if !defined(XI_LOG_LOG_HPP)
#define XI_LOG_LOG_HPP 1

#include "Xi/Log/Category.hpp"
#include "Xi/Log/Level.hpp"
#include "Xi/Log/Context.hpp"
#include "Xi/Log/Registry.hpp"
#include "Xi/Log/Handle.hpp"

#define XI_LOGGER(CATEGORY) static ::Xi::Log::Handle LogHandle = ::Xi::Log::Registry::get(CATEGORY);

#define XI_LOG(LEVEL) LogHandle.log<::Xi::Log::Level::LEVEL>
#define XI_LOG_IF(LEVEL) LogHandle.logIf<::Xi::Log::Level::LEVEL>
#define XI_LOG_IF_NOT(LEVEL) LogHandle.logIfNot<::Xi::Log::Level::LEVEL>

#define XI_TRACE XI_LOG(Trace)
#define XI_TRACE_IF XI_LOG_IF(Trace)
#define XI_TRACE_IF_NOT XI_LOG_IF_NOT(Trace)

#define XI_DEBUG XI_LOG(Debug)
#define XI_DEBUG_IF XI_LOG_IF(Debug)
#define XI_DEBUG_IF_NOT XI_LOG_IF_NOT(Debug)

#define XI_INFO XI_LOG(Info)
#define XI_INFO_IF XI_LOG_IF(Info)
#define XI_INFO_IF_NOT XI_LOG_IF_NOT(Info)

#define XI_WARN XI_LOG(Warn)
#define XI_WARN_IF XI_LOG_IF(Warn)
#define XI_WARN_IF_NOT XI_LOG_IF_NOT(Warn)

#define XI_ERROR XI_LOG(Error)
#define XI_ERROR_IF XI_LOG_IF(Error)
#define XI_ERROR_IF_NOT XI_LOG_IF_NOT(Error)

#define XI_FATAL XI_LOG(Fatal)
#define XI_FATAL_IF XI_LOG_IF(Fatal)
#define XI_FATAL_IF_NOT XI_LOG_IF_NOT(Fatal)

#endif

#if defined(XI_VERBOSE)
#undef XI_VERBOSE
#endif

#if defined(XI_VERBOSE_IF)
#undef XI_VERBOSE_IF
#endif

#if defined(XI_VERBOSE_IF_NOT)
#undef XI_VERBOSE_IF_NOT
#endif

#if defined(XI_LOG_VERBOSE)

#define XI_VERBOSE XI_DEBUG
#define XI_VERBOSE_IF XI_DEBUG_IF
#define XI_VERBOSE_IF_NOT XI_DEBUG_IF_NOT

#undef XI_LOG_VERBOSE
#else

#include <Xi/Global.hh>

#define XI_VERBOSE(...)
#define XI_VERBOSE_IF(...)
#define XI_VERBOSE_IF_NOT(...)

#endif
