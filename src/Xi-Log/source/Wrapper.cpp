﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Log/Wrapper.hpp"

#include <Xi/Exceptions.hpp>

Xi::Log::Wrapper::Wrapper(Xi::Log::SharedILogger inner) : m_inner{inner} {
  exceptional_if<NullArgumentError>(inner.get() == nullptr, "log wrappers required none null internal logger");
}

bool Xi::Log::Wrapper::isFiltered(const Xi::Log::Level level) const { return m_inner->isFiltered(level); }

void Xi::Log::Wrapper::print(Xi::Log::Context context, std::string_view message) { m_inner->print(context, message); }
