﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Log/Filter.hpp"

const Xi::Log::Level Xi::Log::Filter::DefaultLevel {
#if defined(NDEBUG)
  Level::Info
#else
  Level::Debug
#endif
};

Xi::Log::Filter::Filter(SharedILogger inner) : Wrapper(inner), m_level{DefaultLevel} {}

Xi::Log::Filter::Filter(SharedILogger inner, Level options) : Wrapper(inner), m_level{options} {}

bool Xi::Log::Filter::isFiltered(const Xi::Log::Level _level) const {
  return static_cast<uint8_t>(level()) > static_cast<uint8_t>(_level);
}

Xi::Log::Level Xi::Log::Filter::level() const { return m_level.load(std::memory_order_consume); }

void Xi::Log::Filter::setLevel(Xi::Log::Level level) { m_level.store(level, std::memory_order_consume); }
