﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Log/Category.hpp"

#include "Xi/Log/Registry.hpp"

Xi::Log::Category::Category(std::string_view name, std::string_view fullName) : m_name{name}, m_fullName{fullName} {}

const std::string &Xi::Log::Category::name() const { return m_name; }

std::string Xi::Log::Category::fullName() const { return m_fullName; }

Xi::Log::WeakCategory Xi::Log::Category::parent() const { return m_parent; }

const std::vector<Xi::Log::WeakCategory> &Xi::Log::Category::children() const { return m_children; }
