﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Log/Compound.hpp"

#include <algorithm>
#include <iterator>

Xi::Log::Compound::Compound() {}

Xi::Log::Compound::Compound(std::initializer_list<Xi::Log::SharedILogger> logger)

{
  std::copy_if(begin(logger), end(logger), back_inserter(m_logger), [](const auto& i) { return i.get() != nullptr; });
}

bool Xi::Log::Compound::isFiltered(const Xi::Log::Level level) const {
  return std::all_of(begin(m_logger), end(m_logger),
                     [level](const auto& iLogger) { return iLogger->isFiltered(level); });
}

void Xi::Log::Compound::print(Xi::Log::Context context, std::string_view message) {
  for (auto& iLogger : m_logger) {
    if (!iLogger->isFiltered(context.level())) {
      iLogger->print(context, message);
    }
  }
}
