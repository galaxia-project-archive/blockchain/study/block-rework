﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Log/Handle.hpp"

Xi::Log::Handle::Handle(WeakCategory category, Xi::Log::WeakILogger logger) : m_category{category}, m_logger{logger} {}

void Xi::Log::Handle::log(const Xi::Log::Level level, std::string_view message) {
  if (!isFiltered(level)) {
    print(level, message);
  }
}

void Xi::Log::Handle::log(const Xi::Log::Level level, std::string_view message, fmt::format_args args) {
  if (!isFiltered(level)) {
    try {
      std::string _message{message};
      print(level, fmt::vformat(_message, args));
    } catch (std::exception& e) {
      log(Level::Error, e.what());
    }
  }
}

bool Xi::Log::Handle::isFiltered(const Xi::Log::Level level) const {
  if (auto locked = m_logger.lock()) {
    return locked->isFiltered(level);
  } else {
    return true;
  }
}

void Xi::Log::Handle::print(const Level level, std::string_view message) {
  if (auto locked = m_logger.lock()) {
    return locked->print(Context::current(level, m_category), message);
  }
}
