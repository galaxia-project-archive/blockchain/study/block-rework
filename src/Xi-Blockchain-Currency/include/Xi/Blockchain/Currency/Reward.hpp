﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <vector>

#include <Xi/Global.hh>
#include <Xi/Span.hpp>
#include <Serialization/ISerializer.h>
#include <Serialization/VariantSerialization.hpp>

#include "Xi/Blockchain/Currency/PublicAddress.hpp"
#include "Xi/Blockchain/Currency/KeyOutput.hpp"
#include "Xi/Blockchain/Currency/RewardVersion.hpp"

namespace Xi {
namespace Blockchain {
namespace Currency {

class RewardOutput {
 public:
  XI_PROPERTY(Crypto::PublicKey, publicKey, Crypto::PublicKey::Null)
  XI_PROPERTY(KeyOutputVector, outputs, {})
  RewardOutput(Null = null) {}

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(m_publicKey, public_key)
  KV_MEMBER_RENAME(m_outputs, outputs)
  KV_END_SERIALIZATION
};

class Reward {
 public:
  XI_PROPERTY(RewardVersion, version, 0)
  XI_PROPERTY(RewardOutput, output, null)

  KV_BEGIN_SERIALIZATION
  KV_MEMBER_RENAME(version(), version)
  XI_RETURN_EC_IF_NOT(version() == RewardVersion{1}, false);
  KV_MEMBER_RENAME(output(), output)
  KV_END_SERIALIZATION

  Crypto::FastHash hash() const;
};

}  // namespace Currency
}  // namespace Blockchain
}  // namespace Xi

namespace CryptoNote {
using BlockReward = Xi::Blockchain::Currency::Reward;
}
