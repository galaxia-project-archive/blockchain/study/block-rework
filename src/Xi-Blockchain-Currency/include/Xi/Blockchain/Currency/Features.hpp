﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/TypeSafe/Flag.hpp>
#include <Serialization/FlagSerialization.hpp>

namespace Xi {
namespace Blockchain {
namespace Currency {

enum struct Features {
  None = 0,
  Reward = 1 << 0,
  StaticReward = 1 << 1,
  Transfers = 1 << 2,
};

XI_TYPESAFE_FLAG_MAKE_OPERATIONS(Features)
XI_SERIALIZATION_FLAG(Features)

}  // namespace Currency
}  // namespace Blockchain
}  // namespace Xi

XI_SERIALIZATION_FLAG_RANGE(Xi::Blockchain::Currency::Features, Reward, Transfers)
XI_SERIALIZATION_FLAG_TAG(Xi::Blockchain::Currency::Features, Reward, "reward")
XI_SERIALIZATION_FLAG_TAG(Xi::Blockchain::Currency::Features, StaticReward, "static_reward")
XI_SERIALIZATION_FLAG_TAG(Xi::Blockchain::Currency::Features, Transfers, "transfers")
