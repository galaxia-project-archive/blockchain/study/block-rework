﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <Xi/TypeSafe/Flag.hpp>
#include <Serialization/ISerializer.h>
#include <Serialization/FlagSerialization.hpp>

namespace Xi {
namespace Blockchain {
namespace Currency {

/*!
 * \attention If a transfer comes in that could have used one of those features to shrink transaction size, consensus
 * will reject the transaction. Thus if one of the flags is applicable for a transfe you may use it.
 *
 * If you do not use mixins at all, you may not set StaticRingSize, because it is already known that the ring size must
 * be one on all inputs.
 */
enum struct TransferFeatures {
  /// No features given, plain transfer without any mixin, ring size is assumed to be one on all inputs
  None = 0,
  /// Transfer uses mixins, thus some inputs may have multiple references to outputs.
  Mixins = 1 << 0,
  /// Transfer contains a static ring size used on all inputs.
  StaticRingSize = 1 << 1,
  /// Transfer provides a public key such that a sender can proof (more easily) that he was the sender.
  /// Can also be used to perform a key exchange with the reciever using the transaction public key.
  PaymentIdentifier = 1 << 2,
  /// Transfer contains a locking, making transfered coins unusable till the unlock time/height is reached.
  Unlock = 1 << 3,
};

XI_TYPESAFE_FLAG_MAKE_OPERATIONS(TransferFeatures)
XI_SERIALIZATION_FLAG(TransferFeatures)

}  // namespace Currency
}  // namespace Blockchain
}  // namespace Xi

XI_SERIALIZATION_FLAG_RANGE(Xi::Blockchain::Currency::TransferFeatures, Mixins, Unlock)
XI_SERIALIZATION_FLAG_TAG(Xi::Blockchain::Currency::TransferFeatures, Mixins, "mixins")
XI_SERIALIZATION_FLAG_TAG(Xi::Blockchain::Currency::TransferFeatures, StaticRingSize, "static_ring_size")
XI_SERIALIZATION_FLAG_TAG(Xi::Blockchain::Currency::TransferFeatures, PaymentIdentifier, "payment_identifer")
XI_SERIALIZATION_FLAG_TAG(Xi::Blockchain::Currency::TransferFeatures, Unlock, "unlock")
