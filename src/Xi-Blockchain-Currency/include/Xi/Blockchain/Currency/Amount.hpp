﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <Xi/Global.hh>
#include <Xi/Span.hpp>
#include <Xi/TypeSafe/Integral.hpp>
#include <Xi/TypeSafe/Arithmetic.hpp>
#include <Serialization/ISerializer.h>
#include <Serialization/TypeSafeSerialization.hpp>

namespace Xi {
namespace Blockchain {
namespace Currency {

struct Amount : TypeSafe::EnableArithmeticFromThis<uint64_t, Amount> {
  using EnableArithmeticFromThis::EnableArithmeticFromThis;

  constexpr static value_type min = std::numeric_limits<value_type>::min();
  constexpr static value_type max = std::numeric_limits<std::make_signed_t<value_type>>::max() / 2;
};

XI_SERIALIAZTION_TYPESAFE_MAX(Amount, Amount::max)

using AmountVector = std::vector<Amount>;
XI_DECLARE_SPANS(Amount)

struct CanonicalAmount : TypeSafe::EnableIntegralFromThis<Amount::value_type, CanonicalAmount> {
  CanonicalAmount(Null = null);
  using EnableIntegralFromThis::EnableIntegralFromThis;

  bool isValid() const;

 private:
  friend bool serialize(CanonicalAmount&, Common::StringView, CryptoNote::ISerializer&);
};

using CanonicalAmountVector = std::vector<CanonicalAmount>;
XI_DECLARE_SPANS(CanonicalAmount)

[[nodiscard]] bool serialize(CanonicalAmount& amount, Common::StringView name, CryptoNote::ISerializer& serializer);

[[nodiscard]] bool isCanonicalAmount(const Amount amount);
[[nodiscard]] uint64_t countCanonicalAmountBuckets(ConstAmountSpan amounts);
[[nodiscard]] Amount cutDigitsFromAmount(Amount amount, const size_t count);
[[nodiscard]] CanonicalAmountVector canonicalDecomposition(Amount amount);

}  // namespace Currency
}  // namespace Blockchain
}  // namespace Xi

XI_TYPESAFE_HASH_OVERLOAD(Xi::Blockchain::Currency, Amount)
XI_TYPESAFE_HASH_OVERLOAD(Xi::Blockchain::Currency, CanonicalAmount)
