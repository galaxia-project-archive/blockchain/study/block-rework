﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <vector>

#include <Xi/Global.hh>
#include <Xi/Span.hpp>
#include <Serialization/ISerializer.h>
#include <Serialization/OptionalSerialization.hpp>
#include <Xi/Crypto/Signature.hpp>

#include "Xi/Blockchain/Currency/TransferPrefix.hpp"
#include "Xi/Blockchain/Currency/Unlock.hpp"

namespace Xi {
namespace Blockchain {
namespace Currency {

using TransferSignature = std::vector<::Crypto::Signature>;
using TransferSignatureVector = std::vector<TransferSignature>;
XI_DECLARE_SPANS(TransferSignature)

class Transfer {
 public:
  XI_PROPERTY(TransferPrefix, prefix, null)
  XI_PROPERTY(TransferSignatureVector, signatures, {})

  [[nodiscard]] bool serialize(CryptoNote::ISerializer& serializer);

  Crypto::FastHash hash() const;
  Crypto::FastHash prefixHash() const;

  Transfer(Null = null) {}
};

using TransferVector = std::vector<Transfer>;
XI_DECLARE_SPANS(Transfer)

}  // namespace Currency
}  // namespace Blockchain
}  // namespace Xi
