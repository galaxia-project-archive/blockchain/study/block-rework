﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.hh>
#include <crypto/CryptoTypes.h>
#include <Serialization/ISerializer.h>
#include <Serialization/SerializationOverloads.h>

#include "Xi/Blockchain/Currency/Amount.hpp"
#include "Xi/Blockchain/Currency/OutputIndex.hpp"
#include "Xi/Blockchain/Currency/RingSize.hpp"

namespace Xi {
namespace Blockchain {
namespace Currency {

class KeyInput {
 public:
  XI_PROPERTY(CanonicalAmount, amount)
  XI_PROPERTY(::Crypto::KeyImage, keyImage)
  XI_PROPERTY(OutputIndexVector, outputs)

  [[nodiscard]] bool serialize(CryptoNote::ISerializer& serializer);
  [[nodiscard]] bool serializeStatic(CryptoNote::ISerializer& serializer, const RingSize ringSize);

  RingSize ringSize() const;

  friend class Transfer;
};

using KeyInputVector = std::vector<KeyInput>;
XI_DECLARE_SPANS(KeyInput)

}  // namespace Currency
}  // namespace Blockchain
}  // namespace Xi
