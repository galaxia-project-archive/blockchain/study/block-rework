﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <optional>

#include <Xi/Global.hh>
#include <Xi/Result.h>
#include <Xi/TypeSafe/Integral.hpp>
#include <Xi/Crypto/PublicKey.hpp>
#include <Serialization/ISerializer.h>
#include <Serialization/TypeSafeSerialization.hpp>

#include "Xi/Blockchain/Currency/TransferVersion.hpp"
#include "Xi/Blockchain/Currency/TransferFeatures.hpp"
#include "Xi/Blockchain/Currency/PaymentIdentifier.hpp"
#include "Xi/Blockchain/Currency/KeyInput.hpp"
#include "Xi/Blockchain/Currency/KeyOutput.hpp"
#include "Xi/Blockchain/Currency/Unlock.hpp"

namespace Xi {
namespace Blockchain {
namespace Currency {

class TransferPrefix {
 public:
  XI_PROPERTY(TransferVersion, version, 0)
  XI_PROPERTY(TransferFeatures, features, TransferFeatures::None)
  XI_PROPERTY(Crypto::PublicKey, publicKey, null)
  XI_PROPERTY(KeyInputVector, inputs, {})
  XI_PROPERTY(KeyOutputVector, outputs, {})

 public:
  const std::optional<RingSize>& staticRingSize() const;
  TransferPrefix& staticRingSize(const std::optional<RingSize>& ringSize);
  const std::optional<Unlock>& unlock() const;
  TransferPrefix& unlock(const std::optional<Unlock>& unlock);
  const std::optional<PaymentIdentifier>& paymentIdentifier() const;
  TransferPrefix& paymentIdentifier(const std::optional<PaymentIdentifier>& pid);

 public:
  [[nodiscard]] bool serialize(CryptoNote::ISerializer& serializer);

  TransferPrefix(Null = null) {}

  /// maximum ring size used in any output
  /// \exception OutOfRange
  Currency::RingSize maximumRingSize() const;

  /// minimum ring size used in any output
  /// \exception OutOfRange
  Currency::RingSize minimumRingSize() const;

  /// Sets Mixin and StaticRingSize if applicable.
  /// \exception RuntimeError : if ring size limit is exceeded
  void emplaceRingSizeFeatures();

  Crypto::FastHash hash() const;

 private:
  std::optional<RingSize> m_staticRingSize{std::nullopt};
  std::optional<Unlock> m_unlock{std::nullopt};
  std::optional<PaymentIdentifier> m_paymentIdentifier{std::nullopt};
};

}  // namespace Currency
}  // namespace Blockchain
}  // namespace Xi
