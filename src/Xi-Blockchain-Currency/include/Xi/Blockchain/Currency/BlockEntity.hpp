﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <optional>

#include <Xi/Global.hh>
#include <Serialization/ISerializer.h>

#include "Xi/Blockchain/Currency/Features.hpp"
#include "Xi/Blockchain/Currency/Reward.hpp"
#include "Xi/Blockchain/Currency/StaticReward.hpp"
#include "Xi/Blockchain/Currency/Transfer.hpp"

namespace Xi {
namespace Blockchain {
namespace Currency {

class BlockEntity {
 public:
  XI_PROPERTY(Features, features, Features::None)
  XI_PROPERTY(std::optional<Reward>, reward, std::nullopt)
  XI_PROPERTY(std::optional<StaticReward>, staticReward, std::nullopt)
  XI_PROPERTY(std::vector<Transfer>, transfers, {})

  BlockEntity(Null = null) {}

  void emplaceFeatureFlags();

  [[nodiscard]] bool serialize(CryptoNote::ISerializer& serializer);

  Crypto::FastHash hash() const;
};

}  // namespace Currency
}  // namespace Blockchain
}  // namespace Xi
