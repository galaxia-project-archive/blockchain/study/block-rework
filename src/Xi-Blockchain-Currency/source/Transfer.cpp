﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Currency/Transfer.hpp"

#include <algorithm>
#include <numeric>
#include <limits>

#include <Xi/Exceptions.hpp>

namespace Xi {
namespace Blockchain {
namespace Currency {

bool Transfer::serialize(CryptoNote::ISerializer& serializer) {
  XI_RETURN_EC_IF_NOT(serializer(prefix(), "prefix"), false);

  XI_RETURN_EC_IF_NOT(serializer.beginStaticArray(prefix().inputs().size(), "signatures"), false);
  if (serializer.isInput()) {
    signatures().resize(prefix().inputs().size());
    for (size_t i = 0; i < prefix().inputs().size(); ++i) {
      XI_RETURN_EC_IF_NOT(serializer.beginStaticArray(prefix().inputs()[i].ringSize().native(), ""), false);
      signatures()[i].resize(prefix().inputs()[i].ringSize().native());
      for (auto& iSignatre : signatures()[i]) {
        XI_RETURN_EC_IF_NOT(serializer(iSignatre, ""), false);
      }
      XI_RETURN_EC_IF_NOT(serializer.endArray(), false);
    }
  } else if (serializer.isOutput()) {
    XI_RETURN_EC_IF_NOT(signatures().size() == prefix().inputs().size(), false);
    for (size_t i = 0; i < signatures().size(); ++i) {
      XI_RETURN_EC_IF_NOT(signatures()[i].size() == prefix().inputs()[i].ringSize().native(), false);
      XI_RETURN_EC_IF_NOT(serializer.beginStaticArray(signatures()[i].size(), "signatures"), false);
      for (auto& iSignature : signatures()[i]) {
        XI_RETURN_EC_IF_NOT(serializer(iSignature, ""), false);
      }
      XI_RETURN_EC_IF_NOT(serializer.endArray(), false);
    }
  } else {
    XI_RETURN_EC(false);
  }
  XI_RETURN_EC_IF_NOT(serializer.endArray(), false);

  XI_RETURN_SC(true);
}

Crypto::FastHash Transfer::hash() const {
  Crypto::FastHashVector hashes{};
  if (signatures().empty()) {
    hashes.resize(3);
  } else {
    hashes.resize(4);
  }

  if (!signatures().empty()) {
    Crypto::FastHashVector signatureHashes;
    signatureHashes.reserve(signatures().size());
    for (const auto& signature : signatures()) {
      signatureHashes.emplace_back(Crypto::FastHash::computeObjectHash(signature).takeOrThrow());
    }
    hashes[4] = Crypto::FastHash::computeMerkleTree(signatureHashes).takeOrThrow();
  }

  return Crypto::FastHash::computeMerkleTree(hashes).takeOrThrow();
}

Crypto::FastHash Transfer::prefixHash() const { return prefix().hash(); }

}  // namespace Currency
}  // namespace Blockchain
}  // namespace Xi
