﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Currency/BlockEntity.hpp"

#include <Xi/Exceptions.hpp>
#include <Common/VectorOutputStream.h>
#include <Serialization/BinaryOutputStreamSerializer.h>

namespace Xi {
namespace Blockchain {
namespace Currency {

bool BlockEntity::serialize(CryptoNote::ISerializer& serializer) {
  XI_RETURN_EC_IF_NOT(serializer(features(), "features"), false);
  if (serializer.isInput()) {
    if (hasFlag(features(), Features::Reward)) {
      reward(Reward{});
      XI_RETURN_EC_IF_NOT(serializer(*reward(), "reward"), false);
    } else {
      reward(std::nullopt);
    }

    if (hasFlag(features(), Features::StaticReward)) {
      staticReward(StaticReward{});
      XI_RETURN_EC_IF_NOT(serializer(*staticReward(), "static_reward"), false);
    } else {
      staticReward(std::nullopt);
    }

    if (hasFlag(features(), Features::Transfers)) {
      XI_RETURN_EC_IF_NOT(serializer(transfers(), "reward"), false);
      XI_RETURN_EC_IF(transfers().empty(), false);
    } else {
      transfers().clear();
    }
    XI_RETURN_SC(true);
  } else if (serializer.isOutput()) {
    if (hasFlag(features(), Features::Reward)) {
      XI_RETURN_EC_IF_NOT(reward().has_value(), false);
      XI_RETURN_EC_IF_NOT(serializer(*reward(), "reward"), false);
    } else {
      XI_RETURN_EC_IF(reward().has_value(), false);
    }

    if (hasFlag(features(), Features::StaticReward)) {
      XI_RETURN_EC_IF_NOT(staticReward().has_value(), false);
      XI_RETURN_EC_IF_NOT(serializer(*staticReward(), "static_reward"), false);
    } else {
      XI_RETURN_EC_IF(staticReward().has_value(), false);
    }

    if (hasFlag(features(), Features::Transfers)) {
      XI_RETURN_EC_IF(transfers().empty(), false);
      XI_RETURN_EC_IF_NOT(serializer(transfers(), "reward"), false);
    } else {
      XI_RETURN_EC_IF_NOT(transfers().empty(), false);
    }
    XI_RETURN_SC(true);
  } else {
    XI_RETURN_EC(false);
  }
}

Crypto::FastHash BlockEntity::hash() const {
  ByteVector blob{};
  blob.reserve(128);

  {
    Common::VectorOutputStream stream{blob};
    CryptoNote::BinaryOutputStreamSerializer serializer{stream};
    if (!serializer(const_cast<Features&>(features()), "features")) {
    }

    if (reward()) {
      auto rewardHash = reward()->hash();
      if (!serializer(rewardHash, "reward_hash")) {
        exceptional<RuntimeError>("serialization failure");
      }
    }

    if (staticReward()) {
      if (!serializer.binary(const_cast<StaticReward&>(*staticReward()).data(), StaticReward::bytes(),
                             "static_reward")) {
        exceptional<RuntimeError>("serialization failure");
      }
    }

    if (!transfers().empty()) {
      Crypto::FastHashVector extraHashes{};
      extraHashes.reserve(transfers().size());
      for (const auto& transfer : transfers()) {
        extraHashes.emplace_back(transfer.hash());
      }
      auto merkleHash = Crypto::FastHash::computeMerkleTree(extraHashes).takeOrThrow();
      if (!serializer(merkleHash, "transfers_hash")) {
        exceptional<RuntimeError>("serialization failure");
      }
    }
  }

  return Crypto::FastHash::compute(blob).takeOrThrow();
}

void BlockEntity::emplaceFeatureFlags() {
  features(Features::None);
  if (reward()) {
    features(addFlag(features(), Features::Reward));
  }
  if (staticReward()) {
    features(addFlag(features(), Features::StaticReward));
  }
  if (!transfers().empty()) {
    features(addFlag(features(), Features::Transfers));
  }
}

}  // namespace Currency
}  // namespace Blockchain
}  // namespace Xi
