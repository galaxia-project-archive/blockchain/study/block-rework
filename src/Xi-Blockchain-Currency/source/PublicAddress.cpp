﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Currency/PublicAddress.hpp"

namespace Xi {
namespace Blockchain {
namespace Currency {

PublicAddress::PublicAddress(Null _) : m_viewKey{_}, m_spendKey{_} {}

PublicAddress::PublicAddress(const Crypto::PublicKey &view, const Crypto::PublicKey &spend)
    : m_viewKey{view}, m_spendKey{spend} {}

bool PublicAddress::isValid() const { return viewKey().isValid() && spendKey().isValid(); }

bool PublicAddress::serialize(CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer(viewKey(), "view_key"), false);
  XI_RETURN_EC_IF_NOT(serializer(spendKey(), "spend_key"), false);
  return isValid();
}

}  // namespace Currency
}  // namespace Blockchain
}  // namespace Xi
