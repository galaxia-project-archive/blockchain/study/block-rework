﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Currency/Amount.hpp"

#include <unordered_map>
#include <type_traits>

#include <Xi/Exceptions.hpp>
#include <Xi/Algorithm/Math.h>

using amount_type = Xi::Blockchain::Currency::Amount;

namespace {
struct AmountDecompositionUnit {
  const amount_type Amount;
  const uint8_t DecadeIndex;
  const uint8_t Digit;

  XI_PADDING(6)

  AmountDecompositionUnit(amount_type amount, uint8_t decadeIndex, uint8_t digit)
      : Amount{amount}, DecadeIndex{decadeIndex}, Digit{digit} {}
};

class AmountDecompositions {
  std::unordered_map<amount_type, AmountDecompositionUnit> m_units;
  AmountDecompositions() {
    for (uint8_t decade = 0; decade < std::numeric_limits<amount_type::value_type>::digits10; ++decade) {
      uint64_t iDecadeUnit = Xi::pow64(10, decade);
      for (uint8_t digit = 1; digit < 10; ++digit) {
        amount_type iAmount{iDecadeUnit * digit};
        if (iAmount.native() > amount_type::max) {
          break;
        }
        m_units.emplace(std::piecewise_construct, std::forward_as_tuple(iAmount),
                        std::forward_as_tuple(iAmount, decade, digit));
      }
    }
  }

 public:
  static const std::unordered_map<amount_type, AmountDecompositionUnit> &units() {
    static const AmountDecompositions __Singleton{};
    return __Singleton.m_units;
  }
};
}  // namespace

bool Xi::Blockchain::Currency::isCanonicalAmount(const Amount amount) {
  return amount > Amount{0} && AmountDecompositions::units().find(amount) != AmountDecompositions::units().end();
}

uint64_t Xi::Blockchain::Currency::countCanonicalAmountBuckets(ConstAmountSpan amounts) {
  if (amounts.empty()) {
    return 0;
  }

  uint64_t count = 1;
  auto search = AmountDecompositions::units().find(amounts[0]);
  exceptional_if<InvalidArgumentError>(search == AmountDecompositions::units().end(), "none canonicial amount");

  uint8_t previousDecadeIndex = search->second.DecadeIndex;
  for (size_t i = 1; i < amounts.size(); ++i) {
    auto search = AmountDecompositions::units().find(amounts[i]);
    exceptional_if<InvalidArgumentError>(search == AmountDecompositions::units().end(), "none canonicial amount");
    if (search->second.DecadeIndex <= previousDecadeIndex) {
      count += 1;
    }
    previousDecadeIndex = search->second.DecadeIndex;
  }

  return count;
}

Xi::Blockchain::Currency::Amount Xi::Blockchain::Currency::cutDigitsFromAmount(Amount amount, const size_t count) {
  if (count == 0) {
    return amount;
  } else if (count >= std::numeric_limits<Amount::value_type>::digits10) {
    return Amount{0};
  } else {
    for (size_t i = 0; i < count; ++i) {
      uint64_t decimal = Xi::pow64(10, i + 1);
      amount = amount - Amount{amount.native() % decimal};
    }
    return amount;
  }
}

Xi::Blockchain::Currency::CanonicalAmount::CanonicalAmount(Xi::Null) : EnableIntegralFromThis(0) {}

bool Xi::Blockchain::Currency::CanonicalAmount::isValid() const { return isCanonicalAmount(Amount{native()}); }

bool Xi::Blockchain::Currency::serialize(Xi::Blockchain::Currency::CanonicalAmount &amount, Common::StringView name,
                                         CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer(amount.value, name), false);
  return amount.isValid();
}

Xi::Blockchain::Currency::CanonicalAmountVector Xi::Blockchain::Currency::canonicalDecomposition(
    Xi::Blockchain::Currency::Amount amount) {
  XI_RETURN_SC_IF(amount == Amount{0}, {});
  CanonicalAmountVector reval{};

  uint64_t nativeAmount = amount.native();
  uint64_t order = 1;
  while (nativeAmount != 0) {
    uint64_t chunk = (nativeAmount % 10) * order;
    nativeAmount /= 10;
    order *= 10;

    if (chunk != 0) {
      reval.emplace_back(chunk);
    }
  }
  return reval;
}
