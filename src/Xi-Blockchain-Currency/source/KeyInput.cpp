﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Currency/KeyInput.hpp"

#include <limits>
#include <cassert>

#include <Xi/Exceptions.hpp>

namespace Xi {
namespace Blockchain {
namespace Currency {

bool KeyInput::serialize(CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer(amount(), "amount"), false);
  XI_RETURN_EC_IF_NOT(serializer(keyImage(), "key_image"), false);
  XI_RETURN_EC_IF_NOT(serializer(outputs(), "outputs"), false);
  XI_RETURN_EC_IF(outputs().size() < RingSize::min, false);
  XI_RETURN_EC_IF(outputs().size() > RingSize::max, false);
  XI_RETURN_SC(true);
}

bool KeyInput::serializeStatic(CryptoNote::ISerializer &serializer, const RingSize ringSize) {
  XI_RETURN_EC_IF_NOT(serializer(amount(), "amount"), false);
  XI_RETURN_EC_IF_NOT(serializer(keyImage(), "key_image"), false);

  assert(ringSize.native() >= RingSize::min);
  assert(ringSize.native() <= RingSize::max);
  XI_RETURN_EC_IF_NOT(serializer.beginStaticArray(ringSize.native(), "outputs"), false);
  if (serializer.isInput()) {
    outputs().resize(ringSize.native());
    for (auto &iOutput : outputs()) {
      XI_RETURN_EC_IF_NOT(serializer(iOutput, ""), false);
    }
  } else if (serializer.isOutput()) {
    XI_RETURN_EC_IF_NOT(outputs().size() == ringSize.native(), false);
    for (auto &iOutput : outputs()) {
      XI_RETURN_EC_IF_NOT(serializer(iOutput, ""), false);
    }
  } else {
    XI_RETURN_EC(false);
  }

  XI_RETURN_EC_IF_NOT(serializer.endArray(), false);
  XI_RETURN_SC(true);
}

RingSize KeyInput::ringSize() const {
  exceptional_if<OutOfRangeError>(outputs().size() < RingSize::min, "too few outputs for ring size");
  exceptional_if<OutOfRangeError>(outputs().size() > RingSize::max, "too many outputs for ring size");
  return RingSize{static_cast<RingSize::value_type>(outputs().size())};
}

}  // namespace Currency
}  // namespace Blockchain
}  // namespace Xi
