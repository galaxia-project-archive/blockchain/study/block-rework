﻿/* ============================================================================================== *
 *                                                                                                *
 *                                     Galaxia Blockchain                                         *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Xi framework.                                                         *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Xi Project Developers <support.xiproject.io>                               *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Blockchain/Currency/TransferPrefix.hpp"

#include <numeric>

#include <Xi/Exceptions.hpp>
#include <Common/VectorOutputStream.h>
#include <Serialization/BinaryOutputStreamSerializer.h>

namespace Xi {
namespace Blockchain {
namespace Currency {

const std::optional<RingSize> &TransferPrefix::staticRingSize() const { return m_staticRingSize; }

TransferPrefix &TransferPrefix::staticRingSize(const std::optional<RingSize> &ringSize) {
  m_staticRingSize = ringSize;
  if (ringSize) {
    features(addFlag(features(), TransferFeatures::StaticRingSize));
    features(addFlag(features(), TransferFeatures::Mixins));
  } else {
    features(discardFlag(features(), TransferFeatures::StaticRingSize));
  }
  return *this;
}

const std::optional<Unlock> &TransferPrefix::unlock() const { return m_unlock; }

TransferPrefix &TransferPrefix::unlock(const std::optional<Unlock> &unlock) {
  m_unlock = unlock;
  if (unlock) {
    features(addFlag(features(), TransferFeatures::Unlock));
  } else {
    features(discardFlag(features(), TransferFeatures::Unlock));
  }
  return *this;
}

const std::optional<PaymentIdentifier> &TransferPrefix::paymentIdentifier() const { return m_paymentIdentifier; }

TransferPrefix &TransferPrefix::paymentIdentifier(const std::optional<PaymentIdentifier> &pid) {
  m_paymentIdentifier = pid;
  if (pid) {
    features(addFlag(features(), TransferFeatures::PaymentIdentifier));
  } else {
    features(discardFlag(features(), TransferFeatures::PaymentIdentifier));
  }
  return *this;
}

bool TransferPrefix::serialize(CryptoNote::ISerializer &serializer) {
  XI_RETURN_EC_IF_NOT(serializer(m_version, "version"), false);
  XI_RETURN_EC_IF_NOT(version() == TransferVersion{1}, false);
  XI_RETURN_EC_IF_NOT(serializer(features(), "features"), false);
  XI_RETURN_EC_IF_NOT(serializer(publicKey(), "public_key"), false);

  if (serializer.isInput()) {
    if (hasFlag(features(), TransferFeatures::StaticRingSize)) {
      XI_RETURN_EC_IF_NOT(hasFlag(features(), TransferFeatures::Mixins), false);
      staticRingSize(RingSize{0});
      XI_RETURN_EC_IF_NOT(serializer(*m_staticRingSize, "static_ring_size"), false);
      XI_RETURN_EC_IF(*staticRingSize() == RingSize{1}, false);
    } else {
      staticRingSize(std::nullopt);
    }

    if (hasFlag(features(), TransferFeatures::Unlock)) {
      unlock(Unlock{0});
      XI_RETURN_EC_IF_NOT(serializer(*m_unlock, "unlock"), false);
    } else {
      unlock(std::nullopt);
    }

    if (hasFlag(features(), TransferFeatures::PaymentIdentifier)) {
      paymentIdentifier(PaymentIdentifier::Null);
      XI_RETURN_EC_IF_NOT(serializer(*m_paymentIdentifier, "payment_identifier"), false);
    } else {
      paymentIdentifier(std::nullopt);
    }

    size_t inputsCount = 0;
    XI_RETURN_EC_IF_NOT(serializer.beginArray(inputsCount, "inputs"), false);

    if (staticRingSize() || !hasFlag(features(), TransferFeatures::Mixins)) {
      for (auto &iInput : inputs()) {
        XI_RETURN_EC_IF_NOT(serializer.beginObject(""), false);
        XI_RETURN_EC_IF_NOT(iInput.serializeStatic(serializer, staticRingSize().value_or(RingSize{1})), false);
        XI_RETURN_EC_IF_NOT(serializer.endObject(), false);
      }
    } else {
      RingSize minRingSize{RingSize::max};
      RingSize maxRingSize{RingSize::min};
      for (auto &iInput : inputs()) {
        XI_RETURN_EC_IF_NOT(serializer(iInput, ""), false);
        const auto iRingSize = iInput.ringSize();
        minRingSize = std::min(minRingSize, iRingSize);
        maxRingSize = std::max(maxRingSize, iRingSize);
      }

      if (minRingSize == maxRingSize) {
        XI_RETURN_EC(false);
      }
    }

    XI_RETURN_EC_IF_NOT(serializer.endArray(), false);

    XI_RETURN_EC_IF_NOT(serializer(outputs(), "outputs"), false);
    XI_RETURN_EC_IF(outputs().empty(), false);

    XI_RETURN_SC(true);
  } else if (serializer.isOutput()) {
    if (staticRingSize()) {
      XI_RETURN_EC_IF_NOT(hasFlag(features(), TransferFeatures::StaticRingSize), false);
      XI_RETURN_EC_IF_NOT(hasFlag(features(), TransferFeatures::Mixins), false);
      XI_RETURN_EC_IF_NOT(serializer(*m_staticRingSize, "static_ring_size"), false);
    } else {
      XI_RETURN_EC_IF(hasFlag(features(), TransferFeatures::StaticRingSize), false);
    }

    if (unlock()) {
      XI_RETURN_EC_IF_NOT(hasFlag(features(), TransferFeatures::Unlock), false);
      XI_RETURN_EC_IF_NOT(serializer(m_unlock, "unlock"), false);
    } else {
      XI_RETURN_EC_IF(hasFlag(features(), TransferFeatures::Unlock), false);
    }

    if (paymentIdentifier()) {
      XI_RETURN_EC_IF_NOT(hasFlag(features(), TransferFeatures::PaymentIdentifier), false);
      XI_RETURN_EC_IF_NOT(serializer(*m_paymentIdentifier, "payment_identifier"), false);
    } else {
      XI_RETURN_EC_IF(hasFlag(features(), TransferFeatures::PaymentIdentifier), false);
    }

    XI_RETURN_EC_IF(inputs().empty(), false);
    RingSize minRingSize{RingSize::max};
    RingSize maxRingSize{RingSize::min};
    for (auto &input : inputs()) {
      XI_RETURN_EC_IF(input.outputs().size() < RingSize::min || input.outputs().size() > RingSize::max, false);
      minRingSize = std::min(minRingSize, RingSize{static_cast<RingSize::value_type>(input.outputs().size())});
      maxRingSize = std::max(maxRingSize, RingSize{static_cast<RingSize::value_type>(input.outputs().size())});
    }

    if (minRingSize == maxRingSize) {
      if (minRingSize == RingSize{1}) {
        XI_RETURN_EC_IF(hasFlag(features(), TransferFeatures::Mixins), false);
        XI_RETURN_EC_IF(staticRingSize(), false);
      } else {
        XI_RETURN_EC_IF_NOT(hasFlag(features(), TransferFeatures::Mixins), false);
        XI_RETURN_EC_IF_NOT(staticRingSize(), false);
        XI_RETURN_EC_IF_NOT(*staticRingSize() == minRingSize, false);
      }
    } else {
      XI_RETURN_EC_IF_NOT(hasFlag(features(), TransferFeatures::Mixins), false);
      XI_RETURN_EC_IF(staticRingSize(), false);
    }

    size_t inputsCount = inputs().size();
    XI_RETURN_EC_IF_NOT(serializer.beginArray(inputsCount, "inputs"), false);

    if (staticRingSize() || !hasFlag(features(), TransferFeatures::Mixins)) {
      for (auto &iInput : inputs()) {
        XI_RETURN_EC_IF_NOT(serializer.beginObject(""), false);
        XI_RETURN_EC_IF_NOT(iInput.serializeStatic(serializer, staticRingSize().value_or(RingSize{1})), false);
        XI_RETURN_EC_IF_NOT(serializer.endObject(), false);
      }
    } else {
      for (auto &iInput : inputs()) {
        XI_RETURN_EC_IF_NOT(serializer(iInput, ""), false);
      }
    }

    XI_RETURN_EC_IF_NOT(serializer.endArray(), false);

    XI_RETURN_EC_IF(outputs().empty(), false);
    XI_RETURN_EC_IF_NOT(serializer(outputs(), "outputs"), false);

    XI_RETURN_SC(true);
  } else {
    XI_RETURN_EC(false);
  }
}

Crypto::FastHash TransferPrefix::hash() const {
  Crypto::FastHashVector hashes{};
  hashes.reserve(4);

  {
    ByteVector blob{};
    blob.reserve(128);
    Common::VectorOutputStream stream{blob};
    CryptoNote::BinaryOutputStreamSerializer serializer{stream};
    if (!serializer(const_cast<TransferVersion &>(version()), "version")) {
      exceptional<RuntimeError>("serialization failure in hash computation");
    }
    if (!serializer(const_cast<TransferFeatures &>(features()), "features")) {
      exceptional<RuntimeError>("serialization failure in hash computation");
    }
    if (staticRingSize() && !serializer(const_cast<RingSize &>(*staticRingSize()), "static_ring_size")) {
      exceptional<RuntimeError>("serialization failure in hash computation");
    }
    if (unlock() && !serializer(const_cast<Unlock &>(*unlock()), "unlock")) {
      exceptional<RuntimeError>("serialization failure in hash computation");
    }
    if (paymentIdentifier() &&
        !serializer(const_cast<PaymentIdentifier &>(*paymentIdentifier()), "payment_identifier")) {
      exceptional<RuntimeError>("serialization failure in hash computation");
    }
    hashes.emplace_back(Crypto::FastHash::compute(blob).takeOrThrow());
  }

  hashes.emplace_back(Crypto::FastHash::Null);
  static_assert(Crypto::FastHash::bytes() == Crypto::PublicKey::bytes(), "following line assumes equality");
  std::memcpy(hashes.back().data(), publicKey().span().data(), Crypto::FastHash::bytes());

  hashes.emplace_back(Crypto::FastHash::computeObjectHash(inputs()).takeOrThrow());
  hashes.emplace_back(Crypto::FastHash::computeObjectHash(outputs()).takeOrThrow());

  return Crypto::FastHash::computeMerkleTree(hashes).takeOrThrow();
}

RingSize TransferPrefix::maximumRingSize() const {
  const auto maxSize = std::accumulate(
      inputs().begin(), inputs().end(), std::numeric_limits<RingSize::value_type>::min(),
      [](const auto acc, const auto &iInput) { return std::max<uint64_t>(acc, iInput.outputs().size()); });
  exceptional_if<OutOfRangeError>(maxSize < RingSize::min, "ring size too small");
  exceptional_if<OutOfRangeError>(maxSize > RingSize::max, "ring size too large");
  return RingSize{static_cast<RingSize::value_type>(maxSize)};
}

RingSize TransferPrefix::minimumRingSize() const {
  const auto minSize = std::accumulate(
      inputs().begin(), inputs().end(), std::numeric_limits<RingSize::value_type>::max(),
      [](const auto acc, const auto &iInput) { return std::min<uint64_t>(acc, iInput.outputs().size()); });
  exceptional_if<OutOfRangeError>(minSize < RingSize::min, "ring size too small");
  exceptional_if<OutOfRangeError>(minSize > RingSize::max, "ring size too large");
  return RingSize{static_cast<RingSize::value_type>(minSize)};
}

void TransferPrefix::emplaceRingSizeFeatures() {
  exceptional_if<RuntimeError>(inputs().empty(), "transfer inputs empty");

  RingSize minRingSize{RingSize::max};
  RingSize maxRingSize{RingSize::min};
  for (auto &input : inputs()) {
    exceptional_if<RuntimeError>(input.outputs().size() < RingSize::min || input.outputs().size() > RingSize::max,
                                 "invalid ring size");
    minRingSize = std::min(minRingSize, RingSize{static_cast<RingSize::value_type>(input.outputs().size())});
    maxRingSize = std::max(maxRingSize, RingSize{static_cast<RingSize::value_type>(input.outputs().size())});
  }

  if (minRingSize == maxRingSize) {
    if (minRingSize == RingSize{1}) {
      features(discardFlag(features(), TransferFeatures::Mixins));
      staticRingSize(std::nullopt);
    } else {
      features(addFlag(features(), TransferFeatures::Mixins));
      staticRingSize(minRingSize);
    }
  } else {
    features(addFlag(features(), TransferFeatures::Mixins));
    staticRingSize(std::nullopt);
  }
}

}  // namespace Currency
}  // namespace Blockchain
}  // namespace Xi
